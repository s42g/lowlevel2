#!/bin/bash

NUM_JOBS=1

OUTDIR=benchmarks

function handler {
    kill -- -$$
}

trap "handler" SIGINT

function wait_jobs {
  RUNNING_JOBS=($(jobs -pr))
	RUNNING_JOBS=${#RUNNING_JOBS[*]}
	while [[ $RUNNING_JOBS -ge ${NUM_JOBS} ]]; do
		sleep 1
		RUNNING_JOBS=($(jobs -pr))
		RUNNING_JOBS=${#RUNNING_JOBS[*]}
	done
}

./gradlew install

while IFS=\n read -r truthtable
do
	wait_jobs
    echo "mapping truth table $truthtable ..."
	./build/install/enuMap/bin/enuMap -i $truthtable -t -c syn.config 2>&1 | tee $OUTDIR/$truthtable.log &
done < cello_functions.csv

for job in `jobs -pr`; do
    wait $job
done
