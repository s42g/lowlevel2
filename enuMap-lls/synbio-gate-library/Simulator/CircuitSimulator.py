# -*- coding: utf-8 -*-
"""
Created on Thu Jan  2 07:41:43 2020

@author: Erik Kubaczka
"""
import pickle;
import SimulatorHelper;
#import multiprocessing as mp;
import numpy as np;
import scipy.stats as st;




def insertNativeResponseFunctions(responseFunctions):
    keyList = responseFunctions.keys();
    for key in keyList:
        if (responseFunctions[key]["nativ"]):
            if (responseFunctions[key]["type"] == "INHIBITORY_HILL_EQUATION"):
                responseFunctions[key]["equation"] = inhibitoryHillEquation;
    
    return responseFunctions;


def startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles, circuitSim):
    def getParameters(responseFunction, particles):
        length = -1;
        for particleParameter in responseFunction["particle_parameters"]:
            if (length == -1):
                length = len(particles[particleParameter]);
            elif (length != len(particles[particleParameter])):
                raise Exception("The length of the particle lists are not equal. The number of particles provided for each parameter should be equal.");
            #for particle in particles[particleParameter]:
        parameters = [];
       # for iX in range(length):
            
           # for particleParameter in responseFunction["particle_parameters"]:
                
    length = min([simData["circuit_response_functions_particles"]["number_of_particles"], simSpec["number_of_particles"]]);
    #print("Used particles:", length);
    
    
    results = {};
    for outID in truthTable["output_IDs"]:
        results[outID] = {"0" : {}, "1":{}};
        
        for out in truthTable["outputs"]:         
            #print(out)
            results[outID][truthTable["outputs"][out][outID]][out] = np.zeros(length);
                        
            
    
    for iX in range(length):            
        for node in nodeOrder:
            try:
                respFunc = responseFunctions[node];
                for parameter in respFunc["particle_parameters"]:
                    #print(simData["circuit_response_functions_particles"][node][particle][iX])
                    #respFunc["parameters"][parameter] = simData["circuit_response_functions_particles"][node][parameter][iX];
                    responseFunctions[assignment[node]]["parameters"][parameter] = simData["circuit_response_functions_particles"][assignment[node]][parameter][iX];
                    #print(node, simData["circuit_response_functions_particles"][node][parameter][iX], responseFunctions[node]["parameters"][parameter])
            except KeyError:
                zero = 0;
                #node is an Input or Output
                #print("Exception")
        #print(" ");
        #print("TEST", responseFunctions["NOT_0"]["parameters"]["K"])
        simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iX);
        #for iX in range(length):
         #   for particleParameter in responseFunction["particle_parameters"]:
          #      i = 0;
          
    return results;


"""
For a given set of parameters, this method adds the circuit outputs to the corresponding sets.
The corresponding ID and either to the True or False set.
"""    
def simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iteration):
    #IDmap = truthTable["input_IDs"];
    outputIDs = truthTable["output_IDs"].keys();
    #print(nodeOrder);
    bioInputs = truthTable["bio_inputs"];
    #print(len(truthTable["outputs"]));
    # Durchläuft die verschiedenen Eingaben
    
        
    for iX in range(len(truthTable["outputs"])):
        #print(bioInputs["input_%d" % iX], truthTable["outputs"]["output_%d" % iX]);
        
        circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs["input_%d" % iX], dict(circuitSim), responseFunctions);
        #circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs[out], dict(circuitSim), responseFunctions);
        """        print(iX, "\n", responseFunctions["NOT_0"]["parameters"], "\n", circuitVals, "\n");
        """
        #Store the circuit outputs in the result list
        for outputID in outputIDs:
           results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iteration] = circuitVals[outputID];
           #results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iX].append(circuitVals[outputID]);
        
    #print("");

"""
Evaluates a complete circuit according to the specified nodeOrder
The parameters provided by the responseFunctions are used.
"""    
def getCircuitVal(nodeOrder, circuit, assignment, truthTable, circuitInputs, circuitVals, responseFunctions):
    for node in nodeOrder:
        if (node in truthTable["input_IDs"].keys()):
            # The value of the current node is given by the circuit inputs
            circuitVals[node] = circuitInputs[node];
        elif (node in truthTable["output_IDs"].keys()):
            # Theoretically we are done and can return
            # A final operation to the output value can be applied. This is currently not the case.
            circuitVals[node] = circuitVals[node];
        else:
            bioGate = assignment[node];
            circuitVals[node] = evaluateGate(circuitVals[node], responseFunctions[bioGate]);
            
        for child in circuit[node]:
            circuitVals[child] += circuitVals[node];            
        #print(node, circuitVals[node])                
    #print("OUTPUT", circuitVals["NOT_1"]);
    #print("");
    return circuitVals;
    

"""
Evaluates a gate for the given response function and the corresponding parameters
"""    
def evaluateGate(val, responseFunction):
    if (responseFunction["nativ"]):
        return responseFunction["equation"](val, responseFunction["parameters"]);
    else:        
        return -1;  #Not implemented yet...
    
    


def determineScores(simRes):
    for key in simRes:
        simRes[key]["SCORE"] = -1;
        for outON in simRes[key]["1"]:
            for outOFF in simRes[key]["0"]:
                score = st.wasserstein_distance(simRes[key]["0"][outOFF], simRes[key]["1"][outON]);
                if (simRes[key]["SCORE"] == -1):
                    simRes[key]["SCORE"] = score;
                elif (simRes[key]["SCORE"] > score):
                    simRes[key]["SCORE"] = score;
        
    
"""
    Read the initialisiation data
"""
with open("simulation_data.pkl", "rb") as file:
    simData = pickle.load(file); 
    
"""
    Read the general simulation specifications
"""
#with open("simulation_specification.pkl", "rb") as file:
#    simSpec = pickle.load(file); 
simSpec = SimulatorHelper.loadJSON("simulation_specification.json");



"""
    Retrieve the assignment to simulate
"""
assignment = SimulatorHelper.loadJSON("interfacing_test_data/assignment.json");

"""
    Implement the natively supported response functions
"""
def inhibitoryHillEquation(x, parameters):
    #print(parameters)
    K = parameters["K"];
    n = parameters["n"];
    ymin = parameters["ymin"];
    ymax = parameters["ymax"];
    return ymin + (ymax-ymin)/(1 + (x / K)**n);


    

responseFunctions = simData["circuit_response_functions"];
insertNativeResponseFunctions(responseFunctions);


#simulate(simData["circuit_valid_node_list"], simData["circuit"], simData["circuit_truthtable"], simData["circuit_response_functions"], simData["circuit_simulation"])
nodeOrder = simData["circuit_valid_node_list"];
circuit = simData["circuit"];
truthTable = simData["circuit_truthtable"];
circuitInputs = simData["circuit_truthtable"]["bio_inputs"]["input_0"];
circuitVals = simData["circuit_simulation"];
responseFunctions = simData["circuit_response_functions"];
particles = simData["circuit_response_functions_particles"];
parameters = {gate:simData["circuit_response_functions"][gate]["parameters"] for gate in simData["circuit_response_functions"]};

#result1 = getCircuitVal(nodeOrder, circuit, assignment, truthTable, circuitInputs, dict(circuitVals), responseFunctions);

#results = {};
#for outID in truthTable["output_IDs"]:
#    results[outID] = {"0" : [], "1":[]};

#simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, dict(circuitVals), results)

simRes = startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles, dict(circuitVals));

                

determineScores(simRes);
"""
for outID in simRes:
    for offID in simRes[outID]["0"]:
        print(offID, np.mean(simRes[outID]["0"][offID]), np.var(simRes[outID]["0"][offID]))
    for onID in simRes[outID]["1"]:
        print(onID, np.mean(simRes[outID]["1"][onID]), np.var(simRes[outID]["1"][onID]))
"""
print(simRes["O"]["SCORE"]);
