# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 18:41:41 2020

@author: Erik Kubaczka
"""

"""
    !!!
    Attention: The proposed circuit representation can only be used for combinational circuits
    !!!

    Steps of Initialisation
    1. Load structure.json
    2. Retrieve circuit
    3. 
"""

import numpy as np;
import json;
import parser;
import multiprocessing;
import SimulatorHelper;
import scipy.stats as st;
import ast;
import sys;



VERSION_NUMBER = "2.0";



def initialiseSimulator(structurePath, gateLibPath):
    """
        Unterstützung für eingebaute Response Functions wie Hill Curves hinzufügen (schneller evaluierbar).
        Wenn unbekannte Funktonen vorliegen, dann Parsen mit 
        def load_expression(source_string):
            st = parser.expr(source_string)
            return st, st.compile()
        und anschließend eval(st.compiler()) anstatt direkt eval(source_string).
    """
    def generateResponseFunctions(gateLib):
        def load_expression(source_string):
            st = parser.expr(source_string)
            return st.compile()
        
        
        responseFunctions = {gate['identifier'] : 0 for gate in gateLib};
    
        for gate in gateLib:
            if (gate["biorep"]["response_function"]["type"] == "INHIBITORY_HILL_EQUATION" and True):
                # If native, the simulator makes use of the built in functions and does not evaluate the the given code for not natively supported expressions
                
                responseFunctions[gate["identifier"]] = {"nativ": True, "type":gate["biorep"]["response_function"]["type"]};
            else:
                responseFunctions[gate["identifier"]] = {"nativ": False, "expression": load_expression(gate["biorep"]["response_function"]["equation"])};
    
            responseFunctions[gate["identifier"]]["parameters"] = gate["biorep"]["response_function"]["parameters"];    
            responseFunctions[gate["identifier"]]["particle_parameters"] = list(gate["biorep"]["particles"].keys());
            
            
        return responseFunctions;
    
    
    """
    This method currently only works for one parameter!
    """    
    def prepareParameters(gateLib, n, parameter):
        def generateParticles(k, n):
            sigma_k = 0.8;        
            mu_k = np.log(k) - 0.5 * sigma_k**2;
            return np.random.lognormal(mu_k, sigma_k, n);
        
        
        particlesLib = {"number_of_particles" : n};
        for gate in gateLib:
            
            rawParticles = gate['biorep']['particles'][parameter];
            if (len(rawParticles) > 0):
                if (len(rawParticles) < n):
                    raise Exception('The number of necessary particles should not exceed the number of particles provided within the library. Number of provided particles %d. Condition mismatch %d <= %d' % (len(rawParticles), n, len(rawParticles)))
                if (n < 0):
                    raise Exception('The number of necessary particles should be greater than zero!');
            
                np.random.shuffle(rawParticles);
                particles = np.zero(n);
                for iX in range(n):
                    particles[iX] = rawParticles[iX];
            else:
                particles = {parameter : generateParticles(gate['biorep']['response_function']["parameters"][parameter], n)};
            
            particlesLib[gate['identifier']] = particles;
        return particlesLib;
        
    
        
        
    #   This method checks whether the circuit is represented in the right order, 
    #   thus the gates are in the order corresponding to the calculation.
    def isValid(nodes, edges):
        i = 0;
        for node in nodes:
            sources = getSources(edges, node['id']);
            
            for source in sources:
                #print(source, nodes[i:] );
                if (source in [nod['id'] for nod in nodes[i:]]):
                    return False;
            i += 1;
            
        return True;
    
    
    """
    This method returns a list of node IDs, which is sorted in the order of evaluation for the simulation.
    """
    def createValidNodeIDList(nodes, edges):
        def allSourcesPresent(edges, nodeID, validNodeIDs):
             targetSources = getSources(edges, target);
             for targetSource in targetSources:
                if (not targetSource in validNodeIDs):
                    return False;
             return True;   
        
        validNodeIDs = [];
        
        nodeSets = getSets(nodes);
        sortedInputs = sorted(nodeSets['INPUT'], key=lambda d: d['id']);
        
        for inVal in sortedInputs:
            validNodeIDs.append(inVal['id']);
            
        i = 0;
        while (len(validNodeIDs) != len(nodes)):
            targets = getTargets(edges, validNodeIDs[i]);
            for target in targets:
                if (not target in validNodeIDs):
                    if (allSourcesPresent(edges, target, validNodeIDs)):
                        validNodeIDs.append(target);
                
                        
            i += 1;
                
        
        return validNodeIDs;
    
    
    def sortNodes(validNodeIDs, nodeDict):
        validNodes = [];
        for validNodeID in validNodeIDs:
            validNodes.append(nodeDict[validNodeID]);
        
        return validNodes;   
    
    def getNodeDict(nodes):
        nodeDict = {node['id']: node for node in nodes};
        return nodeDict;
        
    
    def getSets(nodes):
        graphSet = {"INPUT": [], "LOGIC": [], "OUTPUT": []};
        for node in nodes:
            graphSet[node['type']].append(node);        
            
        return graphSet;
        
    
    def getTargets(edges, sourceID):
        targets = [];
        for edge in edges:
            if (edge['source'] == sourceID):
                targets.append(edge['target']);
        return targets;
    
    
    def getSources(edges, targetID):
        sources = [];
        for edge in edges:
            if (edge['target'] == targetID):
                sources.append(edge['source']);
        return sources;
    
    
    def generateCircuit(nodeDict, edges, validNodeIDs):
        circuit = {nodeID:getTargets(edges, nodeID) for nodeID in validNodeIDs};
        return circuit;
        
    def generateTruthTable(inputIDs, outputIDs, outputTruthTable, inputSpecification):
        def getCombination(val, numberOfInputs):
            combination = bin(val).replace("0b", "");
            missingZeros = numberOfInputs - len(combination);
            for i in range(missingZeros):
                combination = "0" + combination;
                
            return combination;
        
        def getBioVals(inputIDs, combination, inputSpecification):
            i = 0;
            #bioVals = [];
            bioVals = {};
            for inputID in inputIDs:
                #bioVals.append(inputSpecification[inputID][str(combination[i])]);
                bioVals[inputID] = inputSpecification[inputID][str(combination[i])];
                i += 1;
                
            return bioVals;
        
        sortedInputIDs = sorted(inputIDs);
        sortedOutputIDs = sorted(outputIDs);
        
        inputIDs = {sortedInputIDs[val]:val for val in range(len(sortedInputIDs))};
        outputIDs = {sortedOutputIDs[val]:val for val in range(len(sortedOutputIDs))};
        
        
        
        truthTable = {      "input_IDs" : inputIDs,
                            "output_IDs" : outputIDs,
                            "inputs" : {"input_" + str(iVal) : [] for iVal in range(len(outputTruthTable))},
                            "bio_inputs" : {"input_" + str(iVal) : [] for iVal in range(len(outputTruthTable))},
                            "outputs" : {"output_" + str(iVal) : [] for iVal in range(len(outputTruthTable))}
                            };
                      
     
        for i in range(len(outputTruthTable)):
            combination = getCombination(i, len(inputIDs));
            truthTable["inputs"]["input_" + str(i)] = [int(c) for c in combination];
            truthTable["outputs"]["output_" + str(i)] =  {sortedOutputIDs[0] : outputTruthTable[i]}; # Only works for just one circuit output
            truthTable["bio_inputs"]["input_" + str(i)] = getBioVals(inputIDs, combination, inputSpecification);
        
    
        return truthTable;
    
    """
        This specification shall be given within the library or any additional source.
    """
    def generateInputSpecification(inputIDs):
        inputSpecification = {inID : {"0" : 0.01, "1" : 3} for inID in inputIDs};
        return inputSpecification;
    
    
    
    
    
    jsonContent = SimulatorHelper.loadJSON(structurePath);
    
    structureData = jsonContent['graph'];
    
    nodes = structureData['nodes'];
    edges = structureData['edges'];
    nodeDict = getNodeDict(nodes);
    nodeSets = getSets(nodes);
    
    validNodeIDs = createValidNodeIDList(nodes, edges);
    validNodes = sortNodes(validNodeIDs, nodeDict);
    
    #   print("SortedNodeList:", nodes2);
    
    circuit = generateCircuit(nodeDict, edges, validNodeIDs);
    circuitSimulation = {nodeID: 0 for nodeID in validNodeIDs};
    circuitInfo =  {'NUMBER_OF_INPUTS': len(nodeSets['INPUT']),
                    'NUMBER_OF_OUTPUTS': len(nodeSets['OUTPUT']),
                    'NUMBER_OF_LOGICS': len(nodeSets['LOGIC'])};
    
    inputIDs = [node['id'] for node in nodeSets['INPUT']];
    outputIDs = [node['id'] for node in nodeSets['OUTPUT']]
    
    inputSpecification = generateInputSpecification(inputIDs);
                    
    truthTable = generateTruthTable(inputIDs, outputIDs, jsonContent['truthtable'], inputSpecification);
    
    gateLib = SimulatorHelper.loadJSON(gateLibPath);
    responseFunctions = generateResponseFunctions(gateLib);
    responseFunctionParameters = prepareParameters(gateLib, 1000, 'K');
    
    
    simData = { 'VERSION_NUMBER': VERSION_NUMBER,
                'circuit_info': circuitInfo,
                'circuit':circuit,
                'circuit_valid_node_list' : validNodeIDs,
                'circuit_simulation': circuitSimulation,
                'circuit_truthtable': truthTable,
                'circuit_response_functions': responseFunctions,
                'circuit_response_functions_particles' : responseFunctionParameters,
                };
               
    simSpec = {    'CPU_COUNT' : multiprocessing.cpu_count(),
                   'number_of_particles' : 1000};           
        
    return simData, simSpec;







def startSimulation(assignment, simData, simSpec):
    def insertNativeResponseFunctions(responseFunctions):
        keyList = responseFunctions.keys();
        for key in keyList:
            if (responseFunctions[key]["nativ"]):
                if (responseFunctions[key]["type"] == "INHIBITORY_HILL_EQUATION"):
                    responseFunctions[key]["equation"] = inhibitoryHillEquation;
        
        return responseFunctions;
    
    
    def startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles, circuitSim):
        def getParameters(responseFunction, particles):
            length = -1;
            for particleParameter in responseFunction["particle_parameters"]:
                if (length == -1):
                    length = len(particles[particleParameter]);
                elif (length != len(particles[particleParameter])):
                    raise Exception("The length of the particle lists are not equal. The number of particles provided for each parameter should be equal.");
                #for particle in particles[particleParameter]:
            parameters = [];
           # for iX in range(length):
                
               # for particleParameter in responseFunction["particle_parameters"]:
                    
        length = min([simData["circuit_response_functions_particles"]["number_of_particles"], simSpec["number_of_particles"]]);
        #print("Used particles:", length);
        
        
        results = {};
        for outID in truthTable["output_IDs"]:
            results[outID] = {"0" : {}, "1":{}};
            
            for out in truthTable["outputs"]:         
                #print(out)
                results[outID][truthTable["outputs"][out][outID]][out] = np.zeros(length);
                            
                
        
        for iX in range(length):            
            for node in nodeOrder:
                try:
                    respFunc = responseFunctions[node];
                    for parameter in respFunc["particle_parameters"]:
                        #print(simData["circuit_response_functions_particles"][node][particle][iX])
                        #respFunc["parameters"][parameter] = simData["circuit_response_functions_particles"][node][parameter][iX];
                        responseFunctions[assignment[node]]["parameters"][parameter] = simData["circuit_response_functions_particles"][assignment[node]][parameter][iX];
                        #print(node, simData["circuit_response_functions_particles"][node][parameter][iX], responseFunctions[node]["parameters"][parameter])
                except KeyError:
                    zero = 0;
                    #node is an Input or Output
                    #print("Exception")
            #print(" ");
            #print("TEST", responseFunctions["NOT_0"]["parameters"]["K"])
            simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iX);
            #for iX in range(length):
             #   for particleParameter in responseFunction["particle_parameters"]:
              #      i = 0;
              
        return results;
    
    
    """
    For a given set of parameters, this method adds the circuit outputs to the corresponding sets.
    The corresponding ID and either to the True or False set.
    """    
    def simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iteration):
        #IDmap = truthTable["input_IDs"];
        outputIDs = truthTable["output_IDs"].keys();
        #print(nodeOrder);
        bioInputs = truthTable["bio_inputs"];
        #print(len(truthTable["outputs"]));
        # Durchläuft die verschiedenen Eingaben
        
            
        for iX in range(len(truthTable["outputs"])):
            #print(bioInputs["input_%d" % iX], truthTable["outputs"]["output_%d" % iX]);
            
            circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs["input_%d" % iX], dict(circuitSim), responseFunctions);
            #circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs[out], dict(circuitSim), responseFunctions);
            """        print(iX, "\n", responseFunctions["NOT_0"]["parameters"], "\n", circuitVals, "\n");
            """
            #Store the circuit outputs in the result list
            for outputID in outputIDs:
               results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iteration] = circuitVals[outputID];
               #results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iX].append(circuitVals[outputID]);
            
        #print("");
    
    """
    Evaluates a complete circuit according to the specified nodeOrder
    The parameters provided by the responseFunctions are used.
    """    
    def getCircuitVal(nodeOrder, circuit, assignment, truthTable, circuitInputs, circuitVals, responseFunctions):
        for node in nodeOrder:
            if (node in truthTable["input_IDs"].keys()):
                # The value of the current node is given by the circuit inputs
                circuitVals[node] = circuitInputs[node];
            elif (node in truthTable["output_IDs"].keys()):
                # Theoretically we are done and can return
                # A final operation to the output value can be applied. This is currently not the case.
                circuitVals[node] = circuitVals[node];
            else:
                bioGate = assignment[node];
                circuitVals[node] = evaluateGate(circuitVals[node], responseFunctions[bioGate]);
                
            for child in circuit[node]:
                circuitVals[child] += circuitVals[node];            
            #print(node, circuitVals[node])                
        #print("OUTPUT", circuitVals["NOT_1"]);
        #print("");
        return circuitVals;
        
    
    """
    Evaluates a gate for the given response function and the corresponding parameters
    """    
    def evaluateGate(val, responseFunction):
        if (responseFunction["nativ"]):
            return responseFunction["equation"](val, responseFunction["parameters"]);
        else:        
            return -1;  #Not implemented yet...
        
        
    
    
    def determineScores(simRes):
        for key in simRes:
            simRes[key]["SCORE"] = -1;
            for outON in simRes[key]["1"]:
                for outOFF in simRes[key]["0"]:
                    score = st.wasserstein_distance(simRes[key]["0"][outOFF], simRes[key]["1"][outON]);
                    if (simRes[key]["SCORE"] == -1):
                        simRes[key]["SCORE"] = score;
                    elif (simRes[key]["SCORE"] > score):
                        simRes[key]["SCORE"] = score;
            
        
   
    """
        Implement the natively supported response functions
    """
    def inhibitoryHillEquation(x, parameters):
        #print(parameters)
        K = parameters["K"];
        n = parameters["n"];
        ymin = parameters["ymin"];
        ymax = parameters["ymax"];
        return ymin + (ymax-ymin)/(1 + (x / K)**n);    


    
    """
        Retrieve the assignment to simulate
    """
    # assignment = SimulatorHelper.loadJSON(assignmentPath);

    
    
        
    
    responseFunctions = simData["circuit_response_functions"];
    insertNativeResponseFunctions(responseFunctions);
    
    
    #simulate(simData["circuit_valid_node_list"], simData["circuit"], simData["circuit_truthtable"], simData["circuit_response_functions"], simData["circuit_simulation"])
    nodeOrder = simData["circuit_valid_node_list"];
    circuit = simData["circuit"];
    truthTable = simData["circuit_truthtable"];
#    circuitInputs = simData["circuit_truthtable"]["bio_inputs"]["input_0"];
    circuitVals = simData["circuit_simulation"];
    responseFunctions = simData["circuit_response_functions"];
    particles = simData["circuit_response_functions_particles"];
#    parameters = {gate:simData["circuit_response_functions"][gate]["parameters"] for gate in simData["circuit_response_functions"]};
    
    #result1 = getCircuitVal(nodeOrder, circuit, assignment, truthTable, circuitInputs, dict(circuitVals), responseFunctions);
    
    #results = {};
    #for outID in truthTable["output_IDs"]:
    #    results[outID] = {"0" : [], "1":[]};
    
    #simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, dict(circuitVals), results)
    
    simRes = startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles, dict(circuitVals));
    
                    
    
    determineScores(simRes);
    """
    for outID in simRes:
        for offID in simRes[outID]["0"]:
            print(offID, np.mean(simRes[outID]["0"][offID]), np.var(simRes[outID]["0"][offID]))
        for onID in simRes[outID]["1"]:
            print(onID, np.mean(simRes[outID]["1"][onID]), np.var(simRes[outID]["1"][onID]))
    """
    print(simRes["O"]["SCORE"]);
    
    
##############################################################################

"""
    Is used for each simulation as well as for the initialisation.
"""
def parseInput(inText):
    def prepareString(inText):
        inText = " ".join(inText.split());
        inText = inText.replace(" =", "=");
        inText = inText.replace("= ", "=");
        inText = inText.replace(" :", ":");
        inText = inText.replace(": ", ":");
        inText = inText.replace(" ,", ",");
        inText = inText.replace(", ", ",");
        return inText;
    
    inText = prepareString(inText);    
    instruction = inText.split();       # Split at Whitespace!
   # print(instruction);
    if (len(instruction) > 0):
        cmd = instruction[0];
        specDict = {};
        #print(inText);
        for elem in instruction[1:]:
            field, val = elem.split("=");
         #   print(val);
            #print(field, val);
            if (field == "n"):
                    specDict[field] = int(val);
            elif (field == "threads"):
                specDict[field] = int(val);
            elif (field == "assignment"):
                specDict[field] = ast.literal_eval(val);
            else:
                specDict[field] = val;
        return cmd, specDict;
    else:
        return "", {};

cmd, specDict = parseInput(" ".join(sys.argv));

print("Start Simulator Initialisation");
if ("s_path" in specDict and "lib_path" in specDict):
#    simData, simSpec = initialiseSimulator("interfacing_test_data/structure.json", "../gate_library.json");
    simData, simSpec = initialiseSimulator(specDict["s_path"], specDict["lib_path"]);
    print("Simulator Initialised and ready to use.");
    print("Dont forget to configure the assignment (append \"start\" with \"a_path=...\", the corresponding path, or directly by the assignment, by making use of\"assignment=...\"=  and the number of particles (append \"n=...\" to \"start\")");
    print("Start simulation with \"start\"");    
else:
    print("You need to set the circuit structure and the library path to initialise the simulation!");
    print("This can be done by appending the command \"settings\" by \"s_path=...\" and the corresponding file path and next to this, the path to the gate lib with \"lib_path=...\"");
    structurePath = "NULL";
    gateLibPath = "NULL";
    configured = False;
    inText = input("define settings: ");
    while (inText != "exit" and not(configured)):
        cmd, specDict = parseInput(inText);
        print(cmd);            
        if (cmd == "settings"):
            if ("s_path" in specDict):
                structurePath = specDict["s_path"];
            if ("lib_path" in specDict):
                gateLibPath = specDict["lib_path"];
            if (structurePath != "NULL" and gateLibPath != "NULL"):
                simData, simSpec = initialiseSimulator(structurePath, gateLibPath);
                print("Configuration finished");
                print("If desired, this configuration can be changed later on with the same comands.");
                configured = True;

        if (not(configured)):
            inText = input("define settings: ");
            
    if (inText == "exit"):
        sys.exit("You stopped the simulator.");



#assignmentPath = "interfacing_test_data/assignment_1.json"
#assignment = SimulatorHelper.loadJSON(assignmentPath);
assignment = "NULL";
#       startSimulation(assignmentPath = "interfacing_test_data/assignment.json", simData = simData, simSpec = simSpec, numberOfSamples = numberOfSamples)
inText = input("ready: ");
#   inText = "start";
specDict = {};
simSpec["number_of_particles"] = 1000;
iX = 0;
while (inText != "exit"):
    cmd, specDict = parseInput(inText);
    #print(cmd, specDict);
    if (cmd == "start"):
    
        if ("n" in specDict):
            simSpec["number_of_particles"] = specDict["n"];
        if ("threads" in specDict):
            simSpec["CPU_COUNT"] = specDict["threads"];
        if ("a_path" in specDict):
            assignmentPath = specDict["a_path"];
            assignment = SimulatorHelper.loadJSON(assignmentPath);
        if ("assignment" in specDict):
            assignment = specDict["assignment"];
        
        if (assignment != "NULL"):
            #print(assignment)
            startSimulation(assignment = assignment, simData = simData, simSpec = simSpec)
            #print(iX);
            iX += 1;     
        else:
            print("ERROR: Assignment needs to be defined at least once by adding \"assignment={...}\" or \"a_path=dir/file.json\" to after start.");
            
    if (cmd == "settings"):
        if ("s_path" in specDict):
            structurePath = specDict["structure"];
        if ("lib_path" in specDict):
            gateLibPath = specDict["lib_path"];
        simData, simSpec = initialiseSimulator(specDict["s_path"], specDict["lib_path"]);

            
    inText = input("ready: ");
    
    
