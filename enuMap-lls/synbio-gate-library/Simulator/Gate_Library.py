# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 12:50:32 2019

@author: Erik Kubaczka
"""
import json;

class GateLibrary(object):
    
    def __init__(self, fileLocation):
        self.fileLocation = fileLocation;    
        self.gateLib = self.__loadData();
        self.gateCytometryLib = [];
        
    
    
    def __loadData(self):
        with open(self.fileLocation, 'r') as jsonFile:
            data = json.load(jsonFile);
        return data;   
      
    
  
        
    def getGateLib(self):
        if (len(self.gateLib) == 0):
            self.__loadData();        
        
        return self.gateLib;

    def getGateCytometryLib(self):
        if (len(self.gateCytometryLib) == 0):
            
            self.gateCytometryLib = [];
    
            for dic in self.gateLib:
                if (dic['collection'] == 'gate_cytometry'):
                    self.gateCytometryLib.append(dic);    
        
        
        return self.gateCytometryLib;
    
    
    def getResponseCurveParameters(self, gateIdentifier):
        parameters = {};
        for dic in self.gateLib:
            if (dic['collection'] == 'response_functions'):
                if (dic['gate_name'] == gateIdentifier):
                    for pair in dic['parameters']:
                       parameters[pair['name']] = pair['value']; 
        
        return parameters;
    
    


