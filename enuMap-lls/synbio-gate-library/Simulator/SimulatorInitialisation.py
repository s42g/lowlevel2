# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 12:50:32 2019

@author: Erik Kubaczka
"""

"""
    !!!
    Attention: The proposed circuit representation can only be used for combinational circuits
    !!!

    Steps of Initialisation
    1. Load structure.json
    2. Retrieve circuit
    3. 
"""

import numpy as np;
import json;
import pickle;
import parser;
import multiprocessing;
import SimulatorHelper;

VERSION_NUMBER = "1.0";




"""
    Unterstützung für eingebaute Response Functions wie Hill Curves hinzufügen (schneller evaluierbar).
    Wenn unbekannte Funktonen vorliegen, dann Parsen mit 
    def load_expression(source_string):
        st = parser.expr(source_string)
        return st, st.compile()
    und anschließend eval(st.compiler()) anstatt direkt eval(source_string).
"""
def generateResponseFunctions(gateLib):
    def load_expression(source_string):
        st = parser.expr(source_string)
        return st.compile()
    
    
    responseFunctions = {gate['identifier'] : 0 for gate in gateLib};

    for gate in gateLib:
        if (gate["biorep"]["response_function"]["type"] == "INHIBITORY_HILL_EQUATION" and True):
            # If native, the simulator makes use of the built in functions and does not evaluate the the given code for not natively supported expressions
            
            responseFunctions[gate["identifier"]] = {"nativ": True, "type":gate["biorep"]["response_function"]["type"]};
        else:
            responseFunctions[gate["identifier"]] = {"nativ": False, "expression": load_expression(gate["biorep"]["response_function"]["equation"])};

        responseFunctions[gate["identifier"]]["parameters"] = gate["biorep"]["response_function"]["parameters"];    
        responseFunctions[gate["identifier"]]["particle_parameters"] = list(gate["biorep"]["particles"].keys());
        
        
    return responseFunctions;


"""
This method currently only works for one parameter!
"""    
def prepareParameters(gateLib, n, parameter):
    def generateParticles(k, n):
        sigma_k = 0.8;        
        mu_k = np.log(k) - 0.5 * sigma_k**2;
        return np.random.lognormal(mu_k, sigma_k, n);
    
    
    particlesLib = {"number_of_particles" : n};
    for gate in gateLib:
        
        rawParticles = gate['biorep']['particles'][parameter];
        if (len(rawParticles) > 0):
            if (len(rawParticles) < n):
                raise Exception('The number of necessary particles should not exceed the number of particles provided within the library. Number of provided particles %d. Condition mismatch %d <= %d' % (len(rawParticles), n, len(rawParticles)))
            if (n < 0):
                raise Exception('The number of necessary particles should be greater than zero!');
        
            np.random.shuffle(rawParticles);
            particles = np.zero(n);
            for iX in range(n):
                particles[iX] = rawParticles[iX];
        else:
            particles = {parameter : generateParticles(gate['biorep']['response_function']["parameters"][parameter], n)};
        
        particlesLib[gate['identifier']] = particles;
    return particlesLib;
    

    
    
#   This method checks whether the circuit is represented in the right order, 
#   thus the gates are in the order corresponding to the calculation.
def isValid(nodes, edges):
    i = 0;
    for node in nodes:
        sources = getSources(edges, node['id']);
        
        for source in sources:
            #print(source, nodes[i:] );
            if (source in [nod['id'] for nod in nodes[i:]]):
                return False;
        i += 1;
        
    return True;


"""
This method returns a list of node IDs, which is sorted in the order of evaluation for the simulation.
"""
def createValidNodeIDList(nodes, edges):
    def allSourcesPresent(edges, nodeID, validNodeIDs):
         targetSources = getSources(edges, target);
         for targetSource in targetSources:
            if (not targetSource in validNodeIDs):
                return False;
         return True;   
    
    validNodeIDs = [];
    
    nodeSets = getSets(nodes);
    sortedInputs = sorted(nodeSets['INPUT'], key=lambda d: d['id']);
    
    for inVal in sortedInputs:
        validNodeIDs.append(inVal['id']);
        
    i = 0;
    while (len(validNodeIDs) != len(nodes)):
        targets = getTargets(edges, validNodeIDs[i]);
        for target in targets:
            if (not target in validNodeIDs):
                if (allSourcesPresent(edges, target, validNodeIDs)):
                    validNodeIDs.append(target);
            
                    
        i += 1;
            
    
    return validNodeIDs;


def sortNodes(validNodeIDs, nodeDict):
    validNodes = [];
    for validNodeID in validNodeIDs:
        validNodes.append(nodeDict[validNodeID]);
    
    return validNodes;   

def getNodeDict(nodes):
    nodeDict = {node['id']: node for node in nodes};
    return nodeDict;
    

def getSets(nodes):
    graphSet = {"INPUT": [], "LOGIC": [], "OUTPUT": []};
    for node in nodes:
        graphSet[node['type']].append(node);        
        
    return graphSet;
    

def getTargets(edges, sourceID):
    targets = [];
    for edge in edges:
        if (edge['source'] == sourceID):
            targets.append(edge['target']);
    return targets;


def getSources(edges, targetID):
    sources = [];
    for edge in edges:
        if (edge['target'] == targetID):
            sources.append(edge['source']);
    return sources;


def generateCircuit(nodeDict, edges, validNodeIDs):
    circuit = {nodeID:getTargets(edges, nodeID) for nodeID in validNodeIDs};
    return circuit;
    
def generateTruthTable(inputIDs, outputIDs, outputTruthTable, inputSpecification):
    def getCombination(val, numberOfInputs):
        combination = bin(val).replace("0b", "");
        missingZeros = numberOfInputs - len(combination);
        for i in range(missingZeros):
            combination = "0" + combination;
            
        return combination;
    
    def getBioVals(inputIDs, combination, inputSpecification):
        i = 0;
        #bioVals = [];
        bioVals = {};
        for inputID in inputIDs:
            #bioVals.append(inputSpecification[inputID][str(combination[i])]);
            bioVals[inputID] = inputSpecification[inputID][str(combination[i])];
            i += 1;
            
        return bioVals;
    
    sortedInputIDs = sorted(inputIDs);
    sortedOutputIDs = sorted(outputIDs);
    
    inputIDs = {sortedInputIDs[val]:val for val in range(len(sortedInputIDs))};
    outputIDs = {sortedOutputIDs[val]:val for val in range(len(sortedOutputIDs))};
    
    
    
    truthTable = {      "input_IDs" : inputIDs,
                        "output_IDs" : outputIDs,
                        "inputs" : {"input_" + str(iVal) : [] for iVal in range(len(outputTruthTable))},
                        "bio_inputs" : {"input_" + str(iVal) : [] for iVal in range(len(outputTruthTable))},
                        "outputs" : {"output_" + str(iVal) : [] for iVal in range(len(outputTruthTable))}
                        };
                  
 
    for i in range(len(outputTruthTable)):
        combination = getCombination(i, len(inputIDs));
        truthTable["inputs"]["input_" + str(i)] = [int(c) for c in combination];
        truthTable["outputs"]["output_" + str(i)] =  {sortedOutputIDs[0] : outputTruthTable[i]}; # Only works for just one circuit output
        truthTable["bio_inputs"]["input_" + str(i)] = getBioVals(inputIDs, combination, inputSpecification);
    

    return truthTable;

"""
    This specification shall be given within the library or any additional source.
"""
def generateInputSpecification(inputIDs):
    inputSpecification = {inID : {"0" : 0.01, "1" : 3} for inID in inputIDs};
    return inputSpecification;





jsonContent = SimulatorHelper.loadJSON("interfacing_test_data/structure.json");

structureData = jsonContent['graph'];

nodes = structureData['nodes'];
edges = structureData['edges'];
nodeDict = getNodeDict(nodes);
nodeSets = getSets(nodes);

validNodeIDs = createValidNodeIDList(nodes, edges);
validNodes = sortNodes(validNodeIDs, nodeDict);

#   print("SortedNodeList:", nodes2);

circuit = generateCircuit(nodeDict, edges, validNodeIDs);
circuitSimulation = {nodeID: 0 for nodeID in validNodeIDs};
circuitInfo =  {'NUMBER_OF_INPUTS': len(nodeSets['INPUT']),
                'NUMBER_OF_OUTPUTS': len(nodeSets['OUTPUT']),
                'NUMBER_OF_LOGICS': len(nodeSets['LOGIC'])};

inputIDs = [node['id'] for node in nodeSets['INPUT']];
outputIDs = [node['id'] for node in nodeSets['OUTPUT']]

inputSpecification = generateInputSpecification(inputIDs);
                
truthTable = generateTruthTable(inputIDs, outputIDs, jsonContent['truthtable'], inputSpecification);

gateLib = SimulatorHelper.loadJSON("../gate_library.json");
responseFunctions = generateResponseFunctions(gateLib);
responseFunctionParameters = prepareParameters(gateLib, 1000, 'K');


simData = { 'VERSION_NUMBER': VERSION_NUMBER,
            'circuit_info': circuitInfo,
            'circuit':circuit,
            'circuit_valid_node_list' : validNodeIDs,
            'circuit_simulation': circuitSimulation,
            'circuit_truthtable': truthTable,
            'circuit_response_functions': responseFunctions,
            'circuit_response_functions_particles' : responseFunctionParameters,
            };
           
simSpec = {    'CPU_COUNT' : multiprocessing.cpu_count(),
               'number_of_particles' : 1000};           
           
with open("simulation_data.pkl", "wb") as file:
    pickle.dump(simData, file);               
    
with open("simulation_specification.pkl", "wb") as file:
    pickle.dump(simSpec, file);       

try:    
    with open("simulation_specification.json", "r") as file:
        simSpecFileContent = file.read();
except FileNotFoundError:            
    with open("simulation_specification.json", "w") as file:
        json.dump(simSpec, file);
        
finally:
    zero = 0;
"""
END
"""
