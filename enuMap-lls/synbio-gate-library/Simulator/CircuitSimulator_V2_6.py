# -*- coding: utf-8 -*-
import numpy as np
import json
import parser
import multiprocessing
import scipy.stats as st
import ast
import sys
import matplotlib.pyplot as plt

"""
Created on Thu Jan 16 18:41:41 2020

@author: Erik Kubaczka
"""

"""
    !!!
    Attention: The proposed internal circuit representation can only be used for combinational circuits
    !!! 
"""

VERSION_NUMBER = "2.6"

def loadJSON(filePath):
    with open(filePath, 'r') as jsonFile:
        data = json.load(jsonFile)
    return data

def logHistWrapper(data):
    logHist(data=data, xmin=0.00001, xmax=100)   # , ymin = 0.0001, ymax = 10)


def logHist(data, xmin, xmax):  # , ymin, ymax):
    bins = np.logspace(np.log10(xmin), np.log10(xmax), 120)
    fig = plt.figure()
    plt.hist(data, bins)
    plt.gca().set_xscale('log')
    plt.show();


def logHistWrapper2(data1, data2):
    logHist2(data1=data1, data2=data2, xmin=0.0001, xmax=10)   # , ymin = 0.0001, ymax = 10)


def logHist2(data1, data2, xmin, xmax):  # , ymin, ymax):
    bins = np.logspace(np.log10(xmin), np.log10(xmax), 120)
    fig = plt.figure()
    plt.hist(data1, bins)
    plt.hist(data2, bins)
    plt.gca().set_xscale('log')
    plt.show();

def logHistWrapperSubplot(data1, data2):
    logHistSubplot(data1=data1, data2=data2, xmin=0.0001, xmax=10)   # , ymin = 0.0001, ymax = 10)


def logHistSubplot(data1, data2, xmin, xmax):  # , ymin, ymax):
    #print(data1, data2)
    bins = np.logspace(np.log10(xmin), np.log10(xmax), 120)
    fig = plt.figure()
    ax = plt.subplot(211)
    ax.title.set_text("Lowest ON")
    plt.hist(data1, bins)
    plt.gca().set_xscale('log')

    ax = plt.subplot(212)#, xscale="log")
    ax.title.set_text("Highest OFF")
    plt.hist(data2, bins)
    plt.gca().set_xscale('log')
    plt.draw()
    plt.show()

def circuitScore(dataON, dataOFF):
    score = 0;
    DIST = simContext["dist"]
    if ("kl" == DIST):  # Minimum of Kullback-Leibler Divergence
        mu_1 = np.mean(dataON)
        var_1 = np.var(dataON)
        mu_2 = np.mean(dataOFF)
        var_2 = np.var(dataOFF)

        score = min([0.5 * np.log(var_2 / var_1) + ((mu_1 - mu_2)**2 + var_1 - var_2) / (2 * var_2),
                     0.5 * np.log(var_1 / var_2) + ((mu_2 - mu_1)**2 + var_2 - var_1) / (2 * var_1)]);
    elif ("ws" == DIST):    # Wasserstein Distance
        score = st.wasserstein_distance(dataON, dataOFF)
    elif ("cello" == DIST):
        score = min(dataON) / max(dataOFF)
    elif ("ws-cello" == DIST):
        score = st.wasserstein_distance(dataON, dataOFF)
        score = score/np.median(dataOFF) + 1
    elif ("ws-cello-m" == DIST):
        score = st.wasserstein_distance(dataON, dataOFF)
        score = score/np.mean(dataOFF) + 1
    elif ("ws-log" == DIST):
        score = st.wasserstein_distance(np.log(dataON), np.log(dataOFF))
    elif ("ws-exp" == DIST):
        score = st.wasserstein_distance(np.exp(dataON), np.exp(dataOFF))
    elif ("ws-log-exp" == DIST):
        score = st.wasserstein_distance(np.log(dataON), np.log(dataOFF))
        score = np.exp(score)
    else:
        score = (np.mean(dataON) - np.mean(dataOFF))**2
    return score;

def initialiseSimulator(structureContent, gateLib, numberOfParticles):
    """
        Unterstützung für eingebaute Response Functions wie Hill Curves hinzufügen (schneller evaluierbar).
        Wenn unbekannte Funktonen vorliegen, dann Parsen mit 
        def load_expression(source_string):
            st = parser.expr(source_string)
            return st, st.compile()
        und anschließend eval(st.compiler()) anstatt direkt eval(source_string).
    """

    def generateResponseFunctions(gateLib):
        def load_expression(source_string):
            st = parser.expr(source_string)
            return st.compile()

        responseFunctions = {gate['identifier']: 0 for gate in gateLib}

        for gate in gateLib:
            if ((gate["biorep"]["response_function"]["type"] == "INHIBITORY_HILL_EQUATION" or "IMPLICIT_OR" == gate["biorep"]["response_function"]["type"])and True):
                # If native, the simulator makes use of the built in functions and does not evaluate the the given code for not natively supported expressions

                responseFunctions[gate["identifier"]] = {"native": True,
                                                         "type": gate["biorep"]["response_function"]["type"]}
            else:
                responseFunctions[gate["identifier"]] = {"native": False, "expression": load_expression(
                    gate["biorep"]["response_function"]["equation"])}

            responseFunctions[gate["identifier"]]["parameters"] = gate["biorep"]["response_function"]["parameters"]
            if ("particles" in gate["biorep"]):
                responseFunctions[gate["identifier"]]["particle_parameters"] = list(gate["biorep"]["particles"].keys())

        return responseFunctions

    """
    This method currently only works for one parameter!
    """

    def prepareParameters(gateLib, n):
        def generateParticles(k, n):
            if (n > 1):
                sigma_k = 0.8
                mu_k = np.log(k) - 0.5 * sigma_k ** 2
                return np.random.lognormal(mu_k, sigma_k, n)
            else:
                return k

        particlesLib = {"number_of_particles": n}
        for gate in gateLib:
            # rawParticles = [] 
            if ("particles" in gate["biorep"]):
                particlesLib[gate['identifier']] = {}
                for parameter in gate['biorep']['particles']:
                    rawParticles = gate['biorep']['particles'][parameter]

                    if (len(rawParticles) > 0):
                        if (len(rawParticles) < n):
                            raise Exception(
                                 'The number of necessary particles should not exceed the number of particles provided within the library. Number of provided particles %d. Condition mismatch %d <= %d' % (
                                    len(rawParticles), n, len(rawParticles)))
                        if (n < 0):
                            raise Exception('The number of necessary particles should be greater than zero!')
                        #n = len(rawParticles)
                        simContext["maxNumberOfParticles"] = len(rawParticles); # TODO Dirty Quick Fix. Geht nur wenn die Anzahl aller Partikel gleich ist.
                        #np.random.shuffle(rawParticles)     # TODO Überdenken, ob es Sinn macht, dass die eingegeben Partikel geshuffelt werden.
                        particles = np.zeros(n)
                        for iX in range(len(rawParticles)):
                            particles[iX] = rawParticles[iX]

                    else:
                        particles = generateParticles(gate['biorep']['response_function']["parameters"][parameter], n)

                    particlesLib[gate['identifier']][parameter] = particles
        return particlesLib

    #   This method checks whether the circuit is represented in the right order,
    #   thus the gates are in the order corresponding to the calculation.
    def isValid(nodes, edges):
        i = 0
        for node in nodes:
            sources = getSources(edges, node['id'])

            for source in sources:
                # print(source, nodes[i:] ) 
                if (source in [nod['id'] for nod in nodes[i:]]):
                    return False
            i += 1

        return True

    """
    This method returns a list of node IDs, which is sorted in the order of evaluation for the simulation.
    """

    def createValidNodeIDList(nodes, edges):
        def allSourcesPresent(edges, nodeID, validNodeIDs):
            targetSources = getSources(edges, target)
            for targetSource in targetSources:
                if (not targetSource in validNodeIDs):
                    return False
            return True

        validNodeIDs = []

        nodeSets = getSets(nodes)
        sortedInputs = sorted(nodeSets['INPUT'], key=lambda d: d['id'])

        for inVal in sortedInputs:
            validNodeIDs.append(inVal['id'])

        i = 0
        while (len(validNodeIDs) != len(nodes)):
            targets = getTargets(edges, validNodeIDs[i])
            for target in targets:
                if (not target in validNodeIDs):
                    if (allSourcesPresent(edges, target, validNodeIDs)):
                        validNodeIDs.append(target)

            i += 1

        return validNodeIDs

    def sortNodes(validNodeIDs, nodeDict):
        validNodes = []
        for validNodeID in validNodeIDs:
            validNodes.append(nodeDict[validNodeID])

        return validNodes

    def getNodeDict(nodes):
        nodeDict = {node['id']: node for node in nodes}
        return nodeDict

    def getSets(nodes):
        graphSet = {"INPUT": [], "LOGIC": [], "OUTPUT": []}
        for node in nodes:
            graphSet[node['type']].append(node)

        return graphSet

    def getTargets(edges, sourceID):
        targets = []
        for edge in edges:
            if (edge['source'] == sourceID):
                targets.append(edge['target'])
        return targets

    def getSources(edges, targetID):
        sources = []
        for edge in edges:
            if (edge['target'] == targetID):
                sources.append(edge['source'])
        return sources

    def generateCircuit(nodeDict, edges, validNodeIDs):
        circuit = {nodeID: getTargets(edges, nodeID) for nodeID in validNodeIDs}
        return circuit

    def generateTruthTable(inputIDs, outputIDs, outputTruthTable, inputSpecification):
        def getCombination(val, numberOfInputs):
            combination = bin(val).replace("0b", "")
            missingZeros = numberOfInputs - len(combination)
            for i in range(missingZeros):
                combination = "0" + combination

            return combination

        def getBioVals(inputIDs, combination, inputSpecification):
            i = 0
            # bioVals = [] 
            bioVals = {}
            for inputID in inputIDs:
                # bioVals.append(inputSpecification[inputID][str(combination[i])]) 
                bioVals[inputID] = inputSpecification[inputID][str(combination[i])]
                i += 1

            return bioVals

        sortedInputIDs = sorted(inputIDs)
        sortedOutputIDs = sorted(outputIDs)

        inputIDs = {sortedInputIDs[val]: val for val in range(len(sortedInputIDs))}
        outputIDs = {sortedOutputIDs[val]: val for val in range(len(sortedOutputIDs))}

        truthTable = {"input_IDs": inputIDs,
                      "output_IDs": outputIDs,
                      "inputs": {"input_" + str(iVal): [] for iVal in range(len(outputTruthTable))},
                      "bio_inputs": {"input_" + str(iVal): [] for iVal in range(len(outputTruthTable))},
                      "outputs": {"output_" + str(iVal): [] for iVal in range(len(outputTruthTable))}
                      }

        for i in range(len(outputTruthTable)):
            combination = getCombination(i, len(inputIDs))
            truthTable["inputs"]["input_" + str(i)] = [int(c) for c in combination]
            truthTable["outputs"]["output_" + str(i)] = {
                sortedOutputIDs[0]: outputTruthTable[i]}   # Only works for just one circuit output
            truthTable["bio_inputs"]["input_" + str(i)] = getBioVals(inputIDs, combination, inputSpecification)

        return truthTable

    """
        This specification shall be given within the library or any additional source.
    """

    def generateInputSpecification(inputIDs):
        inputSpecification = {inID: {"0": 0.01, "1": 3} for inID in inputIDs}
        return inputSpecification

    # Only applicable for three inputs
    def generateCelloInputSpecification(inputIDs):
        inputSpecification = {}
        inputSpecification["a"] = {"0" : 0.0034, "1" : 2.8}
        inputSpecification["b"] = {"0" : 0.0013, "1" : 4.4}
        inputSpecification["c"] = {"0" : 0.0082, "1" : 2.5}
        return inputSpecification

    #jsonContent = loadJSON(structurePath)
    #jsonContent = structureFileContent
    #structureData = jsonContent['graph']
    simContext["numberOfParticles"] = simContext["maxNumberOfParticles"];

    nodes = structureContent['graph']['nodes']
    edges = structureContent['graph']['edges']
    nodeDict = getNodeDict(nodes)
    nodeSets = getSets(nodes)

    validNodeIDs = createValidNodeIDList(nodes, edges)
    validNodes = sortNodes(validNodeIDs, nodeDict)

    #   print("SortedNodeList:", nodes2) 

    circuit = generateCircuit(nodeDict, edges, validNodeIDs)
    circuitSimulation = {nodeID: 0 for nodeID in validNodeIDs}
    circuitInfo = {'NUMBER_OF_INPUTS': len(nodeSets['INPUT']),
                   'NUMBER_OF_OUTPUTS': len(nodeSets['OUTPUT']),
                   'NUMBER_OF_LOGICS': len(nodeSets['LOGIC'])}

    inputIDs = [node['id'] for node in nodeSets['INPUT']]
    outputIDs = [node['id'] for node in nodeSets['OUTPUT']]

    if (len(inputIDs) == 3):
        inputSpecification = generateCelloInputSpecification(inputIDs)
    else:
        inputSpecification = generateInputSpecification(inputIDs)

    truthTableString = structureContent['truthtable']
    truthTableString = truthTableString[len(truthTableString)::-1]

    truthTable = generateTruthTable(inputIDs, outputIDs, truthTableString, inputSpecification)

    #gateLib = loadJSON(gateLibPath)
    responseFunctions = generateResponseFunctions(gateLib)

    responseFunctionParameters = prepareParameters(gateLib, numberOfParticles)

    simData = {'VERSION_NUMBER': VERSION_NUMBER,
               'circuit_info': circuitInfo,
               'circuit': circuit,
               'circuit_valid_node_list': validNodeIDs,
               'circuit_simulation': circuitSimulation,
               'circuit_truthtable': truthTable,
               'circuit_response_functions': responseFunctions,
               'circuit_response_functions_particles': responseFunctionParameters,
               }

    simSpec = {'CPU_COUNT': multiprocessing.cpu_count(),
               'number_of_particles': numberOfParticles}

    return simData, simSpec


def startSimulation(assignment, simData, simSpec):

    # TODO Remove this method from startSimulation and migrate to Lambda Expressions
    def insertNativeResponseFunctions(responseFunctions):
        keyList = responseFunctions.keys()
        for key in keyList:
            if (responseFunctions[key]["native"]):
                if (responseFunctions[key]["type"] == "INHIBITORY_HILL_EQUATION"):
                    responseFunctions[key]["equation"] = inhibitoryHillEquation
                elif (responseFunctions[key]["type"] == "IMPLICIT_OR"):
                    responseFunctions[key]["equation"] = nativeImplicitOr



        return responseFunctions

    def startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles, circuitSim):

        length = min([simData["circuit_response_functions_particles"]["number_of_particles"], simContext["numberOfParticles"]])
        usermodePrint("Used particles:" + str(length))

        results = {}
        for outID in truthTable["output_IDs"]:
            results[outID] = {"0": {}, "1": {}}

            for out in truthTable["outputs"]:
                # print(out)
                results[outID][truthTable["outputs"][out][outID]][out] = np.zeros(length)

        for iX in range(length):
            for node in nodeOrder:
                try:
                    respFunc = responseFunctions[node]
                    for parameter in respFunc["particle_parameters"]:
                        #print( simData["circuit_response_functions_particles"][assignment[node]][parameter])
                        responseFunctions[assignment[node]]["parameters"][parameter] = simData["circuit_response_functions_particles"][assignment[node]][parameter][iX]

                except KeyError:
                    zero = 0
                    # node is an Input or Output
            # print(responseFunctions["NOT_0"]) 
            simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iX)

        return results

    """
    For a given set of parameters, this method adds the circuit outputs to the corresponding sets.
    The corresponding ID and either to the True or False set.
    """

    def simulateCircuit(nodeOrder, circuit, assignment, truthTable, responseFunctions, circuitSim, results, iteration):
        # IDmap = truthTable["input_IDs"] 
        outputIDs = truthTable["output_IDs"].keys()
        #inputIDs = truthTable["input_IDs"].keys()
        # print(nodeOrder) 
        bioInputs = truthTable["bio_inputs"]
        # print(len(truthTable["outputs"])) 
        # Durchläuft die verschiedenen Eingaben

        for iX in range(len(truthTable["outputs"])):
            # print(bioInputs["input_%d" % iX], truthTable["outputs"]["output_%d" % iX]) 

            circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs["input_%d" % iX],
                                        dict(circuitSim), responseFunctions)
            # circuitVals = getCircuitVal(nodeOrder, circuit, assignment, truthTable, bioInputs[out], dict(circuitSim), responseFunctions) 
            """        print(iX, "\n", responseFunctions["NOT_0"]["parameters"], "\n", circuitVals, "\n") 
            """
            # Store the circuit outputs in the result list
            #if (iX == 0):
            #    print(circuitVals)
            debugPrint("circuitVals: " + str(circuitVals))

            for outputID in outputIDs:

                results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iteration] = circuitVals[outputID]
                #print("output_%d" % iX, truthTable["outputs"]["output_%d" % iX][outputID], results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iteration])
                # results[outputID][truthTable["outputs"]["output_%d" % iX][outputID]]["output_%d" % iX][iX].append(circuitVals[outputID]) 

        # print("") 

    """
    Evaluates a complete circuit according to the specified nodeOrder
    The parameters provided by the responseFunctions are used.
    """

    def getCircuitVal(nodeOrder, circuit, assignment, truthTable, circuitInputs, circuitVals, responseFunctions):
        for node in nodeOrder:
            if (node in truthTable["input_IDs"].keys()):
                # The value of the current node is given by the circuit inputs
                circuitVals[node] = circuitInputs[node]
            elif (node in truthTable["output_IDs"].keys()):
                # Theoretically we are done and can return
                # A final operation to the output value can be applied. This is currently not the case.
                circuitVals[node] = circuitVals[node]
            else:
                bioGate = assignment[node]
                circuitVals[node] = evaluateGate(circuitVals[node], responseFunctions[bioGate])

            for child in circuit[node]:
                circuitVals[child] += circuitVals[node]
                # print(node, circuitVals[node])
        # print("OUTPUT", circuitVals["NOT_1"]) 
        # print("") 
        return circuitVals

    """
    Evaluates a gate for the given response function and the corresponding parameters
    """

    def evaluateGate(val, responseFunction):
        if (responseFunction["native"]):
            return responseFunction["equation"](val, responseFunction["parameters"])
        else:
            return -1   # Not implemented yet...

    """
        Implement the natively supported response functions
    """

    def inhibitoryHillEquation(x, parameters):
        # print(parameters)
        K = parameters["K"]
        n = parameters["n"]
        ymin = parameters["ymin"]
        ymax = parameters["ymax"]
        return ymin + (ymax - ymin) / (1 + (x / K) ** n)

    def nativeImplicitOr(x, parameters):
        return x;

    def determineScores(simRes):
        minON = {}
        maxOFF = {}

        for key in simRes:
            simRes[key]["SCORE"] = -1
            #print("Len ons:", len(simRes[key]["1"]))
            #print("Len offs:", len(simRes[key]["0"]))
            debugPrint("ON: " + str(simRes[key]["1"]))
            debugPrint("OFF: " + str(simRes[key]["0"]))
            for outON in simRes[key]["1"]:
                for outOFF in simRes[key]["0"]:

                    if (np.median(simRes[key]["1"][outON]) > np.median(simRes[key]["0"][outOFF])):
                        # Important for the cello metric, that on is data 1 and off is data 2
                        score = circuitScore(simRes[key]["1"][outON], simRes[key]["0"][outOFF])
                        #score = st.wasserstein_distance(simRes[key]["0"][outOFF], simRes[key]["1"][outON])
                    else:
                        score = -1;

                    #print(score);
                    #logHistWrapperSubplot(simRes[key]["1"][outON], simRes[key]["0"][outOFF])
                    if (simRes[key]["SCORE"] == -1 or simRes[key]["SCORE"] > score):
                        simRes[key]["SCORE"] = score
                        minON[key] = simRes[key]["1"][outON]
                        maxOFF[key] = simRes[key]["0"][outOFF]
        #print(visualise);
        #print(minON[key])
        #print(maxOFF[key])
        if (simContext["visualise"]):
            for key in minON:
                print("Smallest Distance for", key)
                logHistWrapperSubplot(minON[key], maxOFF[key])



    responseFunctions = simData["circuit_response_functions"]
    insertNativeResponseFunctions(responseFunctions)

    nodeOrder = simData["circuit_valid_node_list"]
    circuit = simData["circuit"]
    truthTable = simData["circuit_truthtable"]
    circuitVals = simData["circuit_simulation"]
    responseFunctions = simData["circuit_response_functions"]
    particles = simData["circuit_response_functions_particles"]



    """
        Perform Simulation with the retrieved commands.
    """

    simRes = startSimulation(nodeOrder, circuit, assignment, truthTable, responseFunctions, particles,
                             dict(circuitVals))

    #debugPrint("simRes: \n" + str(simRes))
    """
        Determine Scores of all output variables
    """
    # print(simRes) 
    determineScores(simRes)
    """
    for outID in simRes:
        for offID in simRes[outID]["0"]:
            print(offID, np.mean(simRes[outID]["0"][offID]), np.var(simRes[outID]["0"][offID]))
        for onID in simRes[outID]["1"]:
            print(onID, np.mean(simRes[outID]["1"][onID]), np.var(simRes[outID]["1"][onID]))
    """

    usermodePrint("Circuit Scores: ")
    #print("Circuit Score:", simRes["O"]["SCORE"])
    for outputID in truthTable["output_IDs"].keys():
        print(outputID, simRes[outputID]["SCORE"])


##############################################################################

"""
    Is used for each simulation as well as for the initialisation.
"""


def parseInput(inputText):
    def prepareString(prepText):
        prepText = " ".join(prepText.split())
        prepText = prepText.replace(" =", "=")
        prepText = prepText.replace("= ", "=")
        prepText = prepText.replace(" :", ":")
        prepText = prepText.replace(": ", ":")
        prepText = prepText.replace(" ,", ",")
        prepText = prepText.replace(", ", ",")
        return prepText

    def parseBool(inVal):
        curVal = inVal.lower();
        if (curVal == "0" or curVal == "false" or curVal == "f"):
            curVal = 0;
        elif(curVal == "1" or curVal == "true" or curVal == "t"):
            curVal = 1;
        return curVal

    inputText = prepareString(inputText)
    instruction = inputText.split()   # Split at Whitespace!
    # print(instruction) 
    if (len(instruction) > 0):
        cmd = instruction[0]
        specDict = {}
        # print(inText) 
        for elem in instruction[1:]:
            field, val = elem.split("=")
            # TODO Include all aspects which can be defined via interface ("s_path", "lib_path" etc. for example)
            #print(len(val), val, file=sys.stderr)
            # print(field, val) 
            if (field == "n"):
                specDict["numberOfParticles"] = int(val)
            elif(field == "nMax"):
                specDict["maxNumberOfParticles"] = int(val)
            elif (field == "threads"):
                specDict[field] = int(val)
            elif (field == "lib_path"):
                specDict["gate_lib"] = loadJSON(val)
            elif (field == "s_path"):
                specDict["structure"] = loadJSON(val)
            elif (field == "structure"):
                specDict["structure"] = ast.literal_eval(val)
            elif(field == "a_path"):
                specDict["assignment"] = loadJSON(val)
            elif (field == "assignment"):
                specDict["assignment"] = ast.literal_eval(val)
            elif (field == "visualise"):
                specDict[field] = bool(parseBool(val))
            elif (field == "usermode"):
                specDict[field] = bool(parseBool(val))
            elif (field == "debug"):
                specDict[field] = bool(parseBool(val))
            elif (field == "dist"):
                specDict[field] = val.lower()
            else:
                specDict[field] = val
        return cmd, specDict
    else:
        return "", {}



def usermodePrint(text):
    if (simContext["usermode"]):
        print(text, file=sys.stdout)


def debugPrint(text):
    if (simContext["debug"]):
        print(text, file=sys.stderr)

def updateSimContext(specDict):
    for key in specDict:
        if (key in simContext):
            simContext[key] = specDict[key]





print("Start Simulator Initialisation")
simContext = {}
simContext["numberOfParticles"] = 1000;
simContext["maxNumberOfParticles"] = 1000;
simContext["visualise"] = False;
simContext["usermode"] = False;
simContext["debug"] = False;
simContext["structure"] = "NULL";
simContext["assignment"] = "NULL";
simContext["gate_lib"] = "NULL";
simContext["threads"] = 1;
simContext["dist"] = "ws-log-exp";

debugPrint(str(sys.argv))

cmd, specDict = parseInput(" ".join(sys.argv))
updateSimContext(specDict)

# Set the maximum number of particles as default
simContext["numberOfParticles"] = simContext["maxNumberOfParticles"];


if (simContext["structure"] != "NULL" and simContext["gate_lib"] != "NULL"):

    simData, simSpec = initialiseSimulator(simContext["structure"], simContext["gate_lib"], simContext["maxNumberOfParticles"])
    usermodePrint("Simulator Initialised and ready to use.")
    usermodePrint("Dont forget to configure the assignment (append \"start\" with \"a_path=...\", the corresponding path, or directly by the assignment, by making use of\"assignment=...\"=  and the number of particles (append \"n=...\" to \"start\")")
    usermodePrint("Start simulation with \"start\"")
else:
    usermodePrint("You need to set the circuit structure and the library path to initialise the simulation!")
    usermodePrint("This can be done by appending the command \"settings\" by \"s_path=...\" and the corresponding file path and next to this, the path to the gate lib with \"lib_path=...\"")
    structurePath = "NULL"
    gateLibPath = "NULL"
    configured = False
    inText = input("define settings: ")
    while (inText != "exit" and not (configured)):
        cmd, specDict = parseInput(inText)
        debugPrint(cmd)
        if (cmd == "settings"):
            updateSimContext(specDict)

            if (simContext["structure"] != "NULL" and simContext["gate_lib"] != "NULL"):
                simData, simSpec = initialiseSimulator(simContext["structure"], simContext["gate_lib"], simContext["maxNumberOfParticles"])

                usermodePrint("Configuration finished")
                usermodePrint("If desired, this configuration can be changed later on with the same comands.")
                configured = True

        if (not (configured)):
            inText = input("define settings: ")

    if (inText == "exit"):
        sys.exit("You stopped the simulator.")



inText = input("ready: ")

specDict = {}
simSpec["number_of_particles"] = simContext["numberOfParticles"]
iX = 0

while (inText != "exit"):
    cmd, specDict = parseInput(inText)

    if (cmd == "start"):
        updateSimContext(specDict)


        if (simContext["assignment"] != "NULL"):
            # print(assignment)
            usermodePrint("Simulation started")
            startSimulation(assignment=simContext["assignment"], simData=simData, simSpec=simSpec)
            debugPrint("Iteration: " + str(iX))
            iX += 1
        else:
            print("ERROR: Assignment needs to be defined at least once by adding \"assignment={...}\" or \"a_path=dir/file.json\" to after start.")

    if (cmd == "settings"):
        updateSimContext(specDict)

        simData, simSpec = initialiseSimulator(simContext["structure"], simContext["gate_lib"], simContext["maxNumberOfParticles"])
        usermodePrint("Simulator is reinitialised")

    inText = input("ready: ")