# -*- coding: utf-8 -*-
"""
Created on Thu Jan  2 17:12:01 2020

@author: Erik Kubaczka
"""

import json;

def loadJSON(filePath):
    with open(filePath, 'r') as jsonFile:
        data = json.load(jsonFile);
    return data;