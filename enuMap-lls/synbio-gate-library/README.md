Synbio Gate Library
===================

## Grundstrukur

Liste von JSON-Objekten, die individuelle Gates darstellen.

## Eigenschaften der Gate-Objekte

| Eigenschaft           	 | Beschreibung |
| ------                	 | ------ |
| primitiveIdentifier   	 | Identifier-String der Gate-Klasse |
| identifier            	 | Identifier-String des individuellen Gates |
| group						 | Identifier-String der Guppe, der das Gate zugehörig ist. Eine Gruppe kann pro Schaltung nur einmal instanziiert werden. |
| expression            	 | algebraischer Ausdruck der booleschen Funktion (Syntax siehe [LogicNG](https://github.com/logic-ng/LogicNG/blob/master/src/main/java/org/logicng/io/parsers/PropositionalParser.java)) |
| biorep                	 | Repräsentation der biologischen Eigenschaften eines individuellen biologischen Gates. |
| biorep - response_function | Enthält sowohl die Funktion ["equation"] die die Steady State Charakteristik beschreibt, als auch die dazugehörigen Parameter ["ymax", "ymin", "K", "n"]. Zusätzlich sind für die Parameter noch Partikel ["particles"] vorhanden. Zusätzlich gibt "type" den Typ der Response Function an und überschreibt die Expression, fals der Simulator diesen bestimmten Typ nativ unterstützt.   |



Genetic Circuit Simulator (CircuitSimulator_V2.py)
===================

## Aufruf
Der Simulator wird ausgeführt mit:
    `python CircuitSimulator_V2.py [s_path=(pathToCircuitStructureFile)] [lib_path=(pathToGateLibFile)]`
    
Die Argumente `s_path` und `lib_path` sind Optional und spezifizieren welche Circuit Struktur (.json) und welche Gate Library (.json) zu nutzen ist.
Werden diese Argumente nicht übergeben, so können sie nachträglich über "settings" spezifiziert werden.

## Komandos
| Kommando                          | Beschreibung |
| ------                	        | ------ |
| `exit`                              | Der Simulator wird beendet |
| `settings [lib_path=...] [s_path=...]` | Der Pfad zur Gate Library und/oder der Pfad zur Circuit Struktur werden überschrieben. |
| `start [n=...] [assignment=...] [a_path=...]` | Startet eine Simulation mit dem spezifizierten Assignment, welches entweder direkt als Python dict mittels `assignment=` übergeben wird oder als Datei über den entsrpechenden Dateipfad mittels `a_path=`. Zusätzlich dazu kann die Anzahl der betrachteten Partikel über `n=` gesetzt werden. Ist einer dieser Werte nicht angegeben, so wird der vorherige Wert genutzt. |


## Beispiel
Initialisierung des Simulators
    **`python CircuitSimulator_V2.py s_path=interfacing_test_data/structure.json lib_path=../gate_library.json`**
    
Start der Simulation
    **`start n=1000 a_path=interfacing_test_data/assignment_1.json`**

oder
    **`start n=1000 assignment={"NOT_4" : "NOT_0", "NOT_6" : "NOT_1", "NOR2_1" : "NOR2_1", "NOT_7" : "NOT_2", "NOR2_0" : "NOR2_0", "NOR2_3" : "NOR2_3", "NOR2_2" : "NOR2_2", "NOR2_5" : "NOR2_4"}`**
    
## Info
Die Datei `simulation_specification.json` wird nicht mehr benötigt.

#### Parallelisierung
Es kann zwar optional das Feld `threads=...` übergeben werden, welches in einer späteren Version zur Angabe des parallelisierungsgrades genutzt werden soll. Aktuell findet es noch keine Anwendung!

## Releases
| Release                           | Changelog |
| ------                	        | ------ |
| V2.4                              | - Input concentrations according to Cello |
| V2.3                              | - Support of implicit OR                  |

