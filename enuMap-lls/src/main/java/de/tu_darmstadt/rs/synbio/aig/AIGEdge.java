package de.tu_darmstadt.rs.synbio.aig;

import org.logicng.formulas.Variable;

public class AIGEdge {

    private boolean isInverted;
    private Variable variable;

    public AIGEdge(Variable variable, boolean isInverted) {
        super();
        this.variable = variable;
        this.isInverted = isInverted;
    }

    public boolean isInverted() {

        return isInverted;
    }

    public Variable getVariable() {
        return variable;
    }
}
