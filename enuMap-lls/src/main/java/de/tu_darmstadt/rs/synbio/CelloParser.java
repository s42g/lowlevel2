package de.tu_darmstadt.rs.synbio;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.circuit.Wire;
import de.tu_darmstadt.rs.synbio.simulation.SimulatorInterface;
import org.logicng.formulas.Variable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class CelloParser {

    private final static String outdir = "cello_output";
    private static Map<String, String> assignmentMap;

    public static void main(String[] args) throws Exception {

        File dir = new File(args[0]);
        File[] dirListing = dir.listFiles();

        Arrays.sort(dirListing, (f1, f2) -> Long.valueOf(f1.lastModified()).compareTo(f2.lastModified()));

        initAssignmentMap();

        for (File file : dirListing) {

            List<NetlistEntry> netlist = new ArrayList<>();

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {

                if (!line.equals("") && !line.startsWith("-"))
                    netlist.add(new NetlistEntry(line));
                line = reader.readLine();
            }
            reader.close();

            Map<Gate, NetlistEntry> idMap = new HashMap<>();

            Circuit circuit = new Circuit("cello_circuit");

            // order?
            char input = 'a';
            for (NetlistEntry entry : netlist) {
                if (entry.identifier.equals("INPUT")) {
                    Gate inputBuffer = new Gate(String.valueOf(input), String.valueOf(input), Gate.Type.INPUT);
                    idMap.put(inputBuffer, entry);
                    circuit.addVertex(inputBuffer);
                    input++;
                }
            }

            Map<String, String> assignment = new HashMap<>();

            for (NetlistEntry entry : netlist) {

                Gate gate;

                switch (entry.identifier) {
                    case "NOT":
                        gate = new Gate("~x", "NOT", entry.gateIdentifier, "");
                        assignment.put(entry.gateIdentifier, "NOT_" + assignmentMap.get(entry.gateIdentifier));
                        idMap.put(gate, entry);
                        circuit.addVertex(gate);
                        break;
                    case "NOR":
                        gate = new Gate("~(x|y)", "NOR2", entry.gateIdentifier, "");
                        assignment.put(entry.gateIdentifier, "NOR2_" + assignmentMap.get(entry.gateIdentifier));
                        idMap.put(gate, entry);
                        circuit.addVertex(gate);
                        break;
                    case "OUTPUT_OR":
                        gate = new Gate("(x|y)", "OR2", entry.gateIdentifier, "");
                        assignment.put(entry.gateIdentifier, assignmentMap.get(entry.gateIdentifier));
                        idMap.put(gate, entry);
                        circuit.addVertex(gate);
                        break;
                }
            }

            for (Gate gate : idMap.keySet()) {

                if (idMap.get(gate).connectedIndices == null)
                    continue;

                int i = 0;

                for (Variable inputVar : gate.getExpression().variables()) {

                    int connectedGateId = idMap.get(gate).connectedIndices[i];
                    Gate connectedGate = idMap.keySet().stream().filter(g -> idMap.get(g).index == connectedGateId).findAny().get();

                    circuit.addEdge(connectedGate, gate, new Wire(inputVar));

                    i++;
                }
            }

            Gate lastGate = circuit.vertexSet().stream().filter(g -> circuit.outgoingEdgesOf(g).isEmpty()).findAny().get();

            Gate outputBuffer = new Gate("x", "O", Gate.Type.OUTPUT);
            circuit.addVertex(outputBuffer);

            circuit.addEdge(lastGate, outputBuffer, new Wire(outputBuffer.getExpression().variables().first()));

            String tt = circuit.getTruthTable().toString();

            circuit.print(new File(outdir, tt + "_structure.dot"));
            circuit.save(new File(outdir, tt + "_structure.json"));

            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(outdir, tt + "_assignment.json"), assignment);

            SimulatorInterface simulator = new SimulatorInterface(new File("/home/schladt/synbio/enuMap/synbio-gate-library/Simulator"),
                    "CircuitSimulator_V2_4.py", "nMax=1", "dist=cello n=1", new File("/home/schladt/synbio/enuMap/synbio-gate-library/gate_libs",
                    "generated_gate_library_mod.json"));

            simulator.initSimulation(circuit);
            double score = 0;//simulator.simulate(assignment);

            System.out.println(tt + ": " + score);
        }
    }

    private static void initAssignmentMap() {

        assignmentMap = new HashMap<>();
        assignmentMap.put("YFP", "OR_IMPL");
        assignmentMap.put("A1_AmtR", "0");
        assignmentMap.put("B1_BM3R1", "1");
        assignmentMap.put("B2_BM3R1", "2");
        assignmentMap.put("B3_BM3R1", "3");
        assignmentMap.put("E1_BetI", "4");
        assignmentMap.put("F1_AmeR", "5");
        assignmentMap.put("H1_HlyIIR", "6");
        assignmentMap.put("I1_IcaRA", "7");
        assignmentMap.put("L1_LitR", "8");
        assignmentMap.put("N1_LmrA", "9");
        assignmentMap.put("P1_PhlF", "10");
        assignmentMap.put("P2_PhlF", "11");
        assignmentMap.put("P3_PhlF", "12");
        assignmentMap.put("Q1_QacR", "13");
        assignmentMap.put("Q2_QacR", "14");
        assignmentMap.put("R1_PsrA", "15");
        assignmentMap.put("S1_SrpR", "16");
        assignmentMap.put("S2_SrpR", "17");
        assignmentMap.put("S3_SrpR", "18");
        assignmentMap.put("S4_SrpR", "19");
    }

    private static class NetlistEntry {

        private String identifier;
        private String gateIdentifier;
        private int index;
        private int[] connectedIndices;

        public NetlistEntry(String netlistLine) {
            String[] components = netlistLine.split("\\s+");

            this.identifier = components[0];
            this.gateIdentifier = components[2];
            this.index = Integer.parseInt(components[3]);

            if (components.length >= 7) {
                String[] connectedIndicesStr = components[4].split(",");
                connectedIndices = new int[connectedIndicesStr.length];

                for (int i = 0; i < connectedIndicesStr.length; i++) {
                    connectedIndices[i] = Integer.parseInt(connectedIndicesStr[i]);
                }
            }

        }

        public String getIdentifier() {
            return identifier;
        }

        public String getGateIdentifier() {
            return gateIdentifier;
        }

        public int getIndex() {
            return index;
        }

        public int[] getConnectedIndices() {
            return connectedIndices;
        }

    }
}