package de.tu_darmstadt.rs.synbio.simulation;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tu_darmstadt.rs.synbio.assignment.Assignment;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

public class SimulatorInterface {

    private static final Logger logger = LoggerFactory.getLogger(SimulatorInterface.class);

    private final File simulatorPath;
    private final String simScript;
    private final String simInitArgs;
    private final String simArgs;
    private final File library;

    private Process simProcess;
    private BufferedReader reader;
    private BufferedWriter writer;
    private ObjectMapper mapper = new ObjectMapper();

    public SimulatorInterface(SimConfiguration config, File gateLibrary) {
        simulatorPath = config.getSimPath();
        simScript = config.getSimScript();
        simInitArgs = config.getSimInitArgs();
        simArgs = config.getSimArgs();
        library = gateLibrary;
    }

    public SimulatorInterface(File simPath, String simScript, String simInitArgs, String simArgs, File gateLibrary) {
        simulatorPath = simPath;
        this.simScript = simScript;
        this.simInitArgs = simInitArgs;
        this.simArgs = simArgs;
        this.library = gateLibrary;
    }

    public void initSimulation(Circuit circuit) {

        if (simProcess!= null && simProcess.isAlive())
            simProcess.destroy();

        try {
            String structureFileName = "structure_tid" + Thread.currentThread().getId() +".json";
            File structureFile = new File(simulatorPath, structureFileName);
            circuit.save(structureFile);

            ProcessBuilder pb = new ProcessBuilder("python3", simScript, "s_path=" + structureFileName + " lib_path=" + library.getAbsolutePath() + " " + simInitArgs);
            pb.directory(simulatorPath);
            simProcess = pb.start();

            reader = new BufferedReader(new InputStreamReader(simProcess.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(simProcess.getOutputStream()));
            try {
                while (!reader.readLine().startsWith("ready:")) ;
            }
            catch(Exception e){
                System.out.println();
            }
            structureFile.delete();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Double simulate(Assignment assignment) {

        Map<String, String> assignmentIdentifiers = assignment.getIdentifierMap();

        double score = 0.0;

        if (!simProcess.isAlive()) {
            //logger.error("sim process aborted");
            return -1.0;
        }
        try {
            String assignmentStr = mapper.writeValueAsString(assignmentIdentifiers);

            writer.write("start " + simArgs + " assignment=" + assignmentStr);
            writer.newLine();
            writer.flush();

            String scoreStr = reader.readLine();
            if (scoreStr.startsWith("O ")) {
                scoreStr = scoreStr.substring(2);
            }
            else{
                scoreStr = scoreStr.split(" ")[1];
            }

            score = Double.parseDouble(scoreStr);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return score;
    }

    public void shutdown() {
        simProcess.destroy();
    }
}
