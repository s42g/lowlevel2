package de.tu_darmstadt.rs.synbio.aig;

import de.tu_darmstadt.rs.synbio.logic.LogicUtils;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.io.ComponentNameProvider;
import org.jgrapht.io.DOTExporter;
import org.jgrapht.io.GraphExporter;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

public class AIG extends DirectedAcyclicGraph<AIGNode, AIGEdge> {

    private static final Logger logger = LoggerFactory.getLogger(AIG.class);

    private Integer nodeIdCounter = 0;

    public AIG() {
        super(AIGEdge.class);
    }

    public void expandInverterEdges() {

        Set<AIGEdge> edges = new HashSet<>(this.edgeSet());

        for (AIGEdge edge : edges) {

            if (edge.isInverted()) {

                AIGNode n1 = this.getEdgeSource(edge);
                AIGNode n2 = this.getEdgeTarget(edge);

                AIGNode inverter = new AIGNode(AIGNode.NodeType.NOT, this.getNewNodeId().toString());
                this.addVertex(inverter);

                this.removeEdge(edge);
                this.addEdge(n1, inverter, new AIGEdge(inverter.getFormula().variables().first(),false));
                this.addEdge(inverter, n2, new AIGEdge(edge.getVariable(), false));
            }
        }
    }

    public void replaceVertex(AIGNode node, AIGNode replacement) {
        this.addVertex(replacement);
        for (AIGEdge edge : this.outgoingEdgesOf(node)) this.addEdge(replacement, this.getEdgeTarget(edge), new AIGEdge(edge.getVariable(), edge.isInverted()));
        for (AIGEdge edge : this.incomingEdgesOf(node)) this.addEdge(this.getEdgeSource(edge), replacement, new AIGEdge(edge.getVariable(), edge.isInverted()));
        this.removeVertex(node);
    }

    public AIGNode getOutputNode() {
        return this.vertexSet().stream().filter(node -> node.getType() == AIGNode.NodeType.OUTPUT).findFirst().get();
    }

    public Set<Variable> getInputVariables() {
        return getExpression().variables();
    }

    public Formula getExpression() {
        return getExpression(getOutputNode(), null);
    }

    public Formula getExpression(AIGNode startNode, HashSet<AIGNode> terminalNodes) {

        if(startNode.getType() == AIGNode.NodeType.INPUT || (terminalNodes != null && terminalNodes.contains(startNode)))
            return LogicUtils.parseExpression(startNode.getIdentifier());

        Formula expression = startNode.getFormula();

        for (Variable var : expression.variables()) {

            AIGNode connectedNode = getEdgeSource(edgesOf(startNode).stream().filter(e -> e.getVariable().equals(var)).findFirst().get());
            expression = expression.substitute(var, getExpression(connectedNode, terminalNodes));
        }

        return expression;
    }

    private Integer getNewNodeId() {
        nodeIdCounter ++;
        return nodeIdCounter - 1;
    }

    public HashMap<AIGNode, HashSet<HashSet<AIGNode>>> computeCuts(Integer k) {

        HashMap<AIGNode, HashSet<HashSet<AIGNode>>> cuts = new HashMap<>();

        Iterator<AIGNode> it = new TopologicalOrderIterator<>(this);
        while (it.hasNext()) {

            AIGNode node = it.next();

            // add elementary cut
            cuts.put(node, new HashSet<>(Arrays.asList(new HashSet<>(Arrays.asList(node)))));

            // collect cut sets of children
            ArrayList<AIGNode> children = (ArrayList<AIGNode>) this.incomingEdgesOf(node).stream()
                    .map(this::getEdgeSource).collect(Collectors.toList());

            switch (children.size()) {
                // this case should not occur
                case 0:
                    continue;

                // only one child: propagate feasible cuts
                case 1:
                    cuts.get(children.get(0)).stream().filter(cut -> cut.size() <= k).forEach(cut -> cuts.get(node).add(cut));
                    break;

                // two children: merge cut sets and check feasibility
                case 2: {
                    for (HashSet<AIGNode> child1Cut : cuts.get(children.get(0))) {

                        for (HashSet<AIGNode> child2Cut : cuts.get(children.get(1))) {

                            HashSet<AIGNode> mergedSet = new HashSet<>();

                            mergedSet.addAll(child1Cut);
                            mergedSet.addAll(child2Cut);

                            //TODO: check: duplicates + redundancy?

                            if (mergedSet.size() > k) continue;

                            cuts.get(node).add(mergedSet);
                        }
                    }
                }

            }
        }
        return cuts;
    }

    public Integer getCutVolume(AIGNode startNode, HashSet<AIGNode> terminalNodes) {

        if ((startNode.getType() == AIGNode.NodeType.INPUT) || (terminalNodes != null && terminalNodes.contains(startNode)))
            return 0;

        int volume = 0;

        for (AIGNode node : this.incomingEdgesOf(startNode).stream().map(this::getEdgeSource).collect(Collectors.toCollection(HashSet::new)))
            volume += 1 + getCutVolume(node, terminalNodes);

        return volume;
    }

    public void print(File outputFile) {

        ComponentNameProvider<AIGNode> vertexIdProvider = new ComponentNameProvider<AIGNode>()
        {
            public String getName(AIGNode node)
            {
                return node.getIdentifier();
            }
        };
        ComponentNameProvider<AIGNode> vertexLabelProvider = new ComponentNameProvider<AIGNode>()
        {
            public String getName(AIGNode node)
            {
                return node.getType().toString() + "\n" + node.getIdentifier();
            }
        };

        ComponentNameProvider<AIGEdge> edgeLabelProvider = new ComponentNameProvider<AIGEdge>()
        {
            public String getName(AIGEdge edge)
            {
                return edge.isInverted() ? "! " : " " + edge.getVariable();
            }
        };

        GraphExporter<AIGNode, AIGEdge> exporter = new DOTExporter<>(vertexIdProvider, vertexLabelProvider, edgeLabelProvider);

        try {
            Writer writer = new FileWriter(outputFile);
            exporter.exportGraph(this, writer);
        } catch(Exception e) {
            logger.error(e.getMessage());
        }

    }

}
