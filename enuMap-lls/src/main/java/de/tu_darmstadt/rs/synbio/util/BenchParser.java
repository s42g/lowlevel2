package de.tu_darmstadt.rs.synbio.util;

import de.tu_darmstadt.rs.synbio.aig.AIGEdge;
import de.tu_darmstadt.rs.synbio.aig.AIG;
import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class BenchParser {

    private static final Logger logger = LoggerFactory.getLogger(BenchParser.class);

    private static List<String> benchFile;

    public static AIG parse(File inputFile) {

        AIG aig = new AIG();
        benchFile = new LinkedList<>();

        // read file

        try(Stream<String> stream = Files.lines(Paths.get(inputFile.toURI()))) {
            stream.forEach(line -> benchFile.add(line));
        } catch (IOException e) {
            logger.error("Failed to read bench file!");
        }

        // create intermediate hash map with nodes and incoming edges, if present

        HashMap<AIGNode, String[]> nodes = new HashMap<AIGNode, String[]>();

        // parse inputs

        benchFile.stream().filter(line -> line.startsWith("INPUT"))
                .forEach(line -> nodes.put(new AIGNode(AIGNode.NodeType.INPUT, line.substring(line.indexOf("(") + 1, line.indexOf(")"))), null));

        // determine output identifier

        String outputId = benchFile.stream().filter(line -> line.startsWith("OUTPUT")).findFirst().get();
        outputId = outputId.substring(outputId.indexOf("(") + 1, outputId.indexOf(")"));

        // add AND nodes to hash map

        benchFile.stream()
                .filter(line -> !line.startsWith("OUTPUT"))
                .filter(line -> !line.startsWith("INPUT"))
                .filter(line -> !line.startsWith("#"))
                .filter(line -> line.contains("AND"))
                .forEach(line -> nodes.put(new AIGNode(AIGNode.NodeType.AND, line.substring(0, line.indexOf(" ", 0))),
                        line.substring(line.indexOf("(") + 1, line.indexOf(")")).split(",\\s*")));

        // add nodes to AIG

        for(AIGNode node : nodes.keySet()) {
            aig.addVertex(node);
        }

        // create intermediate hash map with inverter nodes

        HashMap<String, String> invNodes = new HashMap<String, String>();

        benchFile.stream()
                .filter(line -> line.contains("NOT"))
                .forEach(line -> invNodes.put(line.substring(0, line.indexOf(" ", 0)), line.substring(line.indexOf("(") + 1, line.indexOf(")"))));

        HashMap<String, String> invNodesLeft = new HashMap<>(invNodes);

        // iterate over annotated nodes and add edges

        for (AIGNode node : nodes.keySet()) {

            // if node has incoming edges
            if (nodes.get(node) != null) {

                int edgeNum = 0;
                ArrayList<Variable> nodeVars = new ArrayList<>(node.getFormula().variables());

                // iterate over incoming edges
                for (String edgeId : nodes.get(node)) {

                    Variable edgeVar = nodeVars.get(edgeNum);

                    // try to find direct connection to another node
                    if (findNode(nodes.keySet(), edgeId) != null) {
                        AIGNode connectedNode = findNode(nodes.keySet(), edgeId);
                        aig.addEdge(connectedNode, node, new AIGEdge(edgeVar, false));
                    }

                    // try to find inverted connection to another node
                    else if (invNodes.keySet().contains(edgeId)) {
                        AIGNode connectedNode = findNode(nodes.keySet(), invNodes.get(edgeId));
                        aig.addEdge(connectedNode, node, new AIGEdge(edgeVar, true));
                        invNodesLeft.remove(edgeId);
                    }

                    else {
                        logger.error("Error parsing bench file: Could not find connection!");
                    }

                    edgeNum ++;
                }
            }
        }

        // if an inverted node is left over, it is the output node and needs to be added separately

        for(String invNode : invNodesLeft.keySet()) {

                AIGNode newNode = new AIGNode(AIGNode.NodeType.OUTPUT, invNode);
                aig.addVertex(newNode);
                aig.addEdge(findNode(nodes.keySet(), invNodes.get(invNode)), newNode, new AIGEdge(newNode.getFormula().variables().first(), true));
        }

        // if output node is an AND node, add additional output node

        if (findNode(aig.vertexSet(), outputId).getType() == AIGNode.NodeType.AND) {

            // rename old output node
            AIGNode oldOutputNode = findNode(aig.vertexSet(), outputId);
            AIGNode oldOutputNodeReplacement = new AIGNode(AIGNode.NodeType.AND, outputId + "_");
            aig.replaceVertex(oldOutputNode, oldOutputNodeReplacement);

            // add new output node
            AIGNode newOutputNode = new AIGNode(AIGNode.NodeType.OUTPUT, outputId);
            aig.addVertex(newOutputNode);

            // connect nodes with non-inverted edge
            aig.addEdge(oldOutputNodeReplacement, newOutputNode, new AIGEdge(newOutputNode.getFormula().variables().first(), false));
        }

        // verify that graph is an AIG

        for(AIGNode node : aig.vertexSet()) {

            if (aig.incomingEdgesOf(node).size() > 2) {
                logger.info(String.valueOf(aig.getDescendants(node).size()));
                logger.error("AIG verification: Node with more than two inputs detected!");
            }

        }

        return aig;
    }

    private static AIGNode findNode(Set<AIGNode> nodes, String id) {

        Optional<AIGNode> node = nodes.stream().filter(n -> n.getIdentifier().equals(id)).findFirst();
        return node.orElse(null);
    }

}
