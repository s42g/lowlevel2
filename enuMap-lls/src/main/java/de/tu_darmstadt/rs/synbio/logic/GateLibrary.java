package de.tu_darmstadt.rs.synbio.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tu_darmstadt.rs.synbio.circuit.*;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

public class GateLibrary {

    private static final Logger logger = LoggerFactory.getLogger(GateLibrary.class);

    private HashMap<TruthTable, Circuit> complemented = new HashMap<>();
    private HashMap<String, List<Gate>> primitiveGatesLib = new HashMap<>();

    private File sourceFile;

    public GateLibrary(File primitiveLibraryFile) {

        this.sourceFile = primitiveLibraryFile;
        loadPrimitiveLibrary(primitiveLibraryFile);
    }

    /* library files handling */

    private void loadPrimitiveLibrary(File primitiveLibraryFile) {

        logger.info("Loading primitive gate library file " + primitiveLibraryFile.getAbsolutePath());

        HashMap[] parsedPrimitives;

        ObjectMapper mapper = new ObjectMapper();

        try {
            parsedPrimitives = mapper.readValue(primitiveLibraryFile, HashMap[].class);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        for (HashMap primitive : parsedPrimitives) {

            String primitiveIdentifier = (String) Optional.ofNullable(primitive.get("primitiveIdentifier"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"primitiveIdentifier\" not found!"));

            String identifier = (String) Optional.ofNullable(primitive.get("identifier"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"identifier\" not found!"));

            String expression = (String) Optional.ofNullable(primitive.get("expression"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"expression\" not found!"));

            String group = (String) Optional.ofNullable(primitive.get("group")).orElse("");

            /*boolean addComplemented = (boolean) Optional.ofNullable(primitive.get("addComplemented"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"addComplemented\" not found!"));*/


            Gate newGate = new Gate(expression, primitiveIdentifier, identifier, group);

            LinkedHashMap biorep = (LinkedHashMap) Optional.ofNullable(primitive.get("biorep"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"biorep\" not found!"));

            LinkedHashMap responseFunction = (LinkedHashMap) Optional.ofNullable(biorep.get("response_function"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"response_function\" not found!"));

            LinkedHashMap parameters = (LinkedHashMap) Optional.ofNullable(responseFunction.get("parameters"))
                    .orElseThrow(() -> new RuntimeException("Invalid primitive gate library: Key \"parameters\" not found!"));

            Optional ymaxOpt = Optional.ofNullable(parameters.get("ymax"));
            Optional yminOpt = Optional.ofNullable(parameters.get("ymin"));
            Optional kOpt = Optional.ofNullable(parameters.get("K"));
            Optional nOpt = Optional.ofNullable(parameters.get("n"));

            if (ymaxOpt.isPresent() && yminOpt.isPresent() && kOpt.isPresent() && nOpt.isPresent()) {

                double ymax = (double) ymaxOpt.get();
                double ymin = (double) yminOpt.get();
                double k = (double) kOpt.get();
                double n = (double) nOpt.get();

                double vm = Math.pow(((ymin + (ymax - ymin)) / (((ymax - ymin) / 2 ) + ymin) - 1), n) * k;
                newGate.setVm(vm);
            }

            addToLibrary(newGate, false);
        }
    }

    /* adding and complementation of primitive gates */

    private void addToLibrary(Gate element, boolean addComplemented) {

        if (primitiveGatesLib.containsKey(element.getPrimitiveIdentifier())) {

            primitiveGatesLib.get(element.getPrimitiveIdentifier()).add(element);

        } else {

            primitiveGatesLib.put(element.getPrimitiveIdentifier(), new ArrayList<>());
            primitiveGatesLib.get(element.getPrimitiveIdentifier()).add(element);

            // add base circuit
            complemented.put(new TruthTable(element.getExpression()), new Circuit(element, element.getPrimitiveIdentifier()));


            // add complemented circuits
            if (addComplemented) {

                // build output complemented circuit
                Circuit circuit = new Circuit(element, element.getPrimitiveIdentifier() + "_oi");
                circuit.insertInverter(element, circuit.getOutputBuffer());
                complemented.put(circuit.getTruthTable(), circuit);

                // build input and output complemented circuits
                for (int i = 1; i < (1 << circuit.getExpression().variables().size()); i++) {

                    circuit = new Circuit(element, element.getPrimitiveIdentifier() + "_" + i);
                    int varPos = 0;

                    for (Variable variable : circuit.getExpression().variables()) {

                        boolean negatedVar = (i & (1 << varPos)) != 0;
                        if (negatedVar) circuit.insertInverter(circuit.getInputBuffer(variable), element);
                        varPos++;
                    }

                    complemented.put(circuit.getTruthTable(), circuit);

                    Circuit circuitOI = (Circuit) circuit.clone();
                    circuitOI.setIdentifier(circuitOI.getIdentifier() + "_oi");
                    circuitOI.insertInverter(element, circuit.getOutputBuffer());
                    complemented.put(circuitOI.getTruthTable(), circuitOI);
                }
            }
        }
    }

    /* getter and utility functions */

    public File getSourceFile() {
        return sourceFile;
    }

    public HashMap<TruthTable, Circuit> get() {
        return complemented;
    }

    public HashMap<String, List<Gate>> getPrimitives() {
        return primitiveGatesLib;
    }

    public void print() {

        logger.info("Primitive and complemented gates:");

        for (TruthTable truthTable : complemented.keySet()) {
            logger.info(truthTable.toString() + " --> " + complemented.get(truthTable).getIdentifier());
        }

        logger.info("Gate number constraints:");

        for (String gateIdentifier : primitiveGatesLib.keySet()) {
            logger.info(gateIdentifier + ": " + primitiveGatesLib.get(gateIdentifier).size());
        }
    }

    public Integer getNumAvailableGates(String primitiveIdentifier) {

        if (primitiveGatesLib.get(primitiveIdentifier) == null)
            return 0;

        return primitiveGatesLib.get(primitiveIdentifier).size();
    }

    public List<Gate> getGateClasses() {

        List<Gate> primGates = new ArrayList<>();

        for (String primId : primitiveGatesLib.keySet()) {

            primGates.add(primitiveGatesLib.get(primId).get(0));
        }

        return primGates;
    }

    public int getFeasibility() {

        OptionalInt feasibility = complemented.values().stream().mapToInt(c -> c.getExpression().variables().size()).max();

        if (feasibility.isEmpty()) {
            logger.error("Library feasibility could not be determined. Using a default value of 2.");
            return 2;
        } else {
            return feasibility.getAsInt();
        }
    }
}