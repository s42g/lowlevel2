
package de.tu_darmstadt.rs.synbio.assignment.search;

import de.tu_darmstadt.rs.synbio.SimulationTestbench;
import de.tu_darmstadt.rs.synbio.assignment.Assignment;
import de.tu_darmstadt.rs.synbio.assignment.ExhaustiveAssigner;
import de.tu_darmstadt.rs.synbio.assignment.ThreeGateCombination;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.circuit.Wire;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import de.tu_darmstadt.rs.synbio.simulation.SimulatorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class GreedySearch extends AssignmentSearchAlgorithm {
	public GreedySearch(Circuit structure, GateLibrary lib, SimConfiguration config) {
		super(structure, lib, config);
	}
	private static final Logger logger = LoggerFactory.getLogger(SimulationTestbench.class);


	@Override
	public SimulationResult simulate() {
		SimulatorInterface simulator = new SimulatorInterface(config, gateLib.getSourceFile());
		simulator.initSimulation(structure);
		HashSet<Assignment> blackList = new HashSet<>();
		Assignment blackListCandidate = null;
		Assignment lastRemoved = null;
		HashSet<ThreeGateCombination> allThreeCombinations = getPossibleThreeGateCombinations(structure, gateLib); //independent of the blackList or if its mappable or not. can be put outside the loop
		SimulatorInterface simulator3 = null;
		int i = 0;
		int count = 0;
		while(isMappable(structure, gateLib, blackList)&&count<10){ //maximum 50 iterations

			if(allThreeCombinations.isEmpty()){
				ExhaustiveAssigner exAssigner= new ExhaustiveAssigner(gateLib, structure);
				Assignment as = exAssigner.getNextAssignment();
				Double simRes = simulator.simulate(as);
				return new SimulationResult(structure,as,simRes);
			}
			String test = structure.getExpression().toString();
			Double minSimulationTime=Double.MAX_VALUE; //min time for all

			for(ThreeGateCombination combi : allThreeCombinations) {

				Circuit circ;
				circ = combi.toCircuit(structure);
				int numberOfLogicGates = circ.getNumberLogicGates(); //This must be always 3!!!
				ExhaustiveAssigner exAssigner= new ExhaustiveAssigner(gateLib, circ);
				//RandomAssigner exAssigner= new RandomAssigner(gateLib, circ);
				Assignment as = exAssigner.getNextAssignment();

				HashMap<Assignment, Double> resultList =new HashMap<>();

				while(!blackListCheck(as,blackList)){
					as = exAssigner.getNextAssignment();
				}

				//if(as==null){
				//	break;
				//}
				blackListCandidate = as;
				if(simulator3 !=null) {
					simulator3.shutdown();
				}
				simulator3 = new SimulatorInterface(config, gateLib.getSourceFile()); //we need another simulation. Simulator depends on the circuit
					try {
						simulator3.initSimulation(circ);
					}
					catch(Exception e)
					{
						System.out.println();
					}
				Double simRes;
					if(as.isValid()){
						simRes = simulator3.simulate(as);
					}
					else{
						simRes = -2.0;
					}
				if(simRes<0){
					simulator3.shutdown();
					continue;
					//return new SimulationResult(null,null,-1.0);
				}
				resultList.put(as, simRes);

				if(simRes<minSimulationTime)
				{
					blackListCandidate = as;
					minSimulationTime = simRes;
				}
				//System.out.println();
				i++; //next combi-circuit
			}
			if(blackListCandidate!=null) {
				blackList.add(blackListCandidate);
				lastRemoved=blackListCandidate;
				blackListCandidate=null;

			}
			i=0; //reset the counter
			count++;
		}
		if (blackList.isEmpty()){
			return null;
		}
		blackList.remove(lastRemoved);

		return findAssignment(gateLib, structure, blackList, simulator);

	}

	private SimulationResult findAssignment(GateLibrary gateLib, Circuit structure, HashSet<Assignment> blackList, SimulatorInterface simulator){
		ExhaustiveAssigner e = new ExhaustiveAssigner(gateLib,structure);
		Assignment as = e.getNextAssignment();
		while(as != null)
		{
			if(blackListCheck(as,blackList))
			{
				return new SimulationResult(structure,as,simulator.simulate(as));
			}
			as = e.getNextAssignment();
		}
		return null;
	}

	private HashSet<ThreeGateCombination> getPossibleThreeGateCombinations(Circuit structure, GateLibrary lib) {
		HashSet<ThreeGateCombination> combiSet = new HashSet<>();
		Gate output = structure.getOutputBuffer();
		combiSet.addAll(getThreeGateCombinations(output,structure));
		return combiSet;
	}

	private HashSet<ThreeGateCombination> getThreeGateCombinations(Gate gate, Circuit structure) {
		HashSet<ThreeGateCombination> combiSet = new HashSet<>();
		Set<Gate> ancestors = new HashSet<>();
		for(Wire incoming : structure.incomingEdgesOf(gate)){
			if(structure.getEdgeSource(incoming).getType() == Gate.Type.LOGIC) {
				ancestors.add(structure.getEdgeSource(incoming));
			}
		}
		//Set<Gate> ancestors = structure.edge(gate);
		if(!ancestors.isEmpty())
		{
			Object[] ancArrayO;
			if(ancestors.size()==2){
				ancArrayO = ancestors.toArray();
				Gate[] ancArray = new Gate[ancArrayO.length];
				for(int i = 0; i<ancArrayO.length;i++){
					ancArray[i] = (Gate) ancArrayO[i];
				}
				combiSet.add(new ThreeGateCombination(gate,ancArray[0],ancArray[1]));
			}
			else{
				//System.out.println("Gate or Output with more than 2 or less than 2 inputs.");
			}
			for (Gate g : ancestors) {
				combiSet.addAll(getThreeGateCombinations(g, structure));
			}
		}

		return combiSet;
	}

	/**
	 * Checks, if a Circuit is mappable with a given GateLibrary and the current BlackList
	 *
	 * @param structure Circuit to check
	 * @param lib GateLibrary
	 * @param blackList current BlackList
	 * @return True, if Circuit is mappable without any BlackList elements
	 */
	public boolean isMappable(Circuit structure, GateLibrary lib, HashSet<Assignment> blackList){
		//ExhaustiveAssigner e = new ExhaustiveAssigner(lib,structure);
		ExhaustiveAssigner e = new ExhaustiveAssigner(lib,structure);
		Assignment a = e.getNextAssignment();
		Boolean flag = true;
		int numAssignments = 0;
		while(a != null && numAssignments<1000 )
		{
			for(Assignment blackListElement : blackList) {
				if(!a.map.keySet().containsAll(blackListElement.map.keySet())) {
					continue; // assignment doesnt contain this element of the blacklist
				}
				Iterator<Map.Entry<Gate,Gate>> it = blackListElement.map.entrySet().iterator();
				Boolean flag2 = false; // true if blackListElement not in assignment
				while(it.hasNext()){
					Map.Entry<Gate, Gate> gate = it.next();
					if(!(a.get(gate.getKey()).getPrimitiveIdentifier()==gate.getValue().getPrimitiveIdentifier())){
						flag2=true;
						break; // this blacklist element is not in assignment
					}

				}
				if(!flag2){ // if blackListElement in assignment
					flag = false; // assignment not valid
					break;
				}
			}
			if(flag){ // if assignment valid
				return true;
			}
			flag = true;
			numAssignments++;
			a=e.getNextAssignment();

		}
		return false;
	}

	/**
	 * Checks if a Assignment contains NOT a BlackList Element.
	 *
	 * @param a Assignment to check
	 * @param blackList BlackList
	 * @return True, if Assignment does NOT contain any BlackListElements
	 */
	private boolean blackListCheck(Assignment a, HashSet<Assignment> blackList) {
		for(Assignment blackListElement : blackList){
			Iterator<Map.Entry<Gate,Gate>> it = blackListElement.map.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry<Gate, Gate> gate = it.next();
				if(!a.map.containsKey(gate.getKey())||!(a.get(gate.getKey())==gate.getValue()))
					break;

			}
			if(!it.hasNext()){
				return false;
			}
		}
		return true;
	}
}
