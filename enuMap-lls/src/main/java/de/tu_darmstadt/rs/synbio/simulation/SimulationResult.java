package de.tu_darmstadt.rs.synbio.simulation;

import de.tu_darmstadt.rs.synbio.assignment.Assignment;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;

public class SimulationResult implements Comparable<SimulationResult> {

    private Circuit structure;
    private Assignment assignment;
    private double score;

    public SimulationResult(Circuit structure, Assignment assignment, double score) {
        this.structure = structure;
        this.assignment = assignment;
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public Circuit getStructure() {
        return structure;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    @Override
    public int compareTo(SimulationResult cmp) {
        if (this.score < cmp.score)
            return -1;
        else if (cmp.score < this.score)
            return 1;
        return 0;
    }
}
