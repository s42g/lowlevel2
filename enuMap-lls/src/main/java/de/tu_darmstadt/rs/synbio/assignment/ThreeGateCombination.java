package de.tu_darmstadt.rs.synbio.assignment;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.circuit.Wire;
import org.logicng.formulas.Variable;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedSet;

public class ThreeGateCombination {
	Gate gate1;
	Gate gate2;
	Gate gate3;
	public ThreeGateCombination(Gate gate1, Gate gate2, Gate gate3) {
		this.gate1 = gate1;
		this.gate2 = gate2;
		this.gate3 = gate3;
	}

	public Gate getGate1() {
		return gate1;
	}

	public void setGate1(Gate gate) {
		this.gate1 = gate;
	}

	public Gate getGate2() {
		return gate2;
	}

	public void setGate2(Gate gate) {
		this.gate2 = gate;
	}

	public Gate getGate3() {
		return gate3;
	}

	public void setGate3(Gate gate) {
		this.gate3 = gate;
	}

	public LinkedList<Gate> getAllGates(){
		LinkedList<Gate> list = new LinkedList<Gate>();
		list.add(gate1);
		list.add(gate2);
		list.add(gate3);
		return list;
	}

	public Circuit toCircuit(Circuit origin){
		//Gate gate1 = new Gate("a", this.gate1.getPrimitiveIdentifier(), this.gate1.getIdentifier()+"t", this.gate1.getGroup());
		//Gate gate2 = new Gate("b", this.gate2.getPrimitiveIdentifier(), this.gate2.getIdentifier()+"t", this.gate2.getGroup());
		//Gate gate3 = new Gate("c", this.gate3.getPrimitiveIdentifier(), this.gate3.getIdentifier()+"t", this.gate3.getGroup());
		Circuit circ = new Circuit("id_" + Math.round(Math.random()*1000000));
		Gate outputBuffer = new Gate("d", Math.round(Math.random()*1000000)+"" , Gate.Type.OUTPUT);
		circ.addVertex(outputBuffer);
		circ.addVertex(gate1);
		circ.addVertex(gate2);
		circ.addVertex(gate3);
		SortedSet<Variable> test = gate1.getExpression().variables();
		circ.addEdge(gate1, outputBuffer, new Wire(outputBuffer.getExpression().variables().first()));
		circ.addEdge(gate2, gate1, new Wire(gate1.getExpression().variables().first()));
		circ.addEdge(gate3, gate1, new Wire(gate1.getExpression().variables().last()));

		Gate inputBuffer;


		Iterator<Variable> it = gate2.getExpression().variables().iterator();
		char a = 's';
		for(Wire w: origin.incomingEdgesOf(gate2)) {
			String id =Character.toString(a);
			inputBuffer = new Gate(id, Math.round(Math.random()*100000)+"" , Gate.Type.INPUT); // input oder logic
			circ.addVertex(inputBuffer);
			circ.addEdge(inputBuffer, gate2, new Wire(it.next()));
			a++;
		}
		it = gate3.getExpression().variables().iterator();
		for(Wire w: origin.incomingEdgesOf(gate3)) {
			String id =Character.toString(a);
			inputBuffer = new Gate(id, Math.round(Math.random()*100000)+"" , Gate.Type.INPUT); // input oder logic
			circ.addVertex(inputBuffer);
			circ.addEdge(inputBuffer, gate3, new Wire(it.next()));
			a++;
		}
		String circuit = circ.getExpression().toString();
		return circ;
	}
}
