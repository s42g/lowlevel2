package de.tu_darmstadt.rs.synbio.aig;

import de.tu_darmstadt.rs.synbio.logic.LogicUtils;
import org.logicng.formulas.Formula;

public class AIGNode {

    public enum NodeType {
        INPUT,
        OUTPUT,
        AND,
        NOT
    }

    private final NodeType type;
    private final String identifier;
    private final Formula expression;

    public AIGNode(NodeType type, String identifier) {

        this.type = type;
        this.identifier = identifier;

        switch(this.type) {
            case AND: this.expression = LogicUtils.parseExpression("x&y"); break;
            case NOT: this.expression = LogicUtils.parseExpression("~x"); break;
            case INPUT: this.expression = LogicUtils.parseExpression(identifier); break;
            default:  this.expression = LogicUtils.parseExpression("x");
        }

    }

    public NodeType getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Formula getFormula() {
       return expression;
    }
}
