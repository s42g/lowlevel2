package de.tu_darmstadt.rs.synbio.matching;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import org.logicng.formulas.Variable;

import java.util.HashMap;
import java.util.HashSet;

public class Match {

    private HashSet<AIGNode> cut;
    private Integer cutVolume;
    private Circuit circuit;
    private HashMap<Gate, Variable> inputMapping;

    public Match(HashSet<AIGNode> cut, Circuit circuit, Integer cutVolume, HashMap<Gate, Variable> inputMapping) {

        this.cut = cut;
        this.circuit = circuit;
        this.cutVolume = cutVolume;
        this.inputMapping = inputMapping;
    }

    public HashSet<AIGNode> getCut() {
        return cut;
    }

    public Circuit getCircuit() {
        return circuit;
    }

    public Integer getCutVolume() {
        return cutVolume;
    }

    public HashMap<Gate, Variable> getInputMapping() {
        return inputMapping;
    }

}
