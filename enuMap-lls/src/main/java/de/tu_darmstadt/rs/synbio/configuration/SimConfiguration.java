package de.tu_darmstadt.rs.synbio.configuration;

import de.tu_darmstadt.rs.synbio.assignment.search.AssignmentSearchAlgorithm;
import de.tu_darmstadt.rs.synbio.assignment.search.ExhaustiveSearch;
import de.tu_darmstadt.rs.synbio.assignment.search.GreedySearch;
import de.tu_darmstadt.rs.synbio.assignment.search.TabuSearch;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;

public class SimConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(SimConfiguration.class);

    /* simulation configuration */

    private boolean simEnabled;
    private boolean simLimitThreads;
    private int simLimitThreadsNum;
    private File simPath;
    private String simScript;
    private String simInitArgs;
    private String simArgs;
    private SearchAlgorithm searchAlgorithm;
    private OptimizationType optimizationType;

    public enum SearchAlgorithm {
        EXHAUSTIVE, TABU, GREEDY
    }

    public enum OptimizationType {
        MAXIMIZE {
            @Override
            public boolean compare(double current, double candidate) {
                return candidate > current;
            }
        },

        MINIMIZE {
            @Override
            public boolean compare(double current, double candidate) {
                return candidate < current;
            }
        };

        public abstract boolean compare(double current, double candidate);
    }

    public SimConfiguration(String configFile) throws Exception {

        /* config file handling */

        Properties props = new Properties();
        InputStream is = null;
        is = new FileInputStream(configFile);
        props.load(is);

        /* simulation config handling */

        simEnabled = Boolean.parseBoolean(props.getProperty("SIM_ENABLED"));

        if (simEnabled) {

            simLimitThreads = Boolean.parseBoolean(props.getProperty("SIM_LIMIT_THREADS"));

            if (simLimitThreads)
                simLimitThreadsNum = Integer.parseInt(props.getProperty("SIM_LIMIT_THREADS_NUM"));

            simPath = new File(props.getProperty("SIM_PATH"));

            simScript = props.getProperty("SIM_SCRIPT");

            if (!new File(simPath, simScript).exists())
                throw new IOException("Simulator script " + simScript + " does not exist.");

            simInitArgs = props.getProperty("SIM_INIT_ARGS");

            simArgs = props.getProperty("SIM_ARGS");

            switch (props.getProperty("SEARCH_ALGORITHM")) {
                case "EXHAUSTIVE":
                    searchAlgorithm = SearchAlgorithm.EXHAUSTIVE;
                    break;
                case "TABU":
                    searchAlgorithm = SearchAlgorithm.TABU;
                    break;
                case "GREEDY":
                    searchAlgorithm = SearchAlgorithm.GREEDY;
                    break;
                default:
                    throw new IOException("Unknown search algorithm! (Available algorithms: " + Arrays.toString(SearchAlgorithm.values()) + ")");
            }

            switch (props.getProperty("OPTIMIZATION_TYPE")) {
                case "MAXIMIZE":
                    optimizationType = OptimizationType.MAXIMIZE;
                    break;
                case "MINIMIZE":
                    optimizationType = OptimizationType.MINIMIZE;
                    break;
                default:
                    throw new IOException("Unknown optimization type! (Available types: " + Arrays.toString(OptimizationType.values()) + ")");
            }
        }
    }

    public void print() {

        if (simEnabled) {
            logger.info("Simulation:");
            logger.info("\tsimulator path: " + simPath);
            logger.info("\tsimulator script: " + simScript);
            logger.info("\tsimulator initialization arguments: " + simInitArgs);
            logger.info("\tsimulator arguments: " + simArgs);
            logger.info("\tsearch algorithm: " + searchAlgorithm.name());
            logger.info("\toptimization type: " + optimizationType.name());
            logger.info("\tlimit sim. threads: " + simLimitThreads);
            if (simLimitThreads)
                logger.info("\tthread limit: " + simLimitThreadsNum);
        }
    }

    /* getter */

    public boolean isSimEnabled() {
        return simEnabled;
    }

    public boolean simLimitThreads() {
        return simLimitThreads;
    }

    public int getSimLimitThreadsNum() {
        return simLimitThreadsNum;
    }

    public File getSimPath() {
        return simPath;
    }

    public String getSimScript() {
        return simScript;
    }

    public String getSimInitArgs() { return simInitArgs; }

    public String getSimArgs() {
        return simArgs;
    }

    public OptimizationType getOptimizationType() {
        return optimizationType;
    }

    /* factories */

    public AssignmentSearchAlgorithm getSearchAlgorithm(Circuit structure, GateLibrary lib, SimConfiguration config) {

        switch (searchAlgorithm) {
            case TABU: return new TabuSearch(structure, lib, config);
            case GREEDY: return new GreedySearch(structure,lib, config);
            default: return new ExhaustiveSearch(structure, lib, config);
        }

    }
}
