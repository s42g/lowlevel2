package de.tu_darmstadt.rs.synbio.circuit;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tu_darmstadt.rs.synbio.logic.TruthTable;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.isomorphism.VF2GraphIsomorphismInspector;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.io.ComponentNameProvider;
import org.jgrapht.io.DOTExporter;
import org.jgrapht.io.GraphExporter;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

public class Circuit extends DirectedAcyclicGraph<Gate, Wire> implements LogicElement, Comparable<Circuit> {

    private static final Logger logger = LoggerFactory.getLogger(Circuit.class);

    private String identifier;

    public Circuit() {
        super(Wire.class);
    }

    public Circuit(String identifier) {
        super(Wire.class);
        this.identifier = identifier;
    }

    public Circuit(Gate gate, String identifier) {
        super(Wire.class);
        this.identifier = identifier;

        Gate outputBuffer = new Gate("x", "F", Gate.Type.OUTPUT);
        this.addVertex(gate);
        this.addVertex(outputBuffer);
        this.addEdge(gate, outputBuffer, new Wire(outputBuffer.getExpression().variables().first()));

        Gate inputBuffer;

        for(Variable inputVar : gate.getExpression().variables()) {
            inputBuffer = new Gate(inputVar.name(), inputVar.name(), Gate.Type.INPUT);
            this.addVertex(inputBuffer);
            this.addEdge(inputBuffer, gate, new Wire(inputVar));
        }
    }

    public Gate getOutputBuffer() {
        return vertexSet().stream().filter(gate -> gate.getType() == Gate.Type.OUTPUT).findFirst().get();
    }

    public List<Gate> getInputBuffers() {
        return vertexSet().stream().filter(gate -> (gate.getType() == Gate.Type.INPUT)).collect(Collectors.toList());
    }

    public Gate getInputBuffer(Variable variable) {
        return vertexSet().stream().filter(gate -> (gate.getType() == Gate.Type.INPUT && gate.getExpression().containsVariable(variable))).findFirst().get();
    }

    @Override
    public Formula getExpression() {
        return getExpression(getOutputBuffer());
    }

    private Formula getExpression(Gate startNode) {

        if(startNode.getType() == Gate.Type.INPUT)
            return startNode.getExpression();

        Formula expression = startNode.getExpression();

        for (Variable var : expression.variables()) {

            Gate connectedGate = getEdgeSource(edgesOf(startNode).stream().filter(w -> w.getVariable().equals(var)).findFirst().get());
            expression = expression.substitute(var, getExpression(connectedGate));
        }

        return expression;
    }

    @Override
    public TruthTable getTruthTable() {
        return new TruthTable(getExpression());
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    public Integer getWeight() {
        return vertexSet().stream().mapToInt(Gate::getWeight).sum();
    }

    public int getNumberLogicGates() { return (int) vertexSet().stream().filter(g -> g.getType().equals(Gate.Type.LOGIC)).count(); }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void insertInverter(Gate source, Gate destination) {

        Variable edgeVar = getEdge(source, destination).getVariable();
        removeEdge(source, destination);

        Gate inverter = new Gate("~" + edgeVar.name(), "NOT", "NOT", "");
        addVertex(inverter);

        addEdge(source, inverter, new Wire(edgeVar));
        addEdge(inverter, destination, new Wire(edgeVar));
    }

    public void replaceGate(Gate node, Gate replacement) {
        this.addVertex(replacement);
        for (Wire wire : this.outgoingEdgesOf(node)) this.addEdge(replacement, this.getEdgeTarget(wire), new Wire(wire.getVariable()));
        for (Wire wire : this.incomingEdgesOf(node)) this.addEdge(this.getEdgeSource(wire), replacement, new Wire(wire.getVariable()));
        this.removeVertex(node);
    }

    private boolean isValid() {

        for (Gate logicGate : vertexSet().stream().filter(gate -> gate.getType().equals(Gate.Type.LOGIC)).collect(Collectors.toList())) {

            // test if number of input wires equals support size
            if (logicGate.getExpression().variables().size() > incomingEdgesOf(logicGate).size())
                return false;

            // test if gate output is connected
            if (outgoingEdgesOf(logicGate).isEmpty())
                return false;
        }

        return true;
    }

    public boolean isEquivalent(Circuit cmp) {

        if (vertexSet().size() != cmp.vertexSet().size())
            return false;

        if (edgeSet().size() != cmp.edgeSet().size())
            return false;

        if (!usesSamePrimitives(cmp))
            return false;

        return new VF2GraphIsomorphismInspector<>(this, cmp, new GateComparator(), new WireComparator()).isomorphismExists();
    }

    public boolean removeRedundantGates() {

        //int redCounter = 0;

        //print(new File("before.dot"));
        TruthTable beforeTT = new TruthTable(getExpression());

        // build redundancy map
        HashMap<String, List<Gate>> redMap = new HashMap<>();

        Iterator<Gate> iterator = new TopologicalOrderIterator<>(this);

        while(iterator.hasNext()) {

            Gate gate = iterator.next();

            if (gate.getType().equals(Gate.Type.LOGIC)) {

                Formula expr = getExpression(gate);
                TruthTable gateTT = new TruthTable(getExpression(gate));
                String gateClassIdentifier = gateTT.toString() + expr.variables().toString();

                redMap.putIfAbsent(gateClassIdentifier, new ArrayList<>());
                redMap.get(gateClassIdentifier).add(gate);
            }
        }

        // remove redundancies
        for (List<Gate> gateClass : redMap.values()) {

            if (gateClass.size() > 1) {

                Gate keeper = gateClass.get(0);

                for (int i = 1; i < gateClass.size(); i ++) {

                    // collect information on redundant gate
                    Gate redGate = gateClass.get(i);
                    Set<Wire> inWires = new HashSet<>(incomingEdgesOf(redGate));
                    Optional<Wire> outWire = outgoingEdgesOf(redGate).stream().findFirst();

                    if (outWire.isEmpty())
                        continue;

                    Gate targetGate = getEdgeTarget(outWire.get());
                    Variable outVar = outWire.get().getVariable();

                    // remove redundant gate and edges
                    removeEdge(outWire.get());
                    inWires.forEach(this::removeEdge);
                    removeVertex(redGate);

                    // add new edge
                    if (!containsEdge(keeper, targetGate))
                        addEdge(keeper, targetGate, new Wire(outVar));

                    //redCounter ++;
                }
            }
        }

        //print(new File("after.dot"));

        if (!isValid())
            return false;

        //logger.info("Removed " + redCounter + " redundant gates.");

        TruthTable afterTT = new TruthTable(getExpression());

        if (!beforeTT.equalsLogically(afterTT))
            logger.error("Circuit with redundancy removed does not equal input circuit!");

        return true;
    }

    public boolean usesSamePrimitives(Circuit cmp) {

        HashMap<String, Integer> primitiveCount = new HashMap<>();
        HashMap<String, Integer> cmpPrimitiveCount = new HashMap<>();

        vertexSet().stream().filter(g -> g.getType() == Gate.Type.LOGIC).forEach(g -> {
            primitiveCount.putIfAbsent(g.getPrimitiveIdentifier(), 0);
            primitiveCount.put(g.getPrimitiveIdentifier(), primitiveCount.get(g.getPrimitiveIdentifier()) + 1);
        });

        cmp.vertexSet().stream().filter(g -> g.getType() == Gate.Type.LOGIC).forEach(g -> {
            cmpPrimitiveCount.putIfAbsent(g.getPrimitiveIdentifier(), 0);
            cmpPrimitiveCount.put(g.getPrimitiveIdentifier(), cmpPrimitiveCount.get(g.getPrimitiveIdentifier()) + 1);
        });

        return primitiveCount.equals(cmpPrimitiveCount);
    }

    public HashMap<Gate, List<Variable>> getUnconnectedGateInputs() {

        HashMap<Gate, List<Variable>> gateInputs = new HashMap<>();

        for (Gate gate : vertexSet().stream().filter(g -> g.getType().equals(Gate.Type.LOGIC)).collect(Collectors.toList())) {

            ArrayList<Variable> inputs = new ArrayList<>(gate.getExpression().variables());
            inputs.removeAll(incomingEdgesOf(gate).stream().map(Wire::getVariable).collect(Collectors.toList()));

            if (inputs.size() > 0)
                gateInputs.put(gate, inputs);
        }

        return gateInputs;
    }

    public int getDepth() {

        AllDirectedPaths<Gate, Wire> allPaths = new AllDirectedPaths<>(this);

        Gate output = getOutputBuffer();
        List<Gate> inputs = getInputBuffers();

        int depth = 0;

        for (Gate input : inputs) {

            List<GraphPath<Gate, Wire>> paths = allPaths.getAllPaths(input, output, true, null);
            OptionalInt pathLength = paths.stream().mapToInt(GraphPath::getLength).max();

            if (pathLength.isPresent()) {
                int length = pathLength.getAsInt();

                if (length > depth)
                    depth = length;
            }
        }

        return depth - 1;
    }

    private class GateComparator implements Comparator<Gate> {
        @Override
        public int compare(Gate first, Gate second) {
            return first.getPrimitiveIdentifier().compareTo(second.getPrimitiveIdentifier());
        }
    }

    private class WireComparator implements Comparator<Wire> {
        @Override
        public int compare(Wire first, Wire second) {
            return 0;
        }
    }

    @Override
    public int compareTo(Circuit cmp) {
        return cmp.getWeight() - getWeight();
    }

    public void print(File outputFile) {

        ComponentNameProvider<Gate> vertexIdProvider = new ComponentNameProvider<Gate>()
        {
            public String getName(Gate gate)
            {
                return gate.getIdentifier();
            }
        };
        ComponentNameProvider<Gate> vertexLabelProvider = new ComponentNameProvider<Gate>()
        {
            public String getName(Gate gate)
            {

                switch (gate.getType()) {
                    case LOGIC: return  gate.getIdentifier();
                    default: return gate.getType() + " " + gate.getIdentifier();
                }
            }
        };

        ComponentNameProvider<Wire> edgeLabelProvider = new ComponentNameProvider<Wire>()
        {
            public String getName(Wire wire)
            {
                return "";//wire.getVariable().name();
            }
        };

        GraphExporter<Gate, Wire> exporter = new DOTExporter<>(vertexIdProvider, vertexLabelProvider, edgeLabelProvider);

        try {
            Writer writer = new FileWriter(outputFile);
            exporter.exportGraph(this, writer);
        } catch(Exception e) {
            logger.error(e.getMessage());
        }

    }

    public void save(File file) {

        ObjectMapper mapper = new ObjectMapper();
        CircuitSerializer circuitSerializer = new CircuitSerializer(Circuit.class);
        SimpleModule module = new SimpleModule("CircuitSerializer", new Version(1, 0, 0, null, null, null));
        module.addSerializer(Circuit.class, circuitSerializer);
        mapper.registerModule(module);

        try {
            Map<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("graph", this);
            jsonMap.put("truthtable", this.getTruthTable().toString());
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, jsonMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
