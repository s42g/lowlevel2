package de.tu_darmstadt.rs.synbio.matching;

import de.tu_darmstadt.rs.synbio.aig.AIG;
import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.logic.SupergateLibrary;
import de.tu_darmstadt.rs.synbio.logic.TruthTable;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Variable;

import java.util.*;
import java.util.stream.Collectors;

public class MatchingAlgorithm {

    public static HashMap<AIGNode, HashSet<Match>> booleanMatching(HashMap<AIGNode, HashSet<HashSet<AIGNode>>> cuts, AIG aig, HashMap<TruthTable, Circuit> lib) {

        HashMap<AIGNode, HashSet<Match>> matches = new HashMap<>();

        Comparator<Gate> inputComparator = Comparator.comparing(Gate::getIdentifier);

        for (AIGNode node : cuts.keySet()) {

            matches.put(node, new HashSet<>());

            for (HashSet<AIGNode> nodeCut : cuts.get(node)) {

                // filter out elementary cuts
                if (!nodeCut.contains(node)) {

                    // collect cut information
                    Formula cutExpression = aig.getExpression(node, nodeCut);
                    SortedSet<Variable> cutVariables = cutExpression.variables();
                    ArrayList<Variable> cutVariableList = new ArrayList<>(cutVariables);
                    TruthTable cutTruthTable = new TruthTable(cutExpression);

                    // find matching entry in library
                    for(TruthTable libTruthTable : lib.keySet()) {

                        if (libTruthTable.equalsLogically(cutTruthTable)) {

                            // compute mapping from input gates of library circuit to cut variables
                            HashMap<Gate, Variable> matchInputMapping = new HashMap<>();
                            Circuit matchedCircuit = lib.get(libTruthTable);

                            ArrayList<Gate> inputGates = matchedCircuit.vertexSet().stream().filter(gate -> gate.getType() == Gate.Type.INPUT)
                                    .sorted(inputComparator)
                                    .collect(Collectors.toCollection(ArrayList::new));

                            for(int i = 0; i < inputGates.size(); i++) {
                                matchInputMapping.put(inputGates.get(i), cutVariableList.get(i));
                            }

                            // add match to matches
                            matches.get(node).add(new Match(nodeCut,matchedCircuit, aig.getCutVolume(node, nodeCut), matchInputMapping));

                            /*logger.info("MATCH: " + node.getIdentifier() + ": " + aig.getExpression(node, nodeCut) + " "
                                    + gateLib.getLibrary().get(libTruthTable).getIdentifier() + " (volume: " + aig.getCutVolume(node, nodeCut) + ") (weight: "
                                    + gateLib.getLibrary().get(libTruthTable).getWeight() + ")");*/
                        }
                    }
                }
            }
        }

        return matches;
    }
}
