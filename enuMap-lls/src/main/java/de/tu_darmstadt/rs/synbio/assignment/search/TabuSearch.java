package de.tu_darmstadt.rs.synbio.assignment.search;

import de.tu_darmstadt.rs.synbio.assignment.Assignment;
import de.tu_darmstadt.rs.synbio.assignment.ExhaustiveAssigner;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import de.tu_darmstadt.rs.synbio.simulation.SimulatorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class TabuSearch  extends AssignmentSearchAlgorithm {

    private static final Logger logger = LoggerFactory.getLogger(TabuSearch.class);

    public TabuSearch(Circuit structure, GateLibrary lib, SimConfiguration config) {
        super(structure, lib, config);
    }

    public SimulationResult simulate() {

        logger.info("searching " + structure.getIdentifier());

        // initialize simulator

        SimulatorInterface simulator = new SimulatorInterface(config, gateLib.getSourceFile());
        simulator.initSimulation(structure);

        // get initial assignment

        ExhaustiveAssigner exhaustiveAssigner = new ExhaustiveAssigner(gateLib, structure);
        //RandomAssigner randomAssigner = new RandomAssigner(gateLibrary, structure);

        Assignment best = exhaustiveAssigner.getNextAssignment();//randomAssigner.getNextAssignment();
        long problemSize = exhaustiveAssigner.getNumTotalPermutations();
        long abortThreshold = Math.min(5000, problemSize / 2); //TODO tweak

        logger.info("possible assignments: " + problemSize + " (threshold " + abortThreshold +")");
        double bestScore = simulator.simulate(best);
        logger.info("start score: " + bestScore);

        // initialize search

        LinkedList<Assignment> tabuList = new LinkedList<>();
        tabuList.add(best);

        Assignment bestNeighbor = best;

        int noImprovementCount = 0;
        int simCount = 0;

        while (noImprovementCount < abortThreshold) {

            List<Assignment> neighborhood = getNeighborhood(bestNeighbor);
            //logger.info("nh size: " + neighborhood.size());
            bestNeighbor = null;//neighborhood.get(0);
            double bestNeighborScore = 0;//simulator.simulate(convertAssignmentToString(bestNeighbor));

            for (Assignment neighbor : neighborhood) {

                if (!tabuList.contains(neighbor)) {

                    double neighborScore = simulator.simulate(neighbor);
                    //logger.info("score: " + neighborScore);

                    simCount ++;

                    if (bestNeighbor == null || config.getOptimizationType().compare(bestNeighborScore, neighborScore)) {
                        bestNeighbor = neighbor;
                        bestNeighborScore = neighborScore;
                    }
                }
            }

            if (bestNeighbor == null) {
                logger.info("Empty neighborhood.");
                break;
                /*bestNeighbor = randomAssigner.getNextAssignment();
                continue;*/
            }

            if (config.getOptimizationType().compare(bestScore, bestNeighborScore)) {

                if (bestNeighborScore / bestScore > 1.001) {
                    noImprovementCount = 0;
                }

                best = bestNeighbor;
                bestScore = bestNeighborScore;

                logger.info("--- sim " + simCount + ": " + bestScore);
            } else {
                noImprovementCount ++;
            }

            tabuList.addFirst(bestNeighbor);

            if (tabuList.size() > abortThreshold / 2) { //TODO: reactive
               tabuList.removeLast();
            }

        }

        logger.info("number of simulations: " + simCount);
        return new SimulationResult(structure, best, bestScore);
    }

    private List<Assignment> getNeighborhood(Assignment assignment) {

        HashMap<String, List<Gate>> primitiveGatesLib = gateLib.getPrimitives();

        //sort lists by char. scalar

        for (List<Gate> list : primitiveGatesLib.values()) {
            list.sort(Comparator.comparingDouble(Gate::getVm));
        }

        List<Assignment> neighbors = new ArrayList<>();

        for (Gate circuitGate : assignment.keySet()) {

            List<Gate> primList = primitiveGatesLib.get(circuitGate.getPrimitiveIdentifier());

            /*int instanceIndex = primList.indexOf(assignment.get(circuitGate));

            if (instanceIndex > 0) {
                Assignment lowerNeighbor = new Assignment(assignment);
                lowerNeighbor.put(circuitGate, primList.get(posModulo((instanceIndex - 1), primList.size())));

                if (lowerNeighbor.isValid()) {
                    neighbors.add(lowerNeighbor);
                }
            }

            if (instanceIndex < (primList.size() - 2)) {
                Assignment upperNeighbor = new Assignment(assignment);
                upperNeighbor.put(circuitGate, primList.get((instanceIndex + 1) % primList.size()));

                if (upperNeighbor.isValid()) {
                    neighbors.add(upperNeighbor);
                }
            }*/

            Assignment lowerNeighbor = new Assignment(assignment);
            int instanceIndex = primList.indexOf(lowerNeighbor.get(circuitGate));

            while(instanceIndex > 0) {

                lowerNeighbor.put(circuitGate, primList.get(--instanceIndex));

                if (lowerNeighbor.isValid()) {
                    neighbors.add(lowerNeighbor);
                    break;
                }
            }

            Assignment upperNeighbor = new Assignment(assignment);
            instanceIndex = primList.indexOf(upperNeighbor.get(circuitGate));

            while (instanceIndex < (primList.size()) - 2) {

                upperNeighbor.put(circuitGate, primList.get(++instanceIndex));

                if (upperNeighbor.isValid()) {
                    neighbors.add(upperNeighbor);
                    break;
                }
            }
        }

        return neighbors;
    }

    private int posModulo(int a, int b) {
        return (((a % b) + b) % b);
    }
}
