package de.tu_darmstadt.rs.synbio.assignment;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.util.PermutationIterator;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.jgrapht.Graphs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class ExhaustiveAssigner {

    private static final Logger logger = LoggerFactory.getLogger(ExhaustiveAssigner.class);

    final private Circuit circuit;

    // gates
    final private HashMap<String, List<Gate>> availableGates;
    final private LinkedHashMap<String, List<Gate>> circuitGates; // linked for iteration order

    // permutation variables
    private HashMap<String, PermutationIterator<Gate>> permutationIterators;
    final private HashMap<String, Long> numPermutations;
    final private long numTotalPermutations;
    public long currentPermutation = 0;

    // assignment buffer
    private HashMap<String, List<Gate>> assignedGates;

    public ExhaustiveAssigner(GateLibrary gateLib, Circuit circuit) {

        Thread.currentThread().setPriority(Thread.NORM_PRIORITY + 1);

        this.circuit = circuit;

        // initialize gate library
        this.availableGates = gateLib.getPrimitives();

        // initialize circuit gate map
        this.circuitGates = new LinkedHashMap<>();
        this.numPermutations = new HashMap<>();
        for (String gateId : availableGates.keySet()) {
            ArrayList<Gate> gates = circuit.vertexSet().stream().filter(g -> g.getPrimitiveIdentifier().equals(gateId)).collect(Collectors.toCollection(ArrayList::new));
            circuitGates.put(gateId, gates);
            numPermutations.put(gateId, getNumAssignments(gateId));
        }

        // initialize permutation iterators
        this.permutationIterators = new HashMap<>();
        for (String gateId : availableGates.keySet()) {
            permutationIterators.put(gateId, new PermutationIterator<>(availableGates.get(gateId), circuitGates.get(gateId).size()));
        }

        this.numTotalPermutations = getNumAssignments();

        this.assignedGates = new HashMap<>();
    }

    public long getNumTotalPermutations() {
        return numTotalPermutations;
    }

    private long getNumAssignments() {

        long totalVariations = 1;

        for (String gateId : availableGates.keySet()) {
            totalVariations *= getNumAssignments(gateId);
        }

        return totalVariations;
    }

    private long getNumAssignments(String gateId) {

        int n = availableGates.get(gateId).size();
        int k = circuitGates.get(gateId).size();
        return CombinatoricsUtils.factorial(n) / CombinatoricsUtils.factorial(n - k);
    }

    private boolean nextAssignment() {

        // generate assignments until there are more and a valid one is found

        do {
            long divider = 1;

            // for every gate class, calculate if the iterator needs to be updated
            for (String gateId : circuitGates.keySet()) {

                if (!circuitGates.get(gateId).isEmpty() && currentPermutation % divider == 0) {
                    assignedGates.put(gateId, permutationIterators.get(gateId).next());
                }

                divider *= numPermutations.get(gateId);
            }

            // print assignment
            /*for (String id : assignedGates.keySet()) {
                logger.info(id + ":");
                assignedGates.get(id).forEach(g -> logger.info(g.getIdentifier() + " "));
            }
            logger.info("----------");*/

            currentPermutation++;

        } while (!groupConstraintsFulfilled() && (currentPermutation != numTotalPermutations));

        return (currentPermutation < numTotalPermutations);
    }

    private boolean groupConstraintsFulfilled() {

        List<String> usedGroups = new ArrayList<>();

        for (List<Gate> primGateClass : assignedGates.values()) {
            for (Gate gate : primGateClass) {
                if (usedGroups.contains(gate.getGroup())) {
                    return false;
                } else {
                    usedGroups.add(gate.getGroup());
                }
            }
        }

        return true;
    }

    /*
     * Generates the next assigned circuit. Returns null if there is no new assignment.
     */

    public Circuit getNextAssignedCircuit() {

        if (!nextAssignment())
            return null;

        Circuit assigned = new Circuit();
        Graphs.addGraph(assigned, circuit);

        for (String gateId : circuitGates.keySet()) {

            List<Gate> originalGates = circuitGates.get(gateId);
            List<Gate> replacements = assignedGates.get(gateId);

            for (int i = 0; i < originalGates.size(); i++) {
                assigned.replaceGate(originalGates.get(i), replacements.get(i));
            }
        }

        return assigned;
    }

    /*
     * Generates the next assignment map. Returns null if there is no new assignment.
     */

    public synchronized Assignment getNextAssignment() {

        if (!nextAssignment())
            return null;

        Assignment assignment = new Assignment();

        for (String gateId : circuitGates.keySet()) {

            List<Gate> originalGates = circuitGates.get(gateId);
            List<Gate> replacements = assignedGates.get(gateId);

            for (int i = 0; i < originalGates.size(); i++) {
                assignment.put(originalGates.get(i), replacements.get(i));
            }
        }

        return assignment;
    }
}
