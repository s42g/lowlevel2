package de.tu_darmstadt.rs.synbio.circuit;

import de.tu_darmstadt.rs.synbio.logic.TruthTable;
import org.logicng.formulas.Formula;

public interface LogicElement {

    Formula getExpression();

    TruthTable getTruthTable();

    String getIdentifier();

    Integer getWeight();
}
