package de.tu_darmstadt.rs.synbio.assignment.search;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;

public abstract class AssignmentSearchAlgorithm {

    protected Circuit structure;
    protected SimConfiguration config;
    protected GateLibrary gateLib;

    public AssignmentSearchAlgorithm(Circuit structure, GateLibrary lib, SimConfiguration config) {
        this.structure = structure;
        this.config = config;
        this.gateLib = lib;
    }

    public abstract SimulationResult simulate();

}
