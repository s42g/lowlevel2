package de.tu_darmstadt.rs.synbio.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.Properties;

public class SynConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(SynConfiguration.class);

    /* IO fields */

    private String outputDir;

    /* mapping configuration */

    private int maxDepth;
    private int maxWeight;
    private MappingMode mappingMode;
    private int limitStructuresNum;

    public enum MappingMode {
        SUPERGATES, EXHAUSTIVE
    }

    /* supergate generation configuration */

    private File supergateLibrary;
    private boolean exportSupergates;
    private int[] supergateFeasibilities;
    private int maxSupergateDepth;
    private int maxSupergateWeight;

    public SynConfiguration() {}

    public SynConfiguration(String configFile) throws Exception {

        /* config file handling */

        Properties props = new Properties();
        InputStream is = null;
        is = new FileInputStream(configFile);
        props.load(is);


        /* output folder */

        outputDir = props.getProperty("OUTPUT_DIR");

        /* mapping config handling */

        switch (props.getProperty("MAPPING_MODE")) {
            case "EXHAUSTIVE": mappingMode = MappingMode.EXHAUSTIVE; break;
            case "SUPERGATES": mappingMode = MappingMode.SUPERGATES; break;
            default: throw new IOException("Unknown mapping mode! (Available modes: " + Arrays.toString(MappingMode.values()) + ")");
        }

        maxDepth = Integer.parseInt(props.getProperty("MAPPING_DEPTH"));
        maxWeight = Integer.parseInt(props.getProperty("MAPPING_WEIGHT"));
        limitStructuresNum = Integer.parseInt(props.getProperty("MAPPING_LIMIT_STRUCTURES_NUM"));


        /* supergate config handling */

        if (mappingMode.equals(MappingMode.SUPERGATES)) {

            supergateLibrary = new File(props.getProperty("SUPERGATE_LIBRARY"));
            exportSupergates = Boolean.parseBoolean(props.getProperty("SUPERGATE_EXPORT"));

            if (!exportSupergates && !supergateLibrary.exists())
                throw new IOException("Supergate gate library file " + supergateLibrary + " does not exist.");

            if (exportSupergates) {
                String[] feasibilitiyStrings = props.getProperty("SUPERGATE_FEASIBILITIES").split(",");
                supergateFeasibilities = new int[feasibilitiyStrings.length];
                for (int i = 0; i < supergateFeasibilities.length; i++) {
                    supergateFeasibilities[i] = Integer.parseInt(feasibilitiyStrings[i]);
                }
                maxSupergateDepth = Integer.parseInt(props.getProperty("SUPERGATE_DEPTH"));
                maxSupergateWeight = Integer.parseInt(props.getProperty("SUPERGATE_WEIGHT"));
            }

        }
    }

    public void print() {

        logger.info("<-- Configuration -->");

        logger.info("IO:");
        logger.info("\toutput dir.: " + outputDir);

        logger.info("Mapping:");
        logger.info("\tmode: " + mappingMode);
        logger.info("\tmax. depth: " + maxDepth);
        logger.info("\tmax. weight: " + maxWeight);
        logger.info("\tstructures limit: " + limitStructuresNum);

        if (mappingMode.equals(MappingMode.SUPERGATES)) {
            logger.info("Supergates:");
            logger.info("\tsupergate library: " + supergateLibrary.getAbsolutePath());
            logger.info("\texport library: " + exportSupergates);
            logger.info("\tfeasibilities: " + Arrays.toString(supergateFeasibilities));
            logger.info("\tmax. depth: " + maxSupergateDepth);
            logger.info("\tmax. weight: " + maxSupergateWeight);
        }
    }

    /* getter */

    public File getSupergateLibrary() {
        return supergateLibrary;
    }

    public boolean exportSupergates() {
        return exportSupergates;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public MappingMode getMappingMode() {
        return mappingMode;
    }

    public int getLimitStructuresNum() {
        return limitStructuresNum;
    }

    public int[] getSupergateFeasibilities() {
        return supergateFeasibilities;
    }

    public int getMaxSupergateDepth() {
        return maxSupergateDepth;
    }

    public int getMaxSupergateWeight() {
        return maxSupergateWeight;
    }
}
