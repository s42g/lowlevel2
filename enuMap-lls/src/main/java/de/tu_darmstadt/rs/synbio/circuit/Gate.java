package de.tu_darmstadt.rs.synbio.circuit;

import de.tu_darmstadt.rs.synbio.logic.TruthTable;
import de.tu_darmstadt.rs.synbio.logic.LogicUtils;
import org.jgrapht.io.Attribute;
import org.jgrapht.io.VertexProvider;
import org.logicng.formulas.Formula;

import java.util.Map;

public class Gate implements LogicElement {

    private final Formula expression;
    private final String identifier;
    private final String primitiveIdentifier;
    private final String group;
    private final Type type;

    private final int numInputs;

    public enum Type {
        INPUT,
        LOGIC,
        OUTPUT
    };

    /* constructor for LOGIC gates */

    public Gate(String expression, String primitiveIdentifier, String identifier, String group) {
        this.expression = LogicUtils.parseExpression(expression);
        this.identifier = identifier;
        this.primitiveIdentifier = primitiveIdentifier;
        this.group = group;
        this.type = Type.LOGIC;

        this.numInputs = this.expression.variables().size();
    }

    /* constructor for INPUT/OUTPUT gates without primitive identifier */

    public Gate(String expression, String identifier, Gate.Type type) {
        this.expression = LogicUtils.parseExpression(expression);
        this.identifier = identifier;
        this.type = type;

        this.primitiveIdentifier = "";
        this.group = "";

        this.numInputs = this.expression.variables().size();
    }

    /* constructor that directly takes formulas */

    public Gate(Formula expression, String primitiveIdentifier, String identifier, Gate.Type type) {
        this.expression = expression;
        this.identifier = identifier;
        this.primitiveIdentifier = primitiveIdentifier;
        this.type = type;

        this.group = "";

        this.numInputs = this.expression.variables().size();
    }

    private double vm = 0.0;

    public void setVm(double vm) {
        this.vm = vm;
    }

    public double getVm() {
        return vm;
    }

    @Override
    public Formula getExpression() {
        return expression;
    }

    @Override
    public TruthTable getTruthTable() {
        return new TruthTable(expression);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    public String getPrimitiveIdentifier() {
        return primitiveIdentifier;
    }

    public String getGroup() {
        return group;
    }

    @Override
    public Integer getWeight() {

        int weight;

        switch (type) {
            case INPUT:     weight = 0; break;
            case OUTPUT:    weight = 0; break;
            //case LOGIC:     weight = numInputs; break;
            default:        weight = 1;
        }

        return weight;
    }

    public Type getType() {
        return type;
    }

    public int getNumInputs() {
        return numInputs;
    }

    static class GateProvider implements VertexProvider<Gate> {

        @Override
        public Gate buildVertex(String s, Map<String, Attribute> map) {

            Formula expression = LogicUtils.parseExpression(map.get("expression").getValue());
            String primitiveIdentifier = map.get("primitiveIdentifier").getValue();

            Gate.Type type;

            switch(map.get("type").getValue()) {
                case "INPUT": type = Gate.Type.INPUT; break;
                case "OUTPUT": type = Gate.Type.OUTPUT; break;
                default: type = Gate.Type.LOGIC;
            }

            return new Gate(expression, primitiveIdentifier, s, type);
        }
    }
}
