package de.tu_darmstadt.rs.synbio;

import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.configuration.SynConfiguration;
import de.tu_darmstadt.rs.synbio.logic.LogicUtils;
import de.tu_darmstadt.rs.synbio.logic.TruthTable;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        // parse command line arguments

        Options options = new Options();

        /* input */

        Option function = new Option("i", "input", true, "input function");
        options.addOption(function);

        Option truthTable = new Option("t", "truthtable", false, "given input is decimal truth table (value,length)");
        options.addOption(truthTable);

        /* library */

        Option gateLibraryFile = new Option("l", "library", true, "path of the gate library file");
        options.addOption(gateLibraryFile);

        /* config files */

        Option synConfigFile = new Option("c", "synConfig", true, "path of the synthesis configuration file");
        options.addOption(synConfigFile);

        Option simConfigFile = new Option("s", "simConfig", true, "path of the simulation configuration file");
        options.addOption(simConfigFile);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            logger.error(e.getMessage());
            formatter.printHelp("enuMap", options);
            System.exit(1);
            return;
        }

        // sanity check arguments

        if (!cmd.hasOption("input") || !cmd.hasOption("library") || !cmd.hasOption("synConfig")) {
            logger.error("Input function, gate library file or synthesis config file not given!");
            formatter.printHelp("enuMap", options);
            System.exit(1);
            return;
        }

        SynConfiguration synConfig = new SynConfiguration(cmd.getOptionValue("synConfig"));
        synConfig.print();

        SimConfiguration simConfig;
        if (cmd.hasOption("simConfig")) {
            simConfig = new SimConfiguration(cmd.getOptionValue("simConfig"));
            simConfig.print();
        } else {
            simConfig = null;
            logger.info("No simulation configuration file given. Skipping simulation.");
        }

        /* input handling */

        TruthTable inputTruthTable;

        if (!cmd.hasOption("truthtable")) {
            inputTruthTable = new TruthTable(LogicUtils.parseExpression(cmd.getOptionValue("input")));
        } else {
            String decTT = Integer.parseInt(cmd.getOptionValue("input"), 2) + "," + cmd.getOptionValue("input").length();
            inputTruthTable = new TruthTable(decTT);
        }

        // call main program

        EnuMap mapper = new EnuMap(inputTruthTable, cmd.getOptionValue("library"), synConfig, simConfig);
        mapper.map();
    }
}
