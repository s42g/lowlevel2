package de.tu_darmstadt.rs.synbio.assignment;

import de.tu_darmstadt.rs.synbio.circuit.Gate;

import java.util.*;

public class Assignment {

    public Map<Gate, Gate> map;

    public Assignment() {
        this.map = new HashMap<>();
    }

    public Assignment(Assignment assignment) {
        this.map = new HashMap<>(assignment.map);
    }

    public void put(Gate circuitGate, Gate instance) {
        map.put(circuitGate, instance);
    }

    public Set<Gate> keySet() {
        return map.keySet();
    }

    public Gate get(Gate circuitGate) {
        return map.get(circuitGate);
    }

    public Collection<Gate> values() {
        return map.values();
    }

    @Override
    public boolean equals(Object o) {

        if (o == this)
            return true;

        if (!(o instanceof Assignment))
            return false;

        return map.equals(((Assignment) o).map);
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    public Map<String, String> getIdentifierMap() {

        Map<String, String> stringMap = new HashMap<>();

        for (Gate circuitGate : map.keySet()) {
            stringMap.put(circuitGate.getIdentifier(), map.get(circuitGate).getIdentifier());
        }

        return stringMap;
    }

    public boolean isValid() {

        // check gate instance redundancy
        List<Gate> instanceList = new ArrayList<>(map.values());
        Set<Gate> instanceSet = new HashSet<>(map.values());

        if (instanceList.size() != instanceSet.size()) {
            return false;
        }

        // check group constraints
        List<String> usedGroups = new ArrayList<>();

        for (Gate instance : map.values()) {
            if (usedGroups.contains(instance.getGroup())) {
                return false;
            } else {
                usedGroups.add(instance.getGroup());
            }
        }

        return true;
    }

    public boolean equals(Assignment a){
        int out = 0;
        for(Map.Entry<Gate,Gate> entryThis: map.entrySet()){
            for(Map.Entry<Gate,Gate> entryThat: a.map.entrySet()){
                if(entryThis.equals(entryThat))
                    out+=1;
                    break;
            }
        }
        if(out==3) {//alle einträge sind gleich
            return true;
        }
        return false;
    }
}
