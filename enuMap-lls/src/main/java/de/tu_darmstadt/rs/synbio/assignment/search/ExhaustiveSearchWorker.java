package de.tu_darmstadt.rs.synbio.assignment.search;

import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.assignment.Assignment;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.assignment.ExhaustiveAssigner;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import de.tu_darmstadt.rs.synbio.simulation.SimulatorInterface;

import java.util.concurrent.Callable;

public class ExhaustiveSearchWorker implements Callable<SimulationResult> {

    private SimulatorInterface simulator;
    private ExhaustiveAssigner assigner;
    private Circuit structure;
    private SimConfiguration config;

    public ExhaustiveSearchWorker(ExhaustiveAssigner assigner, Circuit structure, SimConfiguration config, GateLibrary gateLibrary) {
        this.config = config;
        this.simulator = new SimulatorInterface(config, gateLibrary.getSourceFile());
        this.assigner = assigner;
        this.structure = structure;
    }

    @Override
    public SimulationResult call() throws Exception {

        simulator.initSimulation(structure);

        Assignment assignment = assigner.getNextAssignment();
        SimulationResult bestRes = null;

        while (assignment != null && !Thread.interrupted()) {
            SimulationResult result = new SimulationResult(structure, assignment, simulator.simulate(assignment));

            if (bestRes == null || (config.getOptimizationType().compare(bestRes.getScore(), result.getScore()))) {
                bestRes = result;
            }

            assignment = assigner.getNextAssignment();
        }

        simulator.shutdown();

        return bestRes;
    }
}
