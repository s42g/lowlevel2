package de.tu_darmstadt.rs.synbio.assignment.search;

import de.tu_darmstadt.rs.synbio.assignment.ExhaustiveAssigner;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExhaustiveSearch extends AssignmentSearchAlgorithm {

    private static final Logger logger = LoggerFactory.getLogger(ExhaustiveSearch.class);

    private ExhaustiveAssigner assigner;

    public ExhaustiveSearch(Circuit structure, GateLibrary lib, SimConfiguration config) {
        super(structure, lib, config);
        assigner = new ExhaustiveAssigner(lib, structure);
    }

    public SimulationResult simulate() {

        List<ExhaustiveSearchWorker> workers = new ArrayList<>();
        int maxThreads = Runtime.getRuntime().availableProcessors() - 1;
        int availableProcessors = config.simLimitThreads() ? Math.min(config.getSimLimitThreadsNum(), maxThreads) : maxThreads;

        logger.info("Simulating \"" + structure.getIdentifier() + "\" (up to " + assigner.getNumTotalPermutations() + " assignments) with " + availableProcessors + " threads");

        for (int i = 0; i < availableProcessors; i ++) {
            workers.add(new ExhaustiveSearchWorker(assigner, structure, config, gateLib));
        }

        ExecutorService executor = Executors.newFixedThreadPool(availableProcessors);

        List<Future<SimulationResult>> simResults = Collections.emptyList();

        try {
            simResults = executor.invokeAll(workers);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        SimulationResult bestRes = null;

        for (Future<SimulationResult> result : simResults) {

            try {
                SimulationResult res = result.get();

                if (res != null && (bestRes == null || (config.getOptimizationType().compare(bestRes.getScore(), res.getScore())))) {
                    bestRes = res;
                }

            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        logger.info("Finished simulating " + structure.getIdentifier() + ", score: " + (bestRes != null ? bestRes.getScore() : 0));

        return bestRes;
    }
}
