package de.tu_darmstadt.rs.synbio.output;

import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.circuit.Wire;
import de.tu_darmstadt.rs.synbio.matching.Match;
import org.logicng.formulas.Variable;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CircuitBuilder {

    private static int gateCounter;
    private static Map<Match, Gate> matchAnchors;

    public static Circuit buildOutputCircuit(HashMap<AIGNode, Match> cover, AIGNode outputNode) {

        Circuit circuit = new Circuit();
        gateCounter= 0;
        matchAnchors = new HashMap<>();

        // add output node
        Gate outputGate = new Gate("x",  "O", Gate.Type.OUTPUT);
        circuit.addVertex(outputGate);

        // get first logic node after output and start adding matches to circuit
        Gate logicStartGate = addMatchToCircuit(cover.get(outputNode), circuit, cover);

        // connect output node to logic
        circuit.addEdge(logicStartGate, outputGate, new Wire(logicStartGate.getExpression().variables().first()));

        return circuit;
    }


    private static Gate addMatchToCircuit(Match match, Circuit circuit, HashMap<AIGNode, Match> cover) {

        // if match has already been added --> return anchor gate in output circuit of that match
        if (matchAnchors.get(match) != null)
            return matchAnchors.get(match);

        Map<String, Gate> addedMatchGates = new HashMap<>();

        // get first logic node of match circuit after output node
        Circuit matchCircuit = match.getCircuit();
        Gate firstLogicNode = matchCircuit.getEdgeSource(matchCircuit.incomingEdgesOf(matchCircuit.getOutputBuffer()).stream().findFirst().get());

        // start adding match gates recursively
        Gate matchGate = addGateToCircuit(firstLogicNode, match, addedMatchGates, circuit, cover);
        matchAnchors.putIfAbsent(match, matchGate);
        return matchGate;
    }

    private static Gate addGateToCircuit(Gate gate, Match match, Map<String, Gate> addedMatchGates, Circuit circuit, HashMap<AIGNode, Match> cover) {

        Gate createdGate = null;

        if (gate.getType() == Gate.Type.INPUT) {

            // get AIG variable that is mapped to the input gate
            Variable var = match.getInputMapping().get(gate);

            // find corresponding AIG node in cover to be added
            AIGNode nextCoverNode = null;
            for (AIGNode coverNode : cover.keySet()) {
                if (coverNode.getIdentifier().equals(var.name())) {
                    nextCoverNode = coverNode;
                    break;
                }
            }

            // if cover node found, add its match to the circuit and get first gate
            if (nextCoverNode != null) {
                createdGate = addMatchToCircuit(cover.get(nextCoverNode), circuit, cover);

            // if no cover node found, find existing input gate or create new one
            } else {
                Optional<Gate> existingInputGate = circuit.vertexSet().stream()
                        .filter(vertex -> vertex.getType().equals(Gate.Type.INPUT))
                        .filter(inputGate -> inputGate.getExpression().equals(var))
                        .findFirst();

                if (existingInputGate.isPresent()) {
                    createdGate = existingInputGate.get();
                } else {
                    createdGate = new Gate(var, "", var.name(), Gate.Type.INPUT);
                    circuit.addVertex(createdGate);
                }
            }
        }

        // if gate is a LOGIC gate that has not been added, add a copy to the circuit
        else if (gate.getType() == Gate.Type.LOGIC) {

            if (addedMatchGates.containsKey(gate.getIdentifier()))
                createdGate = addedMatchGates.get(gate.getIdentifier());
            else {
                createdGate = new Gate(gate.getExpression(), gate.getPrimitiveIdentifier(), gate.getPrimitiveIdentifier() + "_" + gateCounter, Gate.Type.LOGIC);
                addedMatchGates.put(gate.getIdentifier(), createdGate);
                circuit.addVertex(createdGate);
                gateCounter ++;
            }
        }

        // add gates of incoming edges recursively
        for (Wire incomingWire : match.getCircuit().incomingEdgesOf(gate)) {
            Gate connectedGate = addGateToCircuit(match.getCircuit().getEdgeSource(incomingWire), match, addedMatchGates, circuit, cover);

            // connect the created and connected gate
            if (createdGate != null && connectedGate != null)
                circuit.addEdge(connectedGate, createdGate, new Wire(incomingWire.getVariable()));
        }
        return createdGate;
    }
}
