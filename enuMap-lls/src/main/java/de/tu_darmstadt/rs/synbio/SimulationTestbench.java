package de.tu_darmstadt.rs.synbio;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import de.tu_darmstadt.rs.synbio.assignment.search.AssignmentSearchAlgorithm;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.CircuitDeserializer;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;

public class SimulationTestbench {

    private static final Logger logger = LoggerFactory.getLogger(SimulationTestbench.class);

    public static void main(String[] args) throws Exception {

        Options options = new Options();

        Option inputDirString = new Option("i", "inputDir", true, "path to the directory containing the input structure files");
        options.addOption(inputDirString);
        Option gateLibraryFile = new Option("l", "library", true, "path of the gate library file");
        options.addOption(gateLibraryFile);
        Option simConfigFile = new Option("s", "simConfig", true, "path of the simulation configuration file");
        options.addOption(simConfigFile);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            logger.error(e.getMessage());
            formatter.printHelp("AssignmentBenchmark", options);
            System.exit(1);
            return;
        }

        if (!cmd.hasOption("inputDir") || !cmd.hasOption("library") || !cmd.hasOption("simConfig")) {
            logger.error("Input directory, gate library file or simulation config file not given!");
            formatter.printHelp("enuMap", options);
            System.exit(1);
            return;
        }

        logger.info("loading simulation config file: " + cmd.getOptionValue("simConfig"));
        SimConfiguration simConfig = new SimConfiguration(cmd.getOptionValue("simConfig"));
        simConfig.print();

        GateLibrary gateLib = new GateLibrary(new File(cmd.getOptionValue("library")));

        File inputDir = new File(cmd.getOptionValue("inputDir"));
        logger.info ("processing input directory " + inputDir.getAbsolutePath());

        File output = new File(inputDir, "results_" + System.currentTimeMillis() + ".txt");
        PrintWriter out;
        try {
            out = new PrintWriter(output);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        // load structure file, deserialize

        long startTime = System.currentTimeMillis();

        File[] directoryListing = inputDir.listFiles();

        if (directoryListing == null) {
            logger.info("Empty input directory.");
            return;
        }

        Arrays.sort(directoryListing);


        for (File child : directoryListing) {

            // test if json
            if (!child.getName().endsWith(".json"))
                continue;

            Circuit structure = null;

            final ObjectNode node;
            ObjectMapper mapper = new ObjectMapper();
            CircuitDeserializer circuitDeserializer = new CircuitDeserializer(Circuit.class);

            try {
                node = mapper.readValue(child, ObjectNode.class);

                if (node.has("graph")) {
                    structure = circuitDeserializer.deserializeString(node.get("graph").toString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            if (structure != null) {
                AssignmentSearchAlgorithm search =  simConfig.getSearchAlgorithm(structure, gateLib, simConfig);
                SimulationResult result = search.simulate();

                try {
                    out.println(child.getName() + "," + result.getScore() + "," + mapper.writeValueAsString(result.getAssignment().getIdentifierMap()));
                    logger.info("--- Score: "+ child.getName()+result.getScore());
                } catch (Exception e) {
                    logger.info("--- Score: 0.0");
                    e.printStackTrace();
                }

                out.flush();
            }

        }

        out.println("total time: " + (System.currentTimeMillis() - startTime) + " ms");
        out.close();

        System.exit(0);
    }

}
