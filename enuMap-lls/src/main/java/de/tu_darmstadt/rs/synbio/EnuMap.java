package de.tu_darmstadt.rs.synbio;


import com.fasterxml.jackson.databind.ObjectMapper;
import de.tu_darmstadt.rs.synbio.configuration.SimConfiguration;
import de.tu_darmstadt.rs.synbio.configuration.SynConfiguration;
import de.tu_darmstadt.rs.synbio.covering.StructuralCoverer;
import de.tu_darmstadt.rs.synbio.matching.Match;
import de.tu_darmstadt.rs.synbio.matching.MatchingAlgorithm;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.logic.*;
import de.tu_darmstadt.rs.synbio.aig.AIG;
import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import de.tu_darmstadt.rs.synbio.simulation.SimulationResult;
import de.tu_darmstadt.rs.synbio.assignment.search.ExhaustiveSearch;
import de.tu_darmstadt.rs.synbio.util.AbcWrapper;
import de.tu_darmstadt.rs.synbio.util.BenchParser;

import org.logicng.formulas.Formula;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EnuMap {

    private static final Logger logger = LoggerFactory.getLogger(EnuMap.class);

    private TruthTable inputTruthTable;
    private File outputDir;
    private File gateLibraryFile;

    private SynConfiguration synConfig;
    private SimConfiguration simConfig;

    public EnuMap(TruthTable inputTruthTable, String libraryPath, SynConfiguration synConfig, SimConfiguration simConfig) throws IOException {

        this.inputTruthTable = inputTruthTable;
        this.synConfig = synConfig;
        this.simConfig = simConfig;

        /* output dir handling */

        outputDir = (synConfig.getOutputDir() == null) ? new File("run_" + System.currentTimeMillis() + "_" + inputTruthTable.toString()) :
                new File(synConfig.getOutputDir(), "run_" + System.currentTimeMillis() + "_" + inputTruthTable.toString());

        if (!this.outputDir.mkdirs())
            throw new IOException("Error creating output directory " + outputDir.getAbsolutePath());

        /* library file handling */

        gateLibraryFile = new File(libraryPath);

        if (!gateLibraryFile.exists())
            throw new IOException("Primitive gate library file " + gateLibraryFile + " does not exist.");
    }

    public void map() {

        long startTime = System.currentTimeMillis();

        /* Logic Minimization by ABC */

        logger.info("<-- Logic minimization -->");
        AbcWrapper abc = new AbcWrapper(outputDir);
        File aigFile = abc.thruthTableToAIG(inputTruthTable.toString(), "aig.bench");

        /* Parse bench file into AIG */

        logger.info("<-- Parse generated AIG -->");
        AIG aig = BenchParser.parse(aigFile);
        aig.expandInverterEdges();
        aig.print(new File(outputDir, "parsed_aig.dot"));

        Formula aigExpression = aig.getExpression();
        TruthTable aigTruthTable = new TruthTable(aigExpression);
        logger.info("Parsed AIG: " + aigTruthTable.toString() + " " + aigExpression);

        if (!aigTruthTable.equalsLogically(inputTruthTable))
            logger.error("Parsed AIG is not equivalent to input function!");

        /* initialize gate library */

        logger.info("<-- Loading gate library -->");
        final GateLibrary gateLib = new GateLibrary(gateLibraryFile);
        SupergateLibrary supergateLib = new SupergateLibrary();

        Enumerator enumerator;
        long enumTime = System.currentTimeMillis();

        if (synConfig.getMappingMode().equals(SynConfiguration.MappingMode.EXHAUSTIVE)) {

            enumerator = new Enumerator(gateLib, inputTruthTable, synConfig.getMaxDepth(), synConfig.getMaxWeight());
            enumerator.enumerate();
            supergateLib.addSupergates(enumerator.getResultCircuits());
            logger.info("Enumeration time: " + (System.currentTimeMillis() - enumTime) + " ms");

        } else {

            if (synConfig.exportSupergates()) {

                for (int feasibility : synConfig.getSupergateFeasibilities()) {
                    enumerator = new Enumerator(gateLib, feasibility, synConfig.getMaxSupergateDepth(), synConfig.getMaxSupergateWeight());
                    enumerator.enumerate();
                    supergateLib.addSupergates(enumerator.getResultCircuits());
                }

                logger.info("Supergate generation time: " + (System.currentTimeMillis() - enumTime) + " ms");
                supergateLib.save(synConfig.getSupergateLibrary());

            } else {
                supergateLib.load(synConfig.getSupergateLibrary());
            }

        }

        gateLib.print();
        supergateLib.print();

        int libraryFeasibility = Math.max(gateLib.getFeasibility(), supergateLib.getFeasibility());

        /* compute cuts */

        logger.info("<-- Cut computation -->");
        HashMap<AIGNode, HashSet<HashSet<AIGNode>>> cuts = aig.computeCuts(libraryFeasibility);
        logger.info("Found " + cuts.values().stream().flatMap(HashSet::stream).mapToInt(HashSet::size).sum()
                + " " + libraryFeasibility + "-feasible cuts.");

        /* find gate matches for all cuts */

        logger.info("<-- Boolean matching -->");
        HashMap<TruthTable, Circuit> mergedLib = new HashMap<>(gateLib.get());
        mergedLib.putAll(supergateLib.get());
        HashMap<AIGNode, HashSet<Match>> matches = MatchingAlgorithm.booleanMatching(cuts, aig, mergedLib);
        logger.info("Found " + matches.values().stream().mapToInt(HashSet::size).sum() + " matches.");

        /* find covers of matches */

        logger.info("<-- Cover calculation -->");

        StructuralCoverer coverer = new StructuralCoverer(gateLib, aig, matches, synConfig.getMaxDepth(), synConfig.getMaxWeight());
        HashMap<HashMap<AIGNode, Match>, Circuit> outputCircuits = coverer.enumerateCovers();

        /* output generation */

        logger.info("<-- Output generation -->");

        if (outputCircuits.isEmpty()) {
            logger.info("No circuit structures found.");
            return;
        }

        HashMap<Integer, Integer> histogram = new HashMap<>();

        int i = 0;

        for(HashMap<AIGNode, Match> cover : outputCircuits.keySet()) {
            i ++;
            Circuit outputCircuit = outputCircuits.get(cover);

            Formula outputExpression = outputCircuit.getExpression();
            logger.info("");
            logger.info("Circuit structure " + i + ": " + outputExpression.toString());

            TruthTable outputTruthTable = new TruthTable(outputExpression);
            if (!outputTruthTable.equalsLogically(inputTruthTable)) {
                logger.error("Circuit function " + outputTruthTable + " is not equivalent to input function!");
            }

            for(AIGNode node : cover.keySet()) {
                logger.info(node.getIdentifier() + " " + aig.getExpression(node, cover.get(node).getCut()) + ": " + cover.get(node).getCircuit().getIdentifier());
            }

            int numberLogicGates = outputCircuit.getNumberLogicGates();
            histogram.putIfAbsent(numberLogicGates, 0);
            histogram.put(numberLogicGates, histogram.get(numberLogicGates) + 1);
        }

        logger.info("");
        int minWeight = outputCircuits.values().stream().mapToInt(Circuit::getWeight).min().getAsInt();
        logger.info("Minimum circuit structure weight: " + minWeight);
        logger.info(outputCircuits.size() + " circuit structures found.");

        logger.info("Gate number distribution: ");
        for(Integer gateNum : histogram.keySet()) {
            logger.info(gateNum + "," + histogram.get(gateNum));
        }

        //TODO: remove/make systematic
        //logger.info("Limiting output to circuits with weight <= " + (minWeight + 1) + " and <= 7");
        List<Circuit> sortedStructures = outputCircuits.values().stream()
                //.filter(c -> c.getWeight() <= minWeight + 1)
                //.filter(c -> c.vertexSet().stream().anyMatch(g -> g.getPrimitiveIdentifier().equals("OR2")) ? c.getWeight() <= 8 : c.getWeight() <= 7)
                .sorted()
                .collect(Collectors.toCollection(ArrayList::new));

        if (sortedStructures.size() > synConfig.getLimitStructuresNum()) {
            logger.info("Limiting output to " + synConfig.getLimitStructuresNum() + " minimal structures.");
            sortedStructures = sortedStructures.subList(sortedStructures.size() - synConfig.getLimitStructuresNum(), sortedStructures.size());
        }

        for (i = 0; i < sortedStructures.size(); i ++) {
            Circuit circ = sortedStructures.get(i);
            circ.setIdentifier("structure_" + i);
            circ.print(new File(outputDir, circ.getIdentifier() + ".dot"));
            //circ.save(new File(outputDir, circ.getIdentifier() + ".json"));
        }

        /* simulation */

        logger.info("<-- simulation -->");

        if (simConfig!= null && simConfig.isSimEnabled()) {

            SimulationResult bestRes = null;

            for (i = sortedStructures.size() - 1; i >= 0; i--) {
                ExhaustiveSearch sim = new ExhaustiveSearch(sortedStructures.get(i), gateLib, simConfig);
                //TabuSearchSimulation sim = new TabuSearchSimulation(sortedStructures.get(i), gateLib, simConfig);
                try {
                    SimulationResult result = sim.simulate();
                    //ObjectMapper mapper = new ObjectMapper();
                    //mapper.writerWithDefaultPrettyPrinter().writeValue(new File(outputDir, result.getStructure().getIdentifier() + "_best_assignment.json"), result.getAssignment());

                    if (bestRes == null || result.getScore() > bestRes.getScore())
                        bestRes = result;

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (bestRes != null) {
                bestRes.getStructure().save(new File(outputDir, bestRes.getStructure().getIdentifier() + ".json"));
                bestRes.getStructure().print(new File(outputDir, bestRes.getStructure().getIdentifier() + ".dot"));

                try {
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.writerWithDefaultPrettyPrinter().writeValue(new File(outputDir, bestRes.getStructure().getIdentifier() + "_assignment.json"), bestRes.getAssignment());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                logger.info("Best result: " + bestRes.getStructure().getIdentifier() + ", score: " + bestRes.getScore());
            }
        }

        logger.info("total time: " + (System.currentTimeMillis() - startTime) + " ms");

        logger.info("<-- finished -->");

        System.exit(0);
    }
}
