package de.tu_darmstadt.rs.synbio.covering;

import de.tu_darmstadt.rs.synbio.aig.AIG;
import de.tu_darmstadt.rs.synbio.aig.AIGNode;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import de.tu_darmstadt.rs.synbio.matching.Match;
import de.tu_darmstadt.rs.synbio.output.CircuitBuilder;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class StructuralCoverer {

    private static final Logger logger = LoggerFactory.getLogger(StructuralCoverer.class);

    private GateLibrary gateLib;
    private int maxWeight;
    private int maxDepth;

    private AIG aig;
    private HashMap<AIGNode, HashSet<Match>> matches;

    public StructuralCoverer(GateLibrary gateLib, AIG aig, HashMap<AIGNode, HashSet<Match>> matches, int maxDepth, int maxWeight) {

        this.gateLib = gateLib;
        this.aig = aig;
        this.matches = matches;
        this.maxDepth = maxDepth;
        this.maxWeight = maxWeight;
    }

    public HashMap<HashMap<AIGNode, Match>, Circuit> enumerateCovers() {

        HashSet<HashMap<AIGNode, Match>> startCovers = new HashSet<>();
        startCovers.add(new HashMap<>());

        HashSet<HashMap<AIGNode, Match>> covers = enumerateCovers(aig.getOutputNode(), startCovers);

        logger.info("Found " + covers.size() + " unfiltered covers.");

        // add complete covers to output circuits

        int redundantInv = 0;
        int completeness = 0;
        int redundantGates = 0;
        int redundantCirc = 0;

        HashMap<HashMap<AIGNode, Match>, Circuit> outputCircuits = new HashMap<>();

        for(HashMap<AIGNode, Match> cover : covers) {

            Circuit coverCircuit = CircuitBuilder.buildOutputCircuit(cover, aig.getOutputNode());
            Formula coverExpression = coverCircuit.getExpression();

            // check if circuit has redundant inverters in a row
            if (hasRedundantInverters(coverCircuit)) {
                redundantInv++;
                continue;
            }

            // check circuit completeness
            if (!aig.getInputVariables().containsAll(coverExpression.variables())) {
                completeness++;
                continue;
            }

            // merge redundant gates
            if (!coverCircuit.removeRedundantGates()) {
                redundantGates++;
                continue;
            }

            // check depth
            int depth = coverCircuit.getDepth();
            if (depth > maxDepth) {
                continue;
            }

            // OR-Gate Quick fix
            if (hasIllegalOrGate(coverCircuit))
                continue;

            // check if equivalent circuit is already present in output
            if (outputCircuits.values().stream().anyMatch(c -> c.isEquivalent(coverCircuit))) {
                redundantCirc++;
                continue;
            }

            outputCircuits.put(cover, coverCircuit);
        }

        logger.info("filtered circuits: " + redundantInv + " redundant inverters, " + completeness + " incomplete circuits, " + redundantGates +
                " non-mergeable parallel gates, " + redundantCirc + " redundant circuits");

        return outputCircuits;
    }

    private HashSet<HashMap<AIGNode, Match>> enumerateCovers(AIGNode node, HashSet<HashMap<AIGNode, Match>> nodeCovers) {

        if ((node.getType() == AIGNode.NodeType.INPUT)) {
            return nodeCovers;
        }

        if (matches.get(node) == null || matches.get(node).isEmpty()) {
            logger.info("No match found for node " + node.getIdentifier() + ". Discarding cover.");
            return nodeCovers;
        }

        HashSet<HashMap<AIGNode, Match>> covers = new HashSet<>();

        // new recursion for every match for the node
        for (Match match : matches.get(node)) {

            HashSet<HashMap<AIGNode, Match>> matchCovers = new HashSet<>();

            for(HashMap<AIGNode, Match> cover : nodeCovers) {

                // check if match gates are available in library
                if (!isCoveredByLibrary(cover.values(), match))
                    continue;

                if (getWeight(cover.values(), match) > maxWeight)
                    continue;

                HashMap<AIGNode, Match> newCover = new HashMap<>(cover);
                newCover.put(node, match);
                matchCovers.add(newCover);
            }

            for (Variable variable : match.getInputMapping().values()) {
                AIGNode nextNode = aig.vertexSet().stream().filter(n -> n.getIdentifier().equals(variable.name())).findFirst().get();
                matchCovers.addAll(enumerateCovers(nextNode, matchCovers));
            }

            covers.addAll(matchCovers);
        }

        return covers;
    }

    //TODO: add checking of gate group constraints
    private boolean isCoveredByLibrary(Collection<Match> currentMatches, Match newMatch) {

        HashSet<Match> newCover = new HashSet<>(currentMatches);
        newCover.add(newMatch);

        // build gate count map for current cover and match

        HashMap<String, Integer> gateCount = new HashMap<>();

        for (Match match : newCover) {

            for (Gate gate : match.getCircuit().vertexSet()) {

                if (gate.getType() == Gate.Type.LOGIC) {

                    if (!gateCount.containsKey(gate.getPrimitiveIdentifier()))
                        gateCount.put(gate.getPrimitiveIdentifier(), 0);

                    gateCount.put(gate.getPrimitiveIdentifier(), gateCount.get(gate.getPrimitiveIdentifier()) + 1);
                }
            }
        }

        // check if enough gates are available in library

        for (String gateIdentifier : gateCount.keySet()) {

            if (gateCount.get(gateIdentifier) > gateLib.getNumAvailableGates(gateIdentifier)) {
                //logger.info("Exceeded number of " + gateIdentifier + " gates available in library. (" + gateCount.get(gateIdentifier) + " > " + gateLib.getNumAvailableGates(gateIdentifier) + ")");
                return false;
            }
        }

        return true;
    }

    private int getWeight(Collection<Match> currentMatches, Match newMatch) {
        return newMatch.getCircuit().getWeight() + currentMatches.stream().mapToInt(m -> m.getCircuit().getWeight()).sum();
    }

    private boolean hasRedundantInverters(Circuit circuit) {

        // filter out inverters in series
        for (Gate gate1 : circuit.vertexSet().stream().filter(gate -> gate.getPrimitiveIdentifier().startsWith("NOT")).collect(Collectors.toCollection(ArrayList::new))) {
            for (Gate gate2 : circuit.vertexSet().stream().filter(gate -> gate.getPrimitiveIdentifier().startsWith("NOT")).collect(Collectors.toCollection(ArrayList::new))) {
                if (circuit.containsEdge(gate1, gate2) || circuit.containsEdge(gate2, gate1)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasIllegalOrGate(Circuit circuit) {

        boolean hasOrGate = circuit.vertexSet().stream().filter(g -> g.getType().equals(Gate.Type.LOGIC)).anyMatch(g -> g.getPrimitiveIdentifier().equals("OR2"));
        boolean hasOutputOrGate = circuit.edgesOf(circuit.getOutputBuffer()).stream().map(circuit::getEdgeSource).anyMatch(g -> g.getPrimitiveIdentifier().equals("OR2"));

        return hasOrGate && !hasOutputOrGate;
    }
}
