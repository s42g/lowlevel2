package de.tu_darmstadt.rs.synbio.util;

import java.io.*;

public class AbcWrapper {

    private final File outputDir;

    public AbcWrapper(File outputDir) {
        this.outputDir = outputDir;
    }

    public File thruthTableToAIG(String thruthTable, String outputFileName) {

        File outputFile = new File(outputDir, outputFileName);

        /*String abcCommand = "abc -c \"read_truth -x " + thruthTable + "; strash; resyn2; " +
                "write " + outputFile.getAbsolutePath() + "; write_dot " + outputFile.getAbsolutePath() + ".dot; quit\"";*/

        try {
            ProcessBuilder pb = new ProcessBuilder("./abc", "-c read_truth -x " + thruthTable + "; strash; rw -l -z; rf -l -z; rw -l -z; rf -l -z; rw -l -z; rf -l -z; b; " +
                    "write " + outputFile.getAbsolutePath() + "; write_dot " + outputFile.getAbsolutePath() + ".dot; quit");

            File log = new File(outputDir, "abc_log.txt");
            pb.redirectErrorStream(true);
            pb.redirectOutput(ProcessBuilder.Redirect.appendTo(log));
            Process p = pb.start();
            p.waitFor();
            assert pb.redirectInput() == ProcessBuilder.Redirect.PIPE;
            assert pb.redirectOutput().file() == log;
            assert p.getInputStream().read() == -1;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return outputFile;
    }
}
