package de.tu_darmstadt.rs.synbio.logic;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.circuit.Wire;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.jgrapht.Graphs;
import org.logicng.formulas.Formula;
import org.logicng.formulas.Variable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class Enumerator {

    private static final Logger logger = LoggerFactory.getLogger(Enumerator.class);

    private TruthTable targetTruthTable;
    private GateLibrary gateLib;
    private int maxDepth;
    private int feasibility;
    private int maxWeight;

    public Enumerator(GateLibrary gateLib, TruthTable targetTruthTable, int maxDepth, int maxWeight) {

        this(gateLib, targetTruthTable.getSupportSize(), maxDepth, maxWeight);
        this.targetTruthTable = targetTruthTable;
    }

    public Enumerator(GateLibrary gateLib, int feasibility,  int maxDepth, int maxWeight) {

        this.gateLib = gateLib;
        this.maxDepth = maxDepth;
        this.maxWeight = maxWeight;
        this.feasibility = feasibility;

        this.primitiveGates = gateLib.getGateClasses();

        // save index of inverter in primitive gates list
        OptionalInt inverterIndex = primitiveGates.stream().filter(g -> g.getPrimitiveIdentifier().equals("NOT")).mapToInt(g -> primitiveGates.indexOf(g)).findFirst();
        this.inverterId = inverterIndex.isEmpty() ? -1 : inverterIndex.getAsInt();

        initTemplateCircuit(feasibility);
    }

    private Circuit templateCircuit;
    private int gateCounter;
    private List<Gate> primitiveGates;
    private int inverterId;
    private List<List<List<Integer>>> intermediateCircuits;
    private HashMap<TruthTable, Circuit> resultCircuits;

    /* initialization */

    private void initTemplateCircuit(int feasibility) {

        templateCircuit = new Circuit();

        // add input and output nodes
        for (int i = 0; i < feasibility; i++) {
            templateCircuit.addVertex(new Gate(Character.toString('a' + i), Character.toString('a' + i), Gate.Type.INPUT));
        }
        Gate output = new Gate("x", "F", Gate.Type.OUTPUT);
        templateCircuit.addVertex(output);
    }

    /* helper functions */

    //TODO: add checking of gate group constraints
    private boolean isCoveredByLibrary(List<Integer> row) {

        for (int primIndex = 0; primIndex < primitiveGates.size(); primIndex ++) {

            int occurences = 0;

            for (int i = 0; i < row.size(); i ++) {
                if (row.get(i) == primIndex + 1)
                    occurences ++;
            }

            if (occurences > gateLib.getNumAvailableGates(primitiveGates.get(primIndex).getPrimitiveIdentifier()))
                return false;
        }

        return true;
    }

    private int getNumberOfInputs(List<Integer> row) {

        int numInputs = 0;

        for (int column = 0; column < row.size(); column ++) {

            int primId = row.get(column) - 1;

            if (primId >= 0)
                numInputs += primitiveGates.get(primId).getNumInputs();
        }

        return numInputs;
    }

    private int getNumberOfUnboundInputs(List<List<Integer>> circuit) {

        // get total number of gate inputs
        int totalInputs = circuit.stream().mapToInt(this::getNumberOfInputs).sum();

        // get number of gates above level 0 (bound inputs)
        int boundInputs = 0;

        for (int level = 1; level < circuit.size(); level ++) {
            for(Integer element : circuit.get(level)) {
                if (element != 0)
                    boundInputs ++;
            }
        }

        return totalInputs - boundInputs;
    }

    private int getCircuitWeight(List<List<Integer>> circuit) {

        int weight = 0;

        for (List<Integer> row : circuit) {
            for (Integer element : row) {

                int primId = element - 1;

                if (primId >= 0)
                    weight += primitiveGates.get(primId).getWeight();
            }
        }

        return weight;
    }

    private boolean hasRedundantInverters(List<List<Integer>> circuit) {

        if (inverterId < 0)
            return false;

        for (int level = 0; level < (circuit.size() - 1); level ++) {

            int upperRow = 0;

            for (Integer element : circuit.get(level)) {

                int primId = element - 1;

                if (primId == inverterId) {

                    if ((circuit.get(level + 1).get(upperRow) - 1) == inverterId)
                        return true;
                }

                if (primId >= 0)
                    upperRow += primitiveGates.get(primId).getNumInputs();
            }
        }
        return false;
    }

    public HashMap<TruthTable, Circuit> getResultCircuits() {
        return resultCircuits;
    }

    /* main algorithm */

    public void enumerate() {

        logger.info("starting circuit enumeration.");

        intermediateCircuits = new ArrayList<>();
        resultCircuits = new HashMap<>();
        gateCounter = 0;

        OptionalInt maxPrimeFeasibility = primitiveGates.stream().mapToInt(Gate::getNumInputs).max();
        if (maxPrimeFeasibility.isEmpty())
            return;

        // max number of gates in uppermost level
        int maxRowLength = (int) Math.pow(maxPrimeFeasibility.getAsInt(), maxDepth - 1);

        // array of array lists of arrays containing combinations
        List<List<Integer>>[] combinations = new ArrayList[maxRowLength];

        for (int i = 0; i < maxRowLength; i ++) {
            combinations[i] = new ArrayList<>();
        }

        // generate rows (combinations of gates and empty slots)
        for (int rowLength = 1; rowLength <= maxRowLength; rowLength ++) {

            List<List<Integer>> lengthCombinations = combinations[rowLength - 1];

            for (Integer i = 1; i < (int) Math.pow(primitiveGates.size() + 1, rowLength); i ++) {

                String combination = Integer.toString(i, primitiveGates.size() + 1);
                combination = StringUtils.leftPad(combination, rowLength, '0');

                ArrayList<Integer> row = new ArrayList<>(rowLength);

                for (int j = 0; j < combination.length(); j++) {
                    row.add(j, Character.getNumericValue(combination.charAt(j)));
                }

                if (isCoveredByLibrary(row))
                    lengthCombinations.add(row);
            }

        }

        // call recursive circuit build function
        List<List<Integer>> emptyCircuit = new ArrayList<>(maxDepth);
        buildCircuits(emptyCircuit, combinations, 0);

        logger.info("found " + intermediateCircuits.size() + " pre-filtered circuits.");

        filterRedundantCircuits();

        logger.info("found " + intermediateCircuits.size() + " structurally different circuits.");

        for (List<List<Integer>> circuit : intermediateCircuits) {
            buildWireAddCircuit(circuit);
        }
    }

    private void buildCircuits(List<List<Integer>> circuit, List<List<Integer>>[] combinations, int level) {

        // if max depth reached --> abort
        if (level >= maxDepth)
            return;

        // if circuit is empty --> build start circuits and recurse
        if (level == 0) {

            for (List<Integer> row : combinations[0]) {

                List<List<Integer>> newCircuit = new ArrayList<>(circuit);
                newCircuit.add(0, row);
                buildCircuits(newCircuit, combinations, 1);
            }

        // if circuit is not empty --> extend by next row
        } else {

            // get number of inputs of lower level
            int numberOfInputs = getNumberOfInputs(circuit.get(level - 1));

            // iterate over rows with corresponding number of gates/entries
            for (List<Integer> row : combinations[numberOfInputs - 1]) {

                List<List<Integer>> newCircuit = new ArrayList<>(circuit);
                newCircuit.add(level, row);

                if (circuitAllowedByPreFilter(newCircuit))
                    intermediateCircuits.add(newCircuit);

                buildCircuits(newCircuit, combinations, level + 1);
            }
        }
    }

    private boolean circuitAllowedByPreFilter(List<List<Integer>> circuit) {

        // check if circuit is implementable with library
        if (!isCoveredByLibrary(circuit.stream().flatMap(Collection::stream).collect(Collectors.toCollection(ArrayList::new))))
            return false;

        // check is feasibility is met
        if (getNumberOfUnboundInputs(circuit) < feasibility)
            return false;

        // check circuit weight. if it exceeds maximum --> continue
        if (getCircuitWeight(circuit) > maxWeight)
            return false;

        // check if circuit contains redundant inverters
        if (hasRedundantInverters(circuit))
            return false;

        return true;
    }

    private List<HashMap<Coordinates, List<List<Integer>>>> pathDB;

    private void filterRedundantCircuits() {

        // calculate all paths
        pathDB = new ArrayList<>();

        for (List<List<Integer>> circuit : intermediateCircuits) {
            pathDB.add(getAllPaths(circuit));
        }

        // filter structurally equivalent circuits
        List<Integer> resultCircuitIndices = new ArrayList<>();
        resultCircuitIndices.add(0);

        for (int i = 0; i < intermediateCircuits.size(); i ++) {

            boolean isUnique = true;

            for (Integer cmpIndex : resultCircuitIndices) {

                if (structurallyEquivalent(intermediateCircuits.get(i), i, intermediateCircuits.get(cmpIndex), cmpIndex)) {
                    isUnique = false;
                    break;
                }
            }

            if (isUnique)
                resultCircuitIndices.add(i);
        }

        // build list of filtered circuits
        List<List<List<Integer>>> filteredCircuits = new ArrayList<>();

        for (Integer index : resultCircuitIndices) {
            filteredCircuits.add(intermediateCircuits.get(index));
        }

        intermediateCircuits = filteredCircuits;
    }

    private boolean structurallyEquivalent(List<List<Integer>> circuit1, int circuit1Index, List<List<Integer>> circuit2, int circuit2Index) {

        if (circuit1.size() != circuit2.size())
            return false;

        // get paths
        HashMap<Coordinates, List<List<Integer>>> paths1 = pathDB.get(circuit1Index);
        HashMap<Coordinates, List<List<Integer>>> paths2 = pathDB.get(circuit2Index);

        // create mark list
        List<List<Boolean>> circuit2Marks = new ArrayList<>();

        for (int level = 0; level < circuit2.size(); level ++) {

            ArrayList<Boolean> markRow = new ArrayList<>();
            circuit2Marks.add(markRow);

            for (Integer element : circuit2.get(level)) {
                markRow.add(false);
            }
        }

        // iterate over elements of circuit 1 and search equivalents in circuit 2
        for (int level = 0; level < circuit1.size(); level ++) {

            if (circuit1.get(level).size() != circuit2.get(level).size())
                return false;

            int numRows = circuit1.get(level).size();

            for (int row = 0; row < numRows; row ++) {

                int element = circuit1.get(level).get(row);
                Coordinates coords1 = new Coordinates(level, row);
                List<List<Integer>> element1Paths = paths1.get(coords1);

                boolean equivalentFound = false;

                // search equivalent gate in circuit 2
                for (int row2 = 0; row2 < numRows; row2 ++) {

                    // if element is same type and is not yet marked
                    if (circuit2.get(level).get(row2) == element && !circuit2Marks.get(level).get(row2)) {

                        Coordinates coords2 = new Coordinates(level, row2);
                        List<List<Integer>> element2Paths = paths2.get(coords2);

                        if (element2Paths.containsAll(element1Paths) && element1Paths.containsAll(element2Paths)) {
                            circuit2Marks.get(level).set(row2, true);
                            equivalentFound = true;
                            break;
                        }
                    }
                }

                if (!equivalentFound)
                    return false;
            }
        }

        // check if every element of circuit 2 is marked
        for (List<Boolean> level : circuit2Marks) {
            for (Boolean marked : level) {
                if (!marked)
                    return false;
            }
        }

        return true;
    }

    private HashMap<Coordinates, List<List<Integer>>> getAllPaths(List<List<Integer>> circuit) {

        HashMap<Coordinates, List<List<Integer>>> paths = new HashMap<>();

        for (int level = circuit.size() - 1; level >= 0; level --) {

            int rowLength = circuit.get(level).size();

            for (int row = 0; row < rowLength; row ++) {

                Coordinates currentNode = new Coordinates(level, row);
                List<Coordinates> connectedNodes = getInputNodes(circuit, currentNode);

                paths.putIfAbsent(currentNode, new ArrayList<>());

                if (connectedNodes.isEmpty()) {

                    ArrayList<Integer> path = new ArrayList<>();
                    path.add(circuit.get(level).get(row));
                    paths.get(currentNode).add(path);
                }

                for (Coordinates node : connectedNodes) {

                    for (List<Integer> path : paths.get(node)) {

                        ArrayList<Integer> extendedPath = new ArrayList<>(path);
                        extendedPath.add(circuit.get(level).get(row));
                        paths.get(currentNode).add(extendedPath);
                    }

                }
            }
        }

        return paths;
    }

    private List<Coordinates> getInputNodes(List<List<Integer>> circuit, Coordinates coords) {

        List<Coordinates> inputNodes = new ArrayList<>();

        int element = circuit.get(coords.level).get(coords.row);
        int numInputs = (element > 0) ? primitiveGates.get(element - 1).getNumInputs() : 0;

        // if element is on uppermost level --> return empty list
        if (coords.level == (circuit.size() - 1)) {
            return inputNodes;
        }

        int upperInput = 0;

        for (int currentRow = 0; currentRow < coords.row; currentRow ++) {

            int currentElement = circuit.get(coords.level).get(currentRow);

            if (currentElement > 0)
                upperInput += primitiveGates.get(currentElement - 1).getNumInputs();
        }

        for (int input = upperInput; input < (upperInput + numInputs); input ++) {

            inputNodes.add(new Coordinates(coords.level + 1, input));
        }

        return inputNodes;

    }

    static private class Coordinates {

        public final Integer level;
        public final Integer row;

        public Coordinates(Integer level, Integer row) {
            this.level = level;
            this.row = row;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Coordinates)) {
                return false;
            }
            Coordinates cmp  = (Coordinates) o;
            EqualsBuilder builder = new EqualsBuilder();
            builder.append(level, cmp.level);
            builder.append(row, cmp.row);
            return builder.isEquals();
        }

        @Override
        public int hashCode() {
            HashCodeBuilder builder = new HashCodeBuilder();
            builder.append(level);
            builder.append(row);
            return builder.hashCode();
        }
    }

    private void buildWireAddCircuit(List<List<Integer>> candidate) {

        /* build circuit */
        Circuit circuit = new Circuit();
        Graphs.addGraph(circuit, templateCircuit);

        // add first logic gate after output
        Gate libraryPrimitive = primitiveGates.get(candidate.get(0).get(0) - 1);
        gateCounter++;
        Gate newGate = new Gate(libraryPrimitive.getExpression(), libraryPrimitive.getPrimitiveIdentifier(), libraryPrimitive.getPrimitiveIdentifier() + "_" + gateCounter, Gate.Type.LOGIC);
        circuit.addVertex(newGate);
        circuit.addEdge(newGate, circuit.getOutputBuffer(), new Wire(circuit.getOutputBuffer().getExpression().variables().first()));

        // add rows of gates and wire them
        List<Gate> prevRow = new ArrayList<>();
        List<Gate> currentRow = new ArrayList<>();
        prevRow.add(newGate);

        for (int level = 1; level < candidate.size(); level ++) {

            int column = 0;

            for (Gate anchorGate : prevRow) {

                for (Variable anchorVariable : anchorGate.getExpression().variables()) {

                    int primId = candidate.get(level).get(column) - 1;

                    if (primId >= 0) {

                        libraryPrimitive = primitiveGates.get(primId);
                        gateCounter++;
                        newGate = new Gate(libraryPrimitive.getExpression(), libraryPrimitive.getPrimitiveIdentifier(), libraryPrimitive.getPrimitiveIdentifier() + "_" + gateCounter, Gate.Type.LOGIC);
                        circuit.addVertex(newGate);
                        circuit.addEdge(newGate, anchorGate, new Wire(anchorVariable));

                        currentRow.add(newGate);
                    }

                    column++;
                }
            }

            prevRow = new ArrayList<>(currentRow);
            currentRow.clear();
        }

        /* wire inputs, post-build checks, addition */
        wireAndAddCircuit(circuit);
    }

    private void wireAndAddCircuit(Circuit candidate) {

        // list ports to be connected
        List<Gate> primaryInputs = new ArrayList<>(candidate.getInputBuffers());
        HashMap<Gate, List<Variable>> gateInputs = candidate.getUnconnectedGateInputs();

        // check if enough gate inputs available for feasibility
        int numGateInputs = gateInputs.keySet().stream().mapToInt(gate -> gateInputs.get(gate).size()).sum();
        if (primaryInputs.size() > numGateInputs) {
            return;
        }

        // connect all combinations of primary inputs and gate inputs
        List<Gate> inputMapping = new ArrayList<>(numGateInputs);

        wiring:
        for (Integer i = 0; i < (int) Math.pow(primaryInputs.size(), numGateInputs); i ++) {

            inputMapping.clear();

            String combination = Integer.toString(i, primaryInputs.size());
            combination = StringUtils.leftPad(combination, numGateInputs, '0');

            for (int j = 0; j < combination.length(); j ++) {

                int inputNum = Character.getNumericValue(combination.charAt(j));
                Gate selectedInput = primaryInputs.get(inputNum);
                inputMapping.add(j, selectedInput);
            }

            // test if mapping contains all primary inputs
            if (!inputMapping.containsAll(primaryInputs))
                continue;

            // wire according to mapping
            Circuit wiredCandidate = new Circuit("gen_circuit");
            Graphs.addGraph(wiredCandidate, candidate);

            int inputNum = 0;
            for (Gate gate : gateInputs.keySet()) {
                for (Variable var : gateInputs.get(gate)) {

                    //check if same input is wired multiple times to one prim. gate --> abort
                    if (wiredCandidate.containsEdge(inputMapping.get(inputNum), gate))
                        continue wiring;

                    wiredCandidate.addEdge(inputMapping.get(inputNum), gate, new Wire(var));
                    inputNum++;
                }
            }

            Formula wiredCandidateExpression = wiredCandidate.getExpression();

            // filter out circuits with reduced support
            if (wiredCandidateExpression.variables().size() < wiredCandidate.getInputBuffers().size())
                continue;

            // remove redundant gates
            if (!wiredCandidate.removeRedundantGates())
                continue;

            TruthTable truthTable = new TruthTable(wiredCandidateExpression);

            // filter out trivial circuits //TODO: check other trivial outputs?
            if (truthTable.getTruthTable() == 0)
                continue;

            // only let fitting circuits pass if guided enumeration with input truth table is done
            if (targetTruthTable != null && !truthTable.equalsLogically(targetTruthTable))
                continue;

            // filter out structurally equivalent circuits
            for (TruthTable libTruthTable : resultCircuits.keySet()) {

                if (targetTruthTable != null || libTruthTable.equalsLogically(truthTable)) {

                    if (wiredCandidate.isEquivalent(resultCircuits.get(libTruthTable))) {
                        continue wiring;
                    }
                }
            }

            resultCircuits.put(truthTable, wiredCandidate);

            //if ((resultCircuits.size() % 1000) == 0)
                //logger.info("found: " + resultCircuits.size());
        }
    }
}
