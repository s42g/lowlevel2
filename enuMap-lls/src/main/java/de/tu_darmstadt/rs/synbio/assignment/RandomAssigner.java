package de.tu_darmstadt.rs.synbio.assignment;

import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.Gate;
import de.tu_darmstadt.rs.synbio.logic.GateLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class RandomAssigner {

    private static final Logger logger = LoggerFactory.getLogger(RandomAssigner.class);

    final private Random randomGenerator;

    // gates
    final private HashMap<String, List<Gate>> availableGates;
    final private List<Gate> circuitGates;

    public RandomAssigner(GateLibrary gateLib, Circuit circuit) {

        // initialize gate library
        this.availableGates = gateLib.getPrimitives();

        // initialize circuit gate list
        this.circuitGates = circuit.vertexSet().stream().filter(g -> g.getType().equals(Gate.Type.LOGIC)).collect(Collectors.toList());

        this.randomGenerator = new Random();
    }

    public Assignment getNextAssignment() {

        Assignment assignment = new Assignment();

        do {
            for (Gate gate : circuitGates) {

                int rand = randomGenerator.nextInt(availableGates.get(gate.getPrimitiveIdentifier()).size());
                assignment.put(gate, availableGates.get(gate.getPrimitiveIdentifier()).get(rand));
            }
        } while (!assignment.isValid());

        return assignment;
    }

    public Assignment shuffleGates(Assignment assignment) {

        Assignment shuffled = new Assignment();

        List<Gate> gates = new ArrayList<>(assignment.values());
        Collections.shuffle(gates);

        for (Gate circuitGate : assignment.keySet()) {

            Gate selected = gates.stream().filter(g -> g.getPrimitiveIdentifier().equals(circuitGate.getPrimitiveIdentifier())).findFirst().get();
            gates.remove(selected);

            shuffled.put(circuitGate, selected);
        }

        return shuffled;
    }


}
