package de.tu_darmstadt.rs.synbio.logic;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.tu_darmstadt.rs.synbio.circuit.Circuit;
import de.tu_darmstadt.rs.synbio.circuit.CircuitDeserializer;
import de.tu_darmstadt.rs.synbio.circuit.CircuitSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.OptionalInt;

public class SupergateLibrary {

    private static final Logger logger = LoggerFactory.getLogger(SupergateLibrary.class);

    private HashMap<TruthTable, Circuit> supergates = new HashMap<>();

    public SupergateLibrary() {}

    public void save(File file) {

        if (supergates.isEmpty()) {
            logger.warn("Supergate library is empty and could not be saved.");
            return;
        }

        logger.info("Saving supergate library to file " + file.getAbsolutePath());

        ObjectMapper mapper = new ObjectMapper();
        CircuitSerializer circuitSerializer = new CircuitSerializer(Circuit.class);
        SimpleModule module = new SimpleModule("CircuitSerializer", new Version(1, 0, 0, null, null, null));
        module.addSerializer(Circuit.class, circuitSerializer);
        mapper.registerModule(module);

        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, supergates);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*int i = 0;
        for (Circuit sg : supergates.values()) {
            sg.print(new File("sg_dump", "sg_" + i + ".dot"));
            i ++;
        }*/
    }

    public void load(File file) {

        //file = new File("consensus.json");

        logger.info("Loading supergate library file " + file.getAbsolutePath());

        ObjectMapper mapper = new ObjectMapper();
        CircuitDeserializer circuitDeserializer = new CircuitDeserializer(Circuit.class);
        SimpleModule module = new SimpleModule("CircuitDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(Circuit.class, circuitDeserializer);
        mapper.registerModule(module);

        try {
            supergates = mapper.readValue(file, new TypeReference<HashMap<TruthTable, Circuit>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<TruthTable, Circuit> get() {
       return supergates;
    }

    public void print() {

        logger.info("Supergates:");
        logger.info(supergates.keySet().size() + " supergates found");
    }

    public int getFeasibility() {

        OptionalInt feas = supergates.values().stream().mapToInt(c -> c.getExpression().variables().size()).max();
        return feas.isPresent() ? feas.getAsInt() : 0;
    }

    public void addSupergates(HashMap<TruthTable, Circuit> supergates) {
        this.supergates.putAll(supergates);
        logger.info("Added " + supergates.size() + " supergates.");
    }

}
